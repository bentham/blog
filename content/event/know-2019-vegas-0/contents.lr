title: KNOW 2019 (Vegas)
---
author: steph
---
start_date: 2019-03-24
---
end_date: 2019-03-27
---
body:


![Roger Dingledine - KNOW](/sites/default/files/inline-images/Roger-Dingledine-KNOW.jpg)
***“Privacy is about choice. We’re giving people the choice.” – Roger Dingledine***

KNOW 2019 is proud to announce that [Roger Dingledine](https://vegas.knowconf.com/speaker/roger-dingledine/), an original advocate for online anonymity, is dropping the mic on the keynote stage!

We’ve entered a brave new world in the age of internet surveillance, and Roger is a pioneer in digital identity protection. He’s the co-founder and an original developer of Tor, an onion router born out of a collaboration with the U.S. Navy (just don’t confuse it with the Dark Web… he hates that). Roger’s been named to MIT Technology Review’s 35 Innovators under 35, won the Usenix Security “Test of Time” award, and has been recognized by Foreign Policy magazine as a top 100 global thinker. 

Today, his mission is to preserve and protect identities all over the world. 

[Don’t miss this opportunity to join Roger as we define the Future of Trust at KNOW 2019.](https://vegas.knowconf.com/register/) 



---
tags: events
