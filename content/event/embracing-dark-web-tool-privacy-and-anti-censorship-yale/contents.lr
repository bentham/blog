title: Embracing the Dark Web as a Tool for Privacy and Anti-Censorship (Yale)
---
author: steph
---
start_date: 2018-10-23
---
body:

Open to The Yale Community

Much has been written about illegal activity that takes place on the Dark Web. Less is said about why the Dark Web came to be and the important role it plays in human rights work around the globe. As tracking Internet browsing and usage patterns becomes a common source of income for the people entrusted to provide us with access to vital resources, as threats to free speech and free press are on the rise and critical voices are stifled and censored, and as people around the world become more and more vulnerable to targeting based on political and personal associations, the Dark Web offers a way to protect us from unwarranted tracking, surveillance, and censorship on the Internet.

This talk will discuss the history and purpose of Tor’s onion services, present insight into this misunderstood technology, and affirm its role as a tool for advancing basic human rights worldwide.

Shari Steele is the executive director of the Tor Project, a US-based nonprofit organization that provides the technical infrastructure for much of the internet freedom movement.  Before joining the Tor Project, Shari spent 15 years as executive director of the Electronic Frontier Foundation (EFF), the premiere civil liberties organization of the online world.  In her 25+ years fighting for digital rights, Shari has watched the internet grow from a military-centric network to the essential tool for communications and commerce that it is today.  Shari will be retiring at the end of 2018.

### Sponsoring Organization(s)

ISP, Tech Soc

---
tags:

events
onion services
