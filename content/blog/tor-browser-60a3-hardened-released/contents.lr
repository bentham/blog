title: Tor Browser 6.0a3-hardened is released
---
pub_date: 2016-03-08
---
author: boklm
---
tags:

tor browser
tbb
tbb-6.0
---
categories:

applications
releases
---
_html_body:

<p>A new hardened Tor Browser release is available. It can be found in the <a href="https://dist.torproject.org/torbrowser/6.0a3-hardened/" rel="nofollow">6.0a3-hardened distribution directory</a> and on the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-hardened" rel="nofollow">download page for hardened builds</a>.</p>

<p>This release features important <a href="https://www.mozilla.org/en-US/security/known-vulnerabilities/firefox-esr/#firefoxesr38.7" rel="nofollow">security updates</a> to Firefox.</p>

<p>This release bumps the versions of several of our components, e.g.: Firefox to 38.7.0esr, Tor to 0.2.8.1-alpha, OpenSSL to 1.0.1s, NoScript to 2.9.0.4 and HTTPS-Everywhere to 5.1.4.</p>

<p>Additionally, we fixed long-standing bugs in our Tor circuit display and window resizing code, and improved the usability of our font fingerprinting defense further.</p>

<p><strong>Note</strong>: There is no incremental update from 6.0a2-hardened available due to <a href="https://trac.torproject.org/projects/tor/ticket/17858" rel="nofollow">bug 17858</a>. The internal updater should work, though, doing a complete update.</p>

<p>Here is the complete changelog since 6.0a2-hardened:</p>

<p>Tor Browser 6.0a3-hardened -- March 8</p>

<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 38.7.0esr</li>
<li>Update Tor to 0.2.8.1-alpha</li>
<li>Update OpenSSL to 1.0.1s</li>
<li>Update NoScript to 2.9.0.4</li>
<li>Update HTTPS Everywhere to 5.1.4</li>
<li>Update Torbutton to 1.9.5.1
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/16990" rel="nofollow">Bug 16990</a>: Don't mishandle multiline commands</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18144" rel="nofollow">Bug 18144</a>: about:tor update arrow position is wrong</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/16725" rel="nofollow">Bug 16725</a>: Allow resizing with non-default homepage</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/16917" rel="nofollow">Bug 16917</a>: Allow users to more easily set a non-tor SSH proxy</li>
<li>Translation updates</li>
</ul>
</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18030" rel="nofollow">Bug 18030</a>: Isolate favicon requests on Page Info dialog</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18297" rel="nofollow">Bug 18297</a>: Use separate Noto JP,KR,SC,TC fonts</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18170" rel="nofollow">Bug 18170</a>: Make sure the homepage is shown after an update as well</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/16728" rel="nofollow">Bug 16728</a>: Add test cases for favicon isolation</li>
</ul>
</li>
<li>Windows
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18292" rel="nofollow">Bug 18292</a>: Disable staged updates on Windows</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-161491"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161491" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2016</p>
    </div>
    <a href="#comment-161491">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161491" class="permalink" rel="bookmark">hii i know many time this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hii i know many time this ask when will harden tbb for stable ? security vry important for me and what danger am i when alpha tbb i use? thank!! much love Qatar</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-161649"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161649" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">March 09, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-161491" class="permalink" rel="bookmark">hii i know many time this</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-161649">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161649" class="permalink" rel="bookmark">There are no plans to have</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are no plans to have the current hardened features used in the stable series as well. There are performance issues and general issues of using ASan builds in production (i.e. stable series).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-161512"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161512" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2016</p>
    </div>
    <a href="#comment-161512">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161512" class="permalink" rel="bookmark">Clownfare is EVIL!!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Clownfare is EVIL!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-161539"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161539" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2016</p>
    </div>
    <a href="#comment-161539">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161539" class="permalink" rel="bookmark">Just fired up tor 6.03a and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just fired up tor 6.03a and it said an update was available. I have not downloaded the latest version but suddenly my onion button has disappeared. Is someone hacking?  Strange coincidence that today in the UK there is a military security fair with a company that claims to be able to get files from encrypted trucrypt folders.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-161650"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161650" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">March 09, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-161539" class="permalink" rel="bookmark">Just fired up tor 6.03a and</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-161650">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161650" class="permalink" rel="bookmark">Works fine for me. Is that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Works fine for me. Is that reproducible? If so, how?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-162035"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162035" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 11, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-162035">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162035" class="permalink" rel="bookmark">I cannot reproduce this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I cannot reproduce this event because it only happened with the update notification. The browser kind of 'jumped' then when it settled the onion button had gone. So did an update with the latest version and all works fine.  In view of the other comment about anti virus causing a problem it indicates a glitch in the update notification link.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-161679"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161679" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-161539" class="permalink" rel="bookmark">Just fired up tor 6.03a and</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-161679">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161679" class="permalink" rel="bookmark">Antivirus somehow</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Antivirus somehow removed/disabled important extensions in the Tor Browser profile?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-161611"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161611" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2016</p>
    </div>
    <a href="#comment-161611">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161611" class="permalink" rel="bookmark">Hi, My question is this.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, My question is this. Could you describe some of the differences between the standard stable torbrowser package, and the "hardened" version? Thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-161652"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161652" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">March 09, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-161611" class="permalink" rel="bookmark">Hi, My question is this.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-161652">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161652" class="permalink" rel="bookmark">Yes. The main differences</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes. The main differences are that Tor and Firefox the hardened series are compiled with Address Sanitizer and in Tor's case with Undefined Behavior Sanitizer additionally. Moreover, both Firefox and Tor are compiled with -fwrapv.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-161765"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161765" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-161765">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161765" class="permalink" rel="bookmark">https://trac.torproject.org/p</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://trac.torproject.org/projects/tor/ticket/17983" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/17983</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-161873"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161873" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-161873">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161873" class="permalink" rel="bookmark">Can we get some practical</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can we get some practical examples of how these measures help? Helps protect against certain 0day bugs in Firefox?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-162011"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162011" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">March 11, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-161873" class="permalink" rel="bookmark">Can we get some practical</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-162011">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162011" class="permalink" rel="bookmark">This is a good question. We</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is a good question. We don't have those numbers (yet). However, we might learn something later this year when we'll look closer at Firefox's vulnerability history which is one of the ToDo things for our sponsor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-162587"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162587" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-161873" class="permalink" rel="bookmark">Can we get some practical</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-162587">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162587" class="permalink" rel="bookmark">Words from my mouth 2</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Words from my mouth 2</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-161773"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161773" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2016</p>
    </div>
    <a href="#comment-161773">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161773" class="permalink" rel="bookmark">The Google video CDN auto</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The Google video CDN auto block exit node as spam IP , even I pass CAPTCHA</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-162117"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162117" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 11, 2016</p>
    </div>
    <a href="#comment-162117">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162117" class="permalink" rel="bookmark">Thanks again for keeping</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks again for keeping pace with Firefox's release schedule</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-162507"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162507" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2016</p>
    </div>
    <a href="#comment-162507">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162507" class="permalink" rel="bookmark">Please add a feature that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please add a feature that allows drag and drop from the URL bar to the computer desktop to create an internet shortcut.  This can be done in regular Firefox.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-162734"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162734" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 13, 2016</p>
    </div>
    <a href="#comment-162734">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162734" class="permalink" rel="bookmark">What is going on between the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What is going on between the TOR webbrowser and Recaptcha? It's becoming a pain in the ass.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-162739"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162739" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 13, 2016</p>
    </div>
    <a href="#comment-162739">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162739" class="permalink" rel="bookmark">What is use and reason that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What is use and reason that CloudFlare does this check "ReCaptcha"?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-162755"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162755" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 13, 2016</p>
    </div>
    <a href="#comment-162755">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162755" class="permalink" rel="bookmark">I&#039;ve done two postings about</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've done two postings about the recaptcha issues. I've done my homework :) and found more info about it. (<a href="https://trac.torproject.org/projects/tor/ticket/18361" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/18361</a>). A never ending issue / problem / story :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-163011"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-163011" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 14, 2016</p>
    </div>
    <a href="#comment-163011">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-163011" class="permalink" rel="bookmark">We need a feature that warns</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We need a feature that warns when a link leads to a site with capcha garbage so we can avoid those capcha using sites.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-163263"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-163263" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 15, 2016</p>
    </div>
    <a href="#comment-163263">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-163263" class="permalink" rel="bookmark">How i can solve this - &gt; To</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How i can solve this - &gt; To view this page ensure that Adobe Flash Player version 11.1.0 or greater is installed.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-163294"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-163294" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 15, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-163263" class="permalink" rel="bookmark">How i can solve this - &gt; To</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-163294">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-163294" class="permalink" rel="bookmark">https://www.torproject.org/do</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.torproject.org/docs/faq.html.en#TBBFlash" rel="nofollow">https://www.torproject.org/docs/faq.html.en#TBBFlash</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-163966"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-163966" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 18, 2016</p>
    </div>
    <a href="#comment-163966">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-163966" class="permalink" rel="bookmark">Tor users HATE you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor users HATE you Clownfare!  Do you UNDERSTAND?  We want you to GO OUT OF BUSINESS.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-166367"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-166367" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2016</p>
    </div>
    <a href="#comment-166367">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-166367" class="permalink" rel="bookmark">this browser is now necome</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>this browser is now necome very slow now.i have checked at on differnt pcs and laptops but working very slow</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-173087"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-173087" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 24, 2016</p>
    </div>
    <a href="#comment-173087">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-173087" class="permalink" rel="bookmark">New to Tor. Is hardened Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>New to Tor. Is hardened Tor better? What is best version? Where<br />
do I get step by step instructions how to use Tor instead of Safari or any other or do I use one special platform with Tor as main platform? if so, how? Which one?</p>
<p>Better not to put Tor on my Mac itself?</p>
<p>I will do anything to increase privacy.</p>
<p><a href="mailto:suzannedk@gmail.com" rel="nofollow">suzannedk@gmail.com</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-174133"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-174133" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">April 27, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-173087" class="permalink" rel="bookmark">New to Tor. Is hardened Tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-174133">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-174133" class="permalink" rel="bookmark">You can find instructions</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can find instructions for using Tor Browser on this page:<br />
<a href="https://www.torproject.org/projects/torbrowser.html.en" rel="nofollow">https://www.torproject.org/projects/torbrowser.html.en</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
