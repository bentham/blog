title: New Release: BridgeDB 0.9.3
---
pub_date: 2020-02-11
---
author: phw
---
tags:

censorship circumvention
bridgedb
bridge distribution
bridges
---
categories: circumvention
---
summary:

When ISPs or governments block access to the Tor network, our users rely on bridges to connect. With BridgeDB, we tackle the problem of how to get bridges to censored users while making it difficult for censors to get all bridges. A lot has changed since our last blog post, which introduced BridgeDB version 0.7.1. 
---
_html_body:

<p>When ISPs or governments block access to the Tor network, our users rely on <a href="https://community.torproject.org/relay/types-of-relays/">bridges</a> to connect. With <a href="https://bridges.torproject.org">BridgeDB</a>, we tackle the problem of how to get bridges to censored users while making it difficult for censors to get all bridges.</p>
<p>A lot has changed since our last blog post, which introduced BridgeDB <a href="https://blog.torproject.org/new-release-bridgedb-071">version 0.7.1</a>. We recently released BridgeDB version 0.9.3, which comes with the following bug fixes and new features:</p>
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/24607">Ticket 24607</a>: We recently made BridgeDB's CAPTCHAs easier to solve by reducing the amount of noise in images. Our usage metrics reveal that the success rate increased from ~50% to ~80%. How is your experience with BridgeDB's CAPTCHAs? Let us know in the comments!</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/9316">Ticket 9316</a>: BridgeDB is now reporting privacy-preserving usage metrics every 24 hours. These metrics help us understand how users interact with BridgeDB. For example, these metrics <a href="https://trac.torproject.org/projects/tor/ticket/32135#comment:1">reveal</a> that Moat is our most popular distributor and obfs4 is our most requested transport protocol. The raw metrics are <a href="https://collector.torproject.org/archive/bridgedb-metrics/">archived on CollecTor</a> but you can also take a look at the <a href="https://metrics.torproject.org/bridgedb-transport.html">number of requests per transport</a> and <a href="https://metrics.torproject.org/bridgedb-distributor.html">requests per distributor</a> on Tor Metrics.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/31252">Ticket 31252</a>: BridgeDB sees many automated requests from bots. This patch makes it possible to detect and block bots based on their HTTP parameters.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/26543">Ticket 26543</a>: We added a language switcher menu to the top right corner of BridgeDB's user interface.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32203">Ticket 32203</a>: We fixed a bug that resulted in missing requests for vanilla bridges.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32134">Ticket 32134</a>: Request another translation and update BridgeDB's documentation on how to request translations.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32105">Ticket 32105</a>: We debugged and fixed an email responder issue that emerged after upgrading BridgeDB's underlying operating system to Debian buster.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/31903">Ticket 31903</a>: We updated existing translations and published translation requests for new strings.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/31780">Ticket 31780</a>: This patch adds a specification for the metrics we implemented as part of <a href="https://trac.torproject.org/projects/tor/ticket/9316">ticket 9316</a>.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/29484">Ticket 29484</a>: We updated BridgeDB's requirements to their latest respective versions.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/17626">Ticket 17626</a>: We helped BridgeDB's email responder deal with quoted email responses. This should make the email responder more pleasant to interact with.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/28533">Ticket 28533</a>: We added links to our Support Portal and our Tor Browser Manual to BridgeDB's landing page and we removed the front desk's email address, which greatly reduced the number of automated email requests.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/26542">Ticket 26542</a>: We fixed a bug that prevented BridgeDB from handing out vanilla IPv6 bridges.</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/22755">Ticket 22755</a>: Some of BridgeDB's unit tests require bridge descriptors to work. We have been using the tool leekspin to create fake descriptors before unit tests are run. This patch replaced leekspin with a script that uses stem to create fake descriptors. This simplifies our unit tests and reduces the number of BridgeDB's dependencies.</li>
</ul>
<p>Several other bug fixes and features are already in development. You can expect the following changes in the near future:</p>
<ul>
<li>BridgeDB was originally implemented in Python 2, which is no longer supported since January 1, 2020. We have been busy porting BridgeDB's code base to Python 3 and we're <a href="https://trac.torproject.org/projects/tor/ticket/30946">almost done</a>.</li>
<li>As part of <a href="https://trac.torproject.org/projects/tor/wiki/org/sponsors/Sponsor30">Sponsor 30</a>, we are working on several UX improvements. For example, we will build a feedback loop between BridgeDB and OONI: BridgeDB will learn from OONI what bridges are blocked where, and use this knowledge to be smarter about bridge distribution. For example, if a user from Turkey is requesting bridges, BridgeDB will no longer hand this user bridges that are blocked in Turkey.</li>
</ul>

---
_comments:

<a id="comment-286676"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286676" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon... (not verified)</span> said:</p>
      <p class="date-time">February 11, 2020</p>
    </div>
    <a href="#comment-286676">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286676" class="permalink" rel="bookmark">Hello,
we can&#039;t install…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,</p>
<p>we can't install Extensions like uBloc Origin again...Why??</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-286798"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286798" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 17, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-286676" class="permalink" rel="bookmark">Hello,
we can&#039;t install…</a> by <span>Anon... (not verified)</span></p>
    <a href="#comment-286798">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286798" class="permalink" rel="bookmark">Please ask in a post with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please ask in a post with the title "Tor Browser". Don't ask here. This post is about BridgeDB.</p>
<p><a href="https://support.torproject.org/faq/faq-3/" rel="nofollow">https://support.torproject.org/faq/faq-3/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-286683"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286683" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 11, 2020</p>
    </div>
    <a href="#comment-286683">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286683" class="permalink" rel="bookmark">The reduction in the amount…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The reduction in the amount of noise in BridgeDB CAPTCHA images is excellent. Thanks for the tasty improvement.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286711"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286711" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 12, 2020</p>
    </div>
    <a href="#comment-286711">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286711" class="permalink" rel="bookmark">Great work! I hope…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great work! I hope translators are informed to participate. I'm interested in BridgeDB learning from OONI.</p>
<p>I don't think I've ever had issues with BridgeDB's CAPTCHAs. Although, please don't make them so easy that AI bots can solve them. Thanks for implementing ticket 31252 at the same time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286732"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286732" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Nancy (not verified)</span> said:</p>
      <p class="date-time">February 13, 2020</p>
    </div>
    <a href="#comment-286732">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286732" class="permalink" rel="bookmark">What is a bridge and do I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What is a bridge and do I need one</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-286735"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286735" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">February 13, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-286732" class="permalink" rel="bookmark">What is a bridge and do I…</a> by <span>Nancy (not verified)</span></p>
    <a href="#comment-286735">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286735" class="permalink" rel="bookmark">A bridge is a &quot;hidden&quot; Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A bridge is a "hidden" Tor relay that makes it possible to use the Tor network if your government or ISP is blocking access. You don't need a bridge if you can already use the Tor network without any problems.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286797"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286797" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 17, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-286732" class="permalink" rel="bookmark">What is a bridge and do I…</a> by <span>Nancy (not verified)</span></p>
    <a href="#comment-286797">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286797" class="permalink" rel="bookmark">&gt; What is a bridge and do I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; What is a bridge and do I need one</p>
<p>Read the first group and third group of bullets here<br />
<a href="https://blog.torproject.org/comment/286754#comment-286754" rel="nofollow">https://blog.torproject.org/comment/286754#comment-286754</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-286810"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286810" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 18, 2020</p>
    </div>
    <a href="#comment-286810">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286810" class="permalink" rel="bookmark">How does the moat protocol…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How does the moat protocol work?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-286819"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286819" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">February 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-286810" class="permalink" rel="bookmark">How does the moat protocol…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-286819">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286819" class="permalink" rel="bookmark">BridgeDB&#039;s README contains a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>BridgeDB's README contains a section with a brief moat specification:<br />
<a href="https://gitweb.torproject.org/bridgedb.git/tree/README.rst#n391" rel="nofollow">https://gitweb.torproject.org/bridgedb.git/tree/README.rst#n391</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-286864"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286864" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 23, 2020</p>
    </div>
    <a href="#comment-286864">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286864" class="permalink" rel="bookmark">If you&#039;re using obfs4,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you're using obfs4, couldn't an eavesdropper between you and the hidden bridge deduce that it's obfs4 and therefore that it's to Tor very simply by realizing it doesn't look like any standard protocols and watching your idle connection predictably make a new connection every 10 minutes?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-286901"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286901" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  cohosh
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">cohosh</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">cohosh said:</p>
      <p class="date-time">March 02, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-286864" class="permalink" rel="bookmark">If you&#039;re using obfs4,…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-286901">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286901" class="permalink" rel="bookmark">Obfs4 gets its censorship…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Obfs4 gets its censorship resistance properties by trying not to look like any standard protocols. It turns out that there's a lot of Internet traffic already that is hard to classify as something known and obfs4 attempts to hide in that "long tail" of unclassifiable traffic. This is the subject of ongoing research, and right now the answer seems to be "no". However, see some interesting <a href="https://ericw.us/trow/probe-proxy.pdf" rel="nofollow">recent research</a> on the subject.</p>
<p>We've also done some small-scale tests to see whether our unpublished bridges have been detected, and so they haven't. See <a href="https://bugs.torproject.org/29279" rel="nofollow">https://bugs.torproject.org/29279</a> for more details.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-286870"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286870" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 24, 2020</p>
    </div>
    <a href="#comment-286870">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286870" class="permalink" rel="bookmark">Some great work here, and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Some great work here, and especially including it in Tor Metrics.</p>
<p>On that note I was curious to find out about Tor's overall bridge usage, it seems Tor had a few noticeable spikes in bridge users, up to 4x normal within the past few years since metrics began logging, but always seems to revert back to minimum levels of around 50k users (if anyone might deduce a reason for this odd behavior beyond standard censorship its appreciated)</p>
<p>I'd like to see the Tor Blog continue to hold periodic public discussion as to ways both to connect more censored users to bridges, and encouraging others to themselves run bridges. As the best way to grow Tor is to get the word out.</p>
<p>Apologies if this is the wrong place, but the rest of the blog generally locks discussion after about a week, so discussing Tor related topics can prove difficult for the public.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286894"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286894" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 01, 2020</p>
    </div>
    <a href="#comment-286894">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286894" class="permalink" rel="bookmark">I got three bridges today…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I got three bridges today from BridgeDB's http onion service. I then hashed their fingerprints and went to metrics' Relay Search to make sure they were good, and it told me one of the three is "Not Recommended". So 1/3 of my activity would go first through a bridge that isn't recommended. Any relay, including bridges, that is deemed necessary to be flagged as not recommended should not be allowed on the network much less given out by BridgeDB.</p>
<p>Here is its hashed fingerprint, not the sensitive fingerprint in its bridge line.<br />
EB822E8EEDDD047D93F3E580023A30FED9C090A4<br />
"version":"0.2.9.16" (possibly Debian stretch (oldstable))<br />
"version_status":"obsolete"<br />
"recommended_version":false</p>
<p>Speaking of Tor's relay fingerprints, all of them, hashed and not, are in SHA1 which was recently reported to have vulnerabilities as well.</p>
</div>
  </div>
</article>
<!-- Comment END -->
