title: Tor 0.2.8.2-alpha is released
---
pub_date: 2016-03-28
---
author: nickm
---
tags:

tor
alpha
release
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.8.2-alpha has been released! You can download the source from the Tor website. Packages should be available over the next week or so.</p>

<p>Tor 0.2.8.2-alpha is the second alpha in its series. It fixes numerous bugs in earlier versions of Tor, including some that prevented authorities using Tor 0.2.7.x from running correctly. IPv6 and directory support should also be much improved.<br />
REMEMBER: This is an alpha release. Expect a lot of bugs. You should only run this release if you're willing to find bugs and report them.</p>

<h2>Changes in version 0.2.8.2-alpha - 2016-03-28</h2>

<ul>
<li>New system requirements:
<ul>
<li>Tor no longer supports versions of OpenSSL with a broken implementation of counter mode. (This bug was present in OpenSSL 1.0.0, and was fixed in OpenSSL 1.0.0a.) Tor still detects, but no longer runs with, these versions.
  </li>
<li>Tor no longer attempts to support platforms where the "time_t" type is unsigned. (To the best of our knowledge, only OpenVMS does this, and Tor has never actually built on OpenVMS.) Closes ticket 18184.
  </li>
<li>Tor now uses Autoconf version 2.63 or later, and Automake 1.11 or later (released in 2008 and 2009 respectively). If you are building Tor from the git repository instead of from the source distribution, and your tools are older than this, you will need to upgrade. Closes ticket 17732.
  </li>
</ul>
</li>
<li>Major bugfixes (security, pointers):
<ul>
<li>Avoid a difficult-to-trigger heap corruption attack when extending a smartlist to contain over 16GB of pointers. Fixes bug 18162; bugfix on 0.1.1.11-alpha, which fixed a related bug incompletely. Reported by Guido Vranken.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Major bugfixes (bridges, pluggable transports):
<ul>
<li>Modify the check for OR connections to private addresses. Allow bridges on private addresses, including pluggable transports that ignore the (potentially private) address in the bridge line. Fixes bug 18517; bugfix on 0.2.8.1-alpha. Reported by gk, patch by teor.
  </li>
</ul>
</li>
<li>Major bugfixes (compilation):
<ul>
<li>Repair hardened builds under the clang compiler. Previously, our use of _FORTIFY_SOURCE would conflict with clang's address sanitizer. Fixes bug 14821; bugfix on 0.2.5.4-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (crash on shutdown):
<ul>
<li>Correctly handle detaching circuits from muxes when shutting down. Fixes bug 18116; bugfix on 0.2.8.1-alpha.
  </li>
<li>Fix an assert-on-exit bug related to counting memory usage in rephist.c. Fixes bug 18651; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (crash on startup):
<ul>
<li>Fix a segfault during startup: If a Unix domain socket was configured as listener (such as a ControlSocket or a SocksPort "unix:" socket), and tor was started as root but not configured to switch to another user, tor would segfault while trying to string compare a NULL value. Fixes bug 18261; bugfix on 0.2.8.1-alpha. Patch by weasel.
  </li>
</ul>
</li>
<li>Major bugfixes (dns proxy mode, crash):
<ul>
<li>Avoid crashing when running as a DNS proxy. Fixes bug 16248; bugfix on 0.2.0.1-alpha. Patch from "cypherpunks".
  </li>
</ul>
</li>
<li>Major bugfixes (relays, bridge clients):
<ul>
<li>Ensure relays always allow IPv4 OR and Dir connections. Ensure bridge clients use the address configured in the bridge line. Fixes bug 18348; bugfix on 0.2.8.1-alpha. Reported by sysrqb, patch by teor.
  </li>
</ul>
</li>
<li>Major bugfixes (voting):
<ul>
<li>Actually enable support for authorities to match routers by their Ed25519 identities. Previously, the code had been written, but some debugging code that had accidentally been left in the codebase made it stay turned off. Fixes bug 17702; bugfix on 0.2.7.2-alpha.
  </li>
<li>When collating votes by Ed25519 identities, authorities now include a "NoEdConsensus" flag if the ed25519 value (or lack thereof) for a server does not reflect the majority consensus. Related to bug 17668; bugfix on 0.2.7.2-alpha.
  </li>
<li>When generating a vote with keypinning disabled, never include two entries for the same ed25519 identity. This bug was causing authorities to generate votes that they could not parse when a router violated key pinning by changing its RSA identity but keeping its Ed25519 identity. Fixes bug 17668; fixes part of bug 18318. Bugfix on 0.2.7.2-alpha.
  </li>
</ul>
</li>
<li>Minor features (security, win32):
<ul>
<li>Set SO_EXCLUSIVEADDRUSE on Win32 to avoid a local port-stealing attack. Fixes bug 18123; bugfix on all tor versions. Patch by teor.
  </li>
</ul>
</li>
<li>Minor features (bug-resistance):
<ul>
<li>Make Tor survive errors involving connections without a corresponding event object. Previously we'd fail with an assertion; now we produce a log message. Related to bug 16248.
  </li>
</ul>
</li>
<li>Minor features (build):
<ul>
<li>Detect systems with FreeBSD-derived kernels (such as GNU/kFreeBSD) as having possible IPFW support. Closes ticket 18448. Patch from Steven Chamberlain.
  </li>
</ul>
</li>
<li>Minor features (code hardening):
<ul>
<li>Use tor_snprintf() and tor_vsnprintf() even in external and low- level code, to harden against accidental failures to NUL- terminate. Part of ticket 17852. Patch from jsturgix. Found with Flawfinder.
  </li>
</ul>
</li>
<li>Minor features (crypto):
<ul>
<li>Validate the hard-coded Diffie-Hellman parameters and ensure that p is a safe prime, and g is a suitable generator. Closes ticket 18221.
  </li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the March 3 2016 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor features (hidden service directory):
<ul>
<li>Streamline relay-side hsdir handling: when relays consider whether to accept an uploaded hidden service descriptor, they no longer check whether they are one of the relays in the network that is "supposed" to handle that descriptor. Implements ticket 18332.
  </li>
</ul>
</li>
<li>Minor features (IPv6):
<ul>
<li>Add ClientPreferIPv6DirPort, which is set to 0 by default. If set to 1, tor prefers IPv6 directory addresses.
  </li>
<li>Add ClientUseIPv4, which is set to 1 by default. If set to 0, tor avoids using IPv4 for client OR and directory connections.
  </li>
<li>Try harder to obey the IP version restrictions "ClientUseIPv4 0", "ClientUseIPv6 0", "ClientPreferIPv6ORPort", and "ClientPreferIPv6DirPort". Closes ticket 17840; patch by teor.
  </li>
</ul>
</li>
<li>Minor features (linux seccomp2 sandbox):
<ul>
<li>Reject attempts to change our Address with "Sandbox 1" enabled. Changing Address with Sandbox turned on would never actually work, but previously it would fail in strange and confusing ways. Found while fixing 18548.
  </li>
</ul>
</li>
<li>Minor features (robustness):
<ul>
<li>Exit immediately with an error message if the code attempts to use Libevent without having initialized it. This should resolve some frequently-made mistakes in our unit tests. Closes ticket 18241.
  </li>
</ul>
</li>
<li>Minor features (unix domain sockets):
<ul>
<li>Add a new per-socket option, RelaxDirModeCheck, to allow creating Unix domain sockets without checking the permissions on the parent directory. (Tor checks permissions by default because some operating systems only check permissions on the parent directory. However, some operating systems do look at permissions on the socket, and tor's default check is unneeded.) Closes ticket 18458. Patch by weasel.
  </li>
</ul>
</li>
<li>Minor bugfixes (exit policies, security):
<ul>
<li>Refresh an exit relay's exit policy when interface addresses change. Previously, tor only refreshed the exit policy when the configured external address changed. Fixes bug 18208; bugfix on 0.2.7.3-rc. Patch by teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (security, hidden services):
<ul>
<li>Prevent hidden services connecting to client-supplied rendezvous addresses that are reserved as internal or multicast. Fixes bug 8976; bugfix on 0.2.3.21-rc. Patch by dgoulet and teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (build):
<ul>
<li>Do not link the unit tests against both the testing and non- testing versions of the static libraries. Fixes bug 18490; bugfix on 0.2.7.1-alpha.
  </li>
<li>Avoid spurious failures from configure files related to calling exit(0) in TOR_SEARCH_LIBRARY. Fixes bug 18625; bugfix on 0.2.0.1-alpha. Patch from "cypherpunks".
  </li>
<li>Silence spurious clang-scan warnings in the ed25519_donna code by explicitly initializing some objects. Fixes bug 18384; bugfix on 0.2.7.2-alpha. Patch by teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (client, bootstrap):
<ul>
<li>Count receipt of new microdescriptors as progress towards bootstrapping. Previously, with EntryNodes set, Tor might not successfully repopulate the guard set on bootstrapping. Fixes bug 16825; bugfix on 0.2.3.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (code correctness):
<ul>
<li>Update to the latest version of Trunnel, which tries harder to avoid generating code that can invoke memcpy(p,NULL,0). Bug found by clang address sanitizer. Fixes bug 18373; bugfix on 0.2.7.2-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (configuration):
<ul>
<li>Fix a tiny memory leak when parsing a port configuration ending in ":auto". Fixes bug 18374; bugfix on 0.2.3.3-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (containers):
<ul>
<li>If we somehow attempt to construct a heap with more than 1073741822 elements, avoid an integer overflow when maintaining the heap property. Fixes bug 18296; bugfix on 0.1.2.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (correctness):
<ul>
<li>Fix a bad memory handling bug that would occur if we had queued a cell on a channel's incoming queue. Fortunately, we can't actually queue a cell like that as our code is constructed today, but it's best to avoid this kind of error, even if there isn't any code that triggers it today. Fixes bug 18570; bugfix on 0.2.4.4-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (directory):
<ul>
<li>When generating a URL for a directory server on an IPv6 address, wrap the IPv6 address in square brackets. Fixes bug 18051; bugfix on 0.2.3.9-alpha. Patch from Malek.
  </li>
</ul>
</li>
<li>Minor bugfixes (fallback directory mirrors):
<ul>
<li>When requesting extrainfo descriptors from a trusted directory server, check whether it is an authority or a fallback directory which supports extrainfo descriptors. Fixes bug 18489; bugfix on 0.2.4.7-alpha. Reported by atagar, patch by teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (hidden service, client):
<ul>
<li>Handle the case where the user makes several fast consecutive requests to the same .onion address. Previously, the first six requests would each trigger a descriptor fetch, each picking a directory (there are 6 overall) and the seventh one would fail because no directories were left, thereby triggering a close on all current directory connections asking for the hidden service. The solution here is to not close the connections if we have pending directory fetches. Fixes bug 15937; bugfix on 0.2.7.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (hidden service, control port):
<ul>
<li>Add the onion address to the HS_DESC event for the UPLOADED action both on success or failure. It was previously hardcoded with UNKNOWN. Fixes bug 16023; bugfix on 0.2.7.2-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (hidden service, directory):
<ul>
<li>Bridges now refuse "rendezvous2" (hidden service descriptor) publish attempts. Suggested by ticket 18332.
  </li>
</ul>
</li>
<li>Minor bugfixes (linux seccomp2 sandbox):
<ul>
<li>Allow the setrlimit syscall, and the prlimit and prlimit64 syscalls, which some libc implementations use under the hood. Fixes bug 15221; bugfix on 0.2.5.1-alpha.
  </li>
<li>Avoid a 10-second delay when starting as a client with "Sandbox 1" enabled and no DNS resolvers configured. This should help TAILS start up faster. Fixes bug 18548; bugfix on 0.2.5.1-alpha.
  </li>
<li>Fix the sandbox's interoperability with unix domain sockets under setuid. Fixes bug 18253; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>When logging information about an unparsable networkstatus vote or consensus, do not say "vote" when we mean consensus. Fixes bug 18368; bugfix on 0.2.0.8-alpha.
  </li>
<li>Scrub service name in "unrecognized service ID" log messages. Fixes bug 18600; bugfix on 0.2.4.11-alpha.
  </li>
<li>Downgrade logs and backtraces about IP versions to info-level. Only log backtraces once each time tor runs. Assists in diagnosing bug 18351; bugfix on 0.2.8.1-alpha. Reported by sysrqb and Christian, patch by teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (memory safety):
<ul>
<li>Avoid freeing an uninitialized pointer when opening a socket fails in get_interface_addresses_ioctl(). Fixes bug 18454; bugfix on 0.2.3.11-alpha. Reported by toralf and "cypherpunks", patch by teor.
  </li>
<li>Correctly duplicate addresses in get_interface_address6_list(). Fixes bug 18454; bugfix on 0.2.8.1-alpha. Reported by toralf, patch by "cypherpunks".
  </li>
<li>Fix a memory leak in tor-gencert. Fixes part of bug 18672; bugfix on 0.2.0.1-alpha.
  </li>
<li>Fix a memory leak in "tor --list-fingerprint". Fixes part of bug 18672; bugfix on 0.2.5.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (private directory):
<ul>
<li>Prevent a race condition when creating private directories. Fixes part of bug 17852; bugfix on 0.0.2pre13. Part of ticket 17852. Patch from jsturgix. Found with Flawfinder.
  </li>
</ul>
</li>
<li>Minor bugfixes (test networks, IPv6):
<ul>
<li>Allow internal IPv6 addresses in descriptors in test networks. Fixes bug 17153; bugfix on 0.2.3.16-alpha. Patch by teor, reported by karsten.
  </li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>We no longer disable assertions in the unit tests when coverage is enabled. Instead, we require you to say --disable-asserts-in-tests to the configure script if you need assertions disabled in the unit tests (for example, if you want to perform branch coverage). Fixes bug 18242; bugfix on 0.2.7.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (time parsing):
<ul>
<li>Avoid overflow in tor_timegm when parsing dates in and after 2038 on platforms with 32-bit time_t. Fixes bug 18479; bugfix on 0.0.2pre14. Patch by teor.
  </li>
</ul>
</li>
<li>Minor bugfixes (tor-gencert):
<ul>
<li>Correctly handle the case where an authority operator enters a passphrase but sends an EOF before sending a newline. Fixes bug 17443; bugfix on 0.2.0.20-rc. Found by junglefowl.
  </li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Quote all the string interpolations in configure.ac -- even those which we are pretty sure can't contain spaces. Closes ticket 17744. Patch from zerosion.
  </li>
<li>Remove specialized code for non-inplace AES_CTR. 99% of our AES is inplace, so there's no need to have a separate implementation for the non-inplace code. Closes ticket 18258. Patch from Malek.
  </li>
<li>Simplify return types for some crypto functions that can't actually fail. Patch from Hassan Alsibyani. Closes ticket 18259.
  </li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Change build messages to refer to "Fedora" instead of "Fedora Core", and "dnf" instead of "yum". Closes tickets 18459 and 18426. Patches from "icanhasaccount" and "cypherpunks".
  </li>
</ul>
</li>
<li>Removed features:
<ul>
<li>We no longer maintain an internal freelist in memarea.c. Allocators should be good enough to make this code unnecessary, and it's doubtful that it ever had any performance benefit.
  </li>
</ul>
</li>
<li>Testing:
<ul>
<li>Fix several warnings from clang's address sanitizer produced in the unit tests.
  </li>
<li>Treat backtrace test failures as expected on FreeBSD until we solve bug 17808. Closes ticket 18204.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-167287"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-167287" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 28, 2016</p>
    </div>
    <a href="#comment-167287">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-167287" class="permalink" rel="bookmark">&quot;Tor 0.2.8.2-alpha is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Tor 0.2.8.2-alpha is released<br />
Posted March 28th, 2016 by nickm in<br />
Tor 0.2.8.1-alpha has been released!"</p>
<p>Error?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-167304"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-167304" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 28, 2016</p>
    </div>
    <a href="#comment-167304">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-167304" class="permalink" rel="bookmark">OpenSSL stays</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OpenSSL stays vulnerable!<br />
<a href="https://www.cs.tau.ac.il/~tromer/mobilesc/" rel="nofollow">https://www.cs.tau.ac.il/~tromer/mobilesc/</a></p>
<p>"OpenSSL's developers notified us that "hardware side-channel attacks are not in OpenSSL's threat model", so no updates are planned to OpenSSL 1.0.x to mitigate our attacks."</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-167361"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-167361" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 28, 2016</p>
    </div>
    <a href="#comment-167361">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-167361" class="permalink" rel="bookmark">PLEASE UPDATE DEBIAN</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>PLEASE UPDATE DEBIAN REPOSITORY.</p>
<p>apt-get update tor!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-168860"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-168860" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 05, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-167361" class="permalink" rel="bookmark">PLEASE UPDATE DEBIAN</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-168860">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-168860" class="permalink" rel="bookmark">you should be using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>you should be using backports (0.2.7.6-1~bpo8+1) if you use debian stable (0.2.5.12-1)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-167439"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-167439" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2016</p>
    </div>
    <a href="#comment-167439">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-167439" class="permalink" rel="bookmark">Thanks for all the great</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for all the great work!<br />
Will authorities go to 0.2.8 stable, too?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-167471"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-167471" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2016</p>
    </div>
    <a href="#comment-167471">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-167471" class="permalink" rel="bookmark">the first line says:
 Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the first line says:</p>
<p> Tor 0.2.8.<b>1</b>-alpha has been released!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-167513"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-167513" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2016</p>
    </div>
    <a href="#comment-167513">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-167513" class="permalink" rel="bookmark">GOOD</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>GOOD</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-167590"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-167590" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2016</p>
    </div>
    <a href="#comment-167590">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-167590" class="permalink" rel="bookmark">thanks</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-167852"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-167852" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 31, 2016</p>
    </div>
    <a href="#comment-167852">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-167852" class="permalink" rel="bookmark">I did get :</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I did get : tor_0.2.8.2-alpha-dev-20160328T151628Z.orig.tar.gz</p>
<p>And now see: tor_0.2.9.0-alpha-dev-20160329T150028Z.orig.tar.gz</p>
<p>There seems to be no reference 0.2.8.2-alpha.orig.gz available.</p>
<p>These are the sources on the deb.torproject.org server(s) which I<br />
use as reference for custom non openssl personal builds in addition<br />
to the distro guided build files.</p>
<p>I am not subscribed to developer listmail, and dislike web forum-only<br />
presentation, and rarely read them.</p>
<p>Obviously I've missed something in the last few days!</p>
<p>Advise status please. What is the significance of 0.2.9.0 ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-168053"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-168053" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 01, 2016</p>
    </div>
    <a href="#comment-168053">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-168053" class="permalink" rel="bookmark">Though some people have</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Though some people have pointed this out already, the "ARCHIVES" tab link (<a href="https://blog.torproject.org/archive" rel="nofollow">https://blog.torproject.org/archive</a>) on this blog lead to a "forbidden" (HTTP 403) error page.</p>
<p>I suppose the directory /archive/ is not used (empty?), in which case you can remove it.<br />
Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
