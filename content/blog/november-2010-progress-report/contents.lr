title: November 2010 Progress Report
---
pub_date: 2010-12-16
---
author: phobos
---
tags:

progress report
translations
enhancements
fixes
---
categories:

localization
releases
reports
---
_html_body:

<p><strong>New Releases</strong></p>

<ul>
<li>On November 16 we released the latest in the Tor -alpha series.  Tor 0.2.2.18-alpha fixes several crash bugs that have been nagging us lately, makes unpublished bridge relays able to detect their IP address, and fixes a wide variety of other bugs to get us much closer to a stable release. <a href="https://blog.torproject.org/blog/tor-02218-alpha-available" rel="nofollow">https://blog.torproject.org/blog/tor-02218-alpha-available</a></li>
<li>On November 23, we released the latest in the Tor -stable series. Tor 0.2.1.27 makes relays work with OpenSSL 0.9.8p and 1.0.0.b --yet another OpenSSL security patch broke its compatibility with Tor. We also took this opportunity to fix several crash bugs, integrate a new directory authority, and update the bundled GeoIP database. <a href="https://blog.torproject.org/blog/tor-02127-released" rel="nofollow">https://blog.torproject.org/blog/tor-02127-released</a></li>
<li>On November 21, we released the latest in the Tor -alpha series.  Yet another OpenSSL security patch broke its compatibility with Tor: Tor 0.2.2.19-alpha makes relays work with OpenSSL 0.9.8p and 1.0.0.b. <a href="https://blog.torproject.org/blog/tor-02219-alpha-out" rel="nofollow">https://blog.torproject.org/blog/tor-02219-alpha-out</a></li>
<li>On November 26, we released updated Tor Browser Bundles.  There are new<br />
browser bundles out with the updated Tor versions (0.2.2.19-alpha and 0.2.1.27)<br />
that work with the latest OpenSSL. <a href="https://blog.torproject.org/blog/new-tor-browser-bundle-packages" rel="nofollow">https://blog.torproject.org/blog/new-tor-browser-bundle-packages</a></li>
<li>On November 30th, we released the latest Arm relay monitor.  Damian writes,<br />
"What's new since August of 2009, you ask? Lots. The project has been under<br />
very active development, continuing to add usability improvements to make relay<br />
operation nicer and less error prone. If you're really curious what I've been up<br />
to this last year then it's all available in the change log."<br />
For those unfamiliar, arm is a terminal monitor for Tor relays and, to a growing<br />
extent, end users. <a href="https://blog.torproject.org/blog/arm-release-140" rel="nofollow">https://blog.torproject.org/blog/arm-release-140</a></li>
</ul>

<p><strong>Advocacy</strong></p>

<ul>
<li>We released our first ever Annual Report for 2009.  It highlights what we've accomplished in 2009.  <a href="https://www.torproject.org/about/financials.html.en" rel="nofollow">https://www.torproject.org/about/financials.html.en</a></li>
<li>Andrew presented to the U.S. Dept of Commerce Information Systems Technical<br />
Advisory Committee (ISTAC) about Tor and its relation to US Export Controls.<br />
More about ISTAC can be found here, <a href="http://tac.bis.doc.gov/ischart.htm" rel="nofollow">http://tac.bis.doc.gov/ischart.htm</a>.<br />
Andrew's presentation can be found at <a href="https://svn.torproject.org/svn/projects/presentations/2010-11-03-ISTAC-tor-circumvention-overview.pdf" rel="nofollow">https://svn.torproject.org/svn/projects/presentations/2010-11-03-ISTAC-…</a></li>
<li>Linus and Erinn attended FSCONS as a guest speaker in Sweden, <a href="http://fscons.org/" rel="nofollow">http://fscons.org/</a>.  Erinn was invited back to be a main speaker next year.  Linus' presentation can be found at <a href="https://svn.torproject.org/svn/projects/presentations/FSCONS-2010.pdf" rel="nofollow">https://svn.torproject.org/svn/projects/presentations/FSCONS-2010.pdf</a>.</li>
<li>Erinn spoke at Codebits in Portugal about Tor, Free Software, and Anonymity,<br />
<a href="http://codebits.eu/" rel="nofollow">http://codebits.eu/</a>.  Her presentation can be found at <a href="https://svn.torproject.org/svn/projects/presentations/2010-11-Codebits.pdf" rel="nofollow">https://svn.torproject.org/svn/projects/presentations/2010-11-Codebits…</a>.</li>
<li>Andrew was interviewed for Internet Evolution, <a href="http://www.internetevolution.com/radio.asp?doc_id=196479" rel="nofollow">http://www.internetevolution.com/radio.asp?doc_id=196479</a> about Tor and online anonymity.</li>
<li>Roger visited Nick Hopper's research group at UMN, <a href="http://www-users.cs.umn.edu/~hopper/" rel="nofollow">http://www-users.cs.umn.edu/~hopper/</a>.  He helped with a number of areas around a Tor simulator, how to anonymously collect Tor internal statistics, effect of guard nodes on security, how to model Tor user behavior, overlaying a censorship resistant publishing system, and some ideas on how to better count tor users anonymously.</li>
<li>Jake and Karen attended a training held with Human Rights Watch and Access.</li>
</ul>

<p><strong>Bridge relay work</strong><br />
Mike's initial research into bridges shows that we get 700-800 new bridges a day, but around 500 of them stay online and are reliable enough to give to users.  We are developing ways to create more bridges through packaging that turns users into a bridge by default, a hardware router that acts as a bridge and transparent tor proxy, and creating images for cloud computing providers such as Amazon, Rackspace, and Microsoft.  We're tracking the progress of these projects at:</p>

<ul>
<li>Bridge-by-default bundles, <a href="https://trac.torproject.org/projects/tor/milestone/Experimental%20Bridge%20Bundles" rel="nofollow">https://trac.torproject.org/projects/tor/milestone/Experimental%20Bridg…</a></li>
<li> Hardware bridge/router, <a href="https://trac.torproject.org/projects/tor/wiki/TheOnionRouter/Torouter" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/TheOnionRouter/Torouter</a>.</li>
<li>Cloud computing images, <a href="https://trac.torproject.org/projects/tor/wiki/sponsors/SponsorE/PhaseOne" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/sponsors/SponsorE/PhaseOne</a>.</li>
</ul>

<p><strong>Scalability, load balancing, directory overhead, efficiency</strong></p>

<ul>
<li>Sebastian and Nick spent some time debugging and resolving the issues with the OpenSSL changes to address a security announcement. The results of this work<br />
is included in the Tor 0.2.1.27 and 0.2.2.19-alpha releases.  Initially, we did not see any direct relevance to Tor: <a href="https://blog.torproject.org/blog/new-openssl-vulnerability-tor-not-affected" rel="nofollow">https://blog.torproject.org/blog/new-openssl-vulnerability-tor-not-affe…</a></li>
<li>Karsten implemented the new user number estimate based on directory requests to many directory mirrors in the metrics portal, <a href="https://metrics.torproject.org/users.html" rel="nofollow">https://metrics.torproject.org/users.html</a>.</li>
<li>Karsten finished tech report on estimating user numbers together with Sebastian and with very helpful comments from Roger and Thomas. The report is available on the metrics website, <a href="https://metrics.torproject.org/papers.html#techreports" rel="nofollow">https://metrics.torproject.org/papers.html#techreports</a> and the specific report is at <a href="https://metrics.torproject.org/papers/countingusers-2010-11-30.pdf" rel="nofollow">https://metrics.torproject.org/papers/countingusers-2010-11-30.pdf</a>.</li>
<li>Karsten implemented a few Tor patches, most of them related to metrics, that<br />
were kindly reviewed and merged by Nick: made log granularity configurable (#1668, 0.2.3.x); included GeoIP database identifier in extra-info descriptors (#1883, 0.2.3.x); turned on dirreq-stats by default (#2174, 0.2.3.x); fixed a bug where fast relays exceeded the 50K limit for extra-info descriptors (#2183, 0.2.2.x); reduced exit-stats to top 10 ports (#2196, 0.2.2.x).</li>
<li>Karsten implemented a few metrics website improvements: all servlets use a database connection pool for their queries; ExoneraTor uses the database tables instead of files; we upgraded to R 2.11.1 after finding out that the reshape package has a problem with large POSIXlt vectors.</li>
<li>Karsten fixed the Python version of the VisiTor script that was contributed by Kiyoto Tamura. </li>
<li>Nick did some multithreaded crypto hacking, wrote the missing code to have clients fetch microdescriptors, and spent a good while longer bughunting in Tor,<br />
particularly for issues related to brokenness in and around openssl.</li>
<li>Nick also spent a while chasing down more lingering Libevent bugs that don't affect Tor directly; having Libevent useful for more people means that we get more contributors for the Libevent codebase, which in turn helps Tor indirectly.  Tor is heavily dependent on libevent, therefore the better libevent becomes, the better Tor becomes.</li>
</ul>

<p><strong>Translations</strong><br />
We are migrating all translation work over to <a href="https://www.transifex.net/projects/p/torproject/" rel="nofollow">https://www.transifex.net/projects/p/torproject/</a>. Runa spent the month preparing the po/pot files for migration, committing translations still residing in pootle at translation.torproject.org.</p>

