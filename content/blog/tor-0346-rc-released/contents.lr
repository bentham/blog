title: New Release: Tor 0.3.4.6-rc
---
pub_date: 2018-08-08
---
author: nickm
---
tags: release candidate
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for Tor 0.3.4.6-rc from the <a href="https://www.torproject.org/download/download.html">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release by some time next month.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.3.4.6-rc fixes several small compilation, portability, and correctness issues in previous versions of Tor. This version is a release candidate: if no serious bugs are found, we expect that the stable 0.3.4 release will be (almost) the same as this release.</p>
<h2>Changes in version 0.3.4.6-rc - 2018-08-06</h2>
<ul>
<li>Major bugfixes (event scheduler):
<ul>
<li>When we enable a periodic event, schedule it in the event loop rather than running it immediately. Previously, we would re-run periodic events immediately in the middle of (for example) changing our options, with unpredictable effects. Fixes bug <a href="https://bugs.torproject.org/27003">27003</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor features (compilation):
<ul>
<li>When building Tor, prefer to use Python 3 over Python 2, and more recent (contemplated) versions over older ones. Closes ticket <a href="https://bugs.torproject.org/26372">26372</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the July 3 2018 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/26674">26674</a>.</li>
</ul>
</li>
<li>Minor features (Rust, portability):
<ul>
<li>Rust cross-compilation is now supported. Closes ticket <a href="https://bugs.torproject.org/25895">25895</a>.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix a compilation warning on some versions of GCC when building code that calls routerinfo_get_my_routerinfo() twice, assuming that the second call will succeed if the first one did. Fixes bug <a href="https://bugs.torproject.org/26269">26269</a>; bugfix on 0.2.8.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (controller):
<ul>
<li>Report the port correctly when a port is configured to bind to "auto". Fixes bug <a href="https://bugs.torproject.org/26568">26568</a>; bugfix on 0.3.4.1-alpha.</li>
<li>Parse the "HSADDRESS=" parameter in HSPOST commands properly. Previously, it was misparsed and ignored. Fixes bug <a href="https://bugs.torproject.org/26523">26523</a>; bugfix on 0.3.3.1-alpha. Patch by "akwizgran".</li>
</ul>
</li>
<li>Minor bugfixes (correctness, flow control):
<ul>
<li>Upon receiving a stream-level SENDME cell, verify that our window has not grown too large. Fixes bug <a href="https://bugs.torproject.org/26214">26214</a>; bugfix on svn r54 (pre-0.0.1)</li>
</ul>
</li>
<li>Minor bugfixes (memory, correctness):
<ul>
<li>Fix a number of small memory leaks identified by coverity. Fixes bug <a href="https://bugs.torproject.org/26467">26467</a>; bugfix on numerous Tor versions.</li>
</ul>
</li>
<li>Minor bugfixes (portability):
<ul>
<li>Avoid a compilation error in test_bwmgt.c on Solaris 10. Fixes bug <a href="https://bugs.torproject.org/26994">26994</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing, compatibility):
<ul>
<li>When running the ntor_ref.py and hs_ntor_ref.py tests, make sure only to pass strings (rather than "bytes" objects) to the Python subprocess module. Python 3 on Windows seems to require this. Fixes bug <a href="https://bugs.torproject.org/26535">26535</a>; bugfix on 0.2.5.5-alpha (for ntor_ref.py) and 0.3.1.1-alpha (for hs_ntor_ref.py).</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-276339"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276339" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 08, 2018</p>
    </div>
    <a href="#comment-276339">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276339" class="permalink" rel="bookmark">Thanks for this.
I am just…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for this.<br />
I am just curious: are there going to be new features in the 0.3.4 or is this going to be "just" a maintenance release? (I mean bug fixes and stuffs like these)</p>
<p>Cheers</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276340"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276340" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 08, 2018</p>
    </div>
    <a href="#comment-276340">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276340" class="permalink" rel="bookmark">Littlebit offtopic:
TBB…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Littlebit offtopic:</p>
<p>TBB isnt for big downloads but, keep2share(k2s.cc) isnt working with NoScript5.1.8.6 on. ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276343"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276343" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 08, 2018</p>
    </div>
    <a href="#comment-276343">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276343" class="permalink" rel="bookmark">Please, fix consdiff cpu…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please, fix consdiff cpu overload bug on Windows at least in 0.3.5 as it's a new LTS. Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276344"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276344" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>AnonyON (not verified)</span> said:</p>
      <p class="date-time">August 08, 2018</p>
    </div>
    <a href="#comment-276344">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276344" class="permalink" rel="bookmark">Thanks Nick.........</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks Nick.........</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276352"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276352" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 09, 2018</p>
    </div>
    <a href="#comment-276352">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276352" class="permalink" rel="bookmark">When I start Tbb 7.5.6, the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When I start Tbb 7.5.6, the browser may show the list of last time tabs as if it has closed unproperly</p>
</div>
  </div>
</article>
<!-- Comment END -->
