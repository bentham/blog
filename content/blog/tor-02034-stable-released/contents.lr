title: Tor 0.2.0.34-stable released
---
pub_date: 2009-02-09
---
author: phobos
---
tags:

stable release
bug fixes
security fixes
---
categories: releases
---
_html_body:

<p>Tor 0.2.0.34 features several more security-related fixes. You<br />
should upgrade, especially if you run an exit relay (remote crash) or<br />
a directory authority (remote infinite loop), or you're on an older<br />
(pre-XP) or not-recently-patched Windows (remote exploit).</p>

<p>This release marks end-of-life for Tor 0.1.2.x. Those Tor versions have<br />
many known flaws, and nobody should be using them. You should upgrade. If<br />
you're using a Linux or BSD and its packages are obsolete, stop using<br />
those packages and upgrade anyway.</p>

<p><a href="https://www.torproject.org/download.html" rel="nofollow">https://www.torproject.org/download.html</a></p>

<p>Changes in version 0.2.0.34 - 2009-02-08<br />
<strong>Security fixes:</strong></p>

<ul>
<li>Fix an infinite-loop bug on handling corrupt votes under certain<br />
      circumstances. Bugfix on 0.2.0.8-alpha.</li>
<li>Fix a temporary DoS vulnerability that could be performed by<br />
      a directory mirror. Bugfix on 0.2.0.9-alpha; reported by lark.</li>
<li>Avoid a potential crash on exit nodes when processing malformed<br />
      input. Remote DoS opportunity. Bugfix on 0.2.0.33.</li>
<li>Do not accept incomplete ipv4 addresses (like 192.168.0) as valid.<br />
      Spec conformance issue. Bugfix on Tor 0.0.2pre27.</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>Fix compilation on systems where time_t is a 64-bit integer.<br />
      Patch from Matthias Drochner.</li>
<li>Don't consider expiring already-closed client connections. Fixes<br />
      bug 893. Bugfix on 0.0.2pre20.</li>
</ul>

<p>The original announcement can be found at <a href="http://archives.seul.org/or/announce/Feb-2009/msg00000.html" rel="nofollow">http://archives.seul.org/or/announce/Feb-2009/msg00000.html</a></p>

---
_comments:

<a id="comment-1066"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1066" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous - maby not (not verified)</span> said:</p>
      <p class="date-time">April 26, 2009</p>
    </div>
    <a href="#comment-1066">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1066" class="permalink" rel="bookmark">Old version nodes still running</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"This release marks end-of-life for Tor 0.1.2.x."</p>
<p>If this is true why do you still allow nodes to run old versions?<br />
version 0.2.1.19 that is known to have been used by nodes that spyed on traffic is still used by 6% of all nodes, even earlier versions are run by 7% of the nodes, when are you gonna stop the possibility to run a node on these old versions(theres really no good explanation why nodes like Kryten or TSL with 4800KB bandwidth still is running an old 0.1.2.19 version).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1067"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1067" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 26, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1066" class="permalink" rel="bookmark">Old version nodes still running</a> by <span>Anonymous - maby not (not verified)</span></p>
    <a href="#comment-1067">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1067" class="permalink" rel="bookmark">typo, should be 0.1.2.19 and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>typo, should be 0.1.2.19 and not 0.2.1.19.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
