title: Tor Messenger 0.1.0b6 is released
---
pub_date: 2016-04-06
---
author: sukhbir
---
tags:

instant messenger
Tor Messenger
chat client
---
categories:

applications
releases
---
_html_body:

<p>We are pleased to announce another public beta release of Tor Messenger. This release features important <a href="https://www.mozilla.org/en-US/security/known-vulnerabilities/thunderbird/" rel="nofollow">security updates</a> to Instantbird. All users are highly encouraged to upgrade.</p>

<h3>Mozilla's ESR cycle</h3>

<p>This release of Tor Messenger is the first release based on <a href="https://www.mozilla.org/en-US/firefox/organizations/faq/" rel="nofollow">Mozilla's ESR cycle</a>.  As with Tor Browser, all future releases will continue to pair with this cycle.</p>

<h3>Secure Updater</h3>

<p>We are well aware of the current pain in upgrading Tor Messenger and are actively working towards porting Tor Browser's updater patches (<a href="https://bugs.torproject.org/14388" rel="nofollow">#14388</a>) so that keeping Tor Messenger up to date is as seamless and easy as possible. We continue to apologize for the inconvenience.</p>

<h3>Before upgrading, back up your OTR keys</h3>

<p>Before upgrading to the new release, you will need to back up your OTR keys or simply generate new ones. Please see the following steps to <a href="https://trac.torproject.org/projects/tor/wiki/doc/TorMessenger/FAQ#WherearemyOTRkeysstoredHowcanIpreservethemacrossupdates" rel="nofollow">back them up</a>.</p>

<h3>Downloads</h3>

<p>Please note that Tor Messenger is <b>still in beta</b>. The purpose of this release is to help test the application and provide feedback. <b><i>At-risk users should not depend on it for their privacy and safety</i></b>.</p>

<p><a href="https://dist.torproject.org/tormessenger/0.1.0b6/tor-messenger-linux32-0.1.0b6_en-US.tar.xz" rel="nofollow">Linux (32-bit)</a></p>

<p><a href="https://dist.torproject.org/tormessenger/0.1.0b6/tor-messenger-linux64-0.1.0b6_en-US.tar.xz" rel="nofollow">Linux (64-bit)</a></p>

<p><a href="https://dist.torproject.org/tormessenger/0.1.0b6/tormessenger-install-0.1.0b6_en-US.exe" rel="nofollow">Windows</a></p>

<p><a href="https://dist.torproject.org/tormessenger/0.1.0b6/TorMessenger-0.1.0b6-osx64_en-US.dmg" rel="nofollow">OS X (Mac)</a></p>

<p><a href="https://dist.torproject.org/tormessenger/0.1.0b6/sha256sums.txt" rel="nofollow">sha256sums.txt</a><br />
<a href="https://dist.torproject.org/tormessenger/0.1.0b6/sha256sums.txt.asc" rel="nofollow">sha256sums.txt.asc</a></p>

<p>The <span class="geshifilter"><code class="php geshifilter-php">sha256sums<span style="color: #339933;">.</span>txt</code></span> file containing hashes of the bundles is signed with the key <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #208080;">0x6887935AB297B391</span></code></span> (fingerprint: <span class="geshifilter"><code class="php geshifilter-php">3A0B 3D84 <span style="color: #cc66cc;">3708</span> <span style="color: #cc66cc;">9613</span> 6B84 <span style="color:#800080;">5E82</span> <span style="color: #cc66cc;">6887</span> 935A B297 B391</code></span>).</p>

<h3> Changelog </h3>

<p>Here is the complete <a href="https://gitweb.torproject.org/tor-messenger-build.git/tree/ChangeLog" rel="nofollow">changelog</a> since v0.1.0b5:</p>

<p>Tor Messenger 0.1.0b6 -- April 06, 2016</p>

<ul>
<li>All Platforms
<ul>
<li>Use the THUNDERBIRD_45_0b3_RELEASE tag on mozilla-esr45</li>
<li>Use the THUNDERBIRD_45_0b3_RELEASE tag on comm-esr45</li>
<li><a href="https://bugs.torproject.org/18533" rel="nofollow">Bug 18533</a>: Disable sending fonts or colors as part of messages</li>
<li>ctypes-otr
<ul>
<li><a href="https://github.com/arlolra/ctypes-otr/issues/68" rel="nofollow">GH 68</a>: Don't close notification bar until verification succeeds (patch by Elias Rohrer)
            </li>
<li><a href="https://github.com/arlolra/ctypes-otr/issues/71" rel="nofollow">GH 71</a>: Improve verifying from the fingerprint manager  (patch by Vu Quoc Huy)
            </li>
<li><a href="https://github.com/arlolra/ctypes-otr/issues/72" rel="nofollow">GH 72</a>: Generate keys automatically after account creation (patch by Vu Quoc Huy)
        </li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-170448"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-170448" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 13, 2016</p>
    </div>
    <a href="#comment-170448">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-170448" class="permalink" rel="bookmark">Little bit off-topic but</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Little bit off-topic but OpenSSL:</p>
<p>An Analysis of OpenSSL's Random Number Generator<br />
<a href="https://eprint.iacr.org/2016/367" rel="nofollow">https://eprint.iacr.org/2016/367</a><br />
In this work we demonstrate various weaknesses of the random number generator (RNG) in the OpenSSL cryptographic library.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-170706"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-170706" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  yawning
  </article>
    <div class="comment-header">
      <p class="comment__submitted">yawning said:</p>
      <p class="date-time">April 14, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-170448" class="permalink" rel="bookmark">Little bit off-topic but</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-170706">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-170706" class="permalink" rel="bookmark">Way off topic.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Way off topic. <a href="https://trac.torproject.org/projects/tor/ticket/17799" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/17799</a> will fix this, though I owe it another round of review.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-170727"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-170727" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 14, 2016</p>
    </div>
    <a href="#comment-170727">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-170727" class="permalink" rel="bookmark">why would i need to install</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>why would i need to install this on my macbook pro if there is nothing i can't  use this site for</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-172795"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-172795" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 23, 2016</p>
    </div>
    <a href="#comment-172795">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-172795" class="permalink" rel="bookmark">Following the instructions</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Following the instructions for migrating my key and I've run into a problem:</p>
<p>On the old version for OS X, the filepath <span class="geshifilter"><code class="php geshifilter-php">Tor Messenger<span style="color: #339933;">.</span>app<span style="color: #339933;">/</span>Contents<span style="color: #339933;">/</span>TorMessenger<span style="color: #339933;">/</span>Data<span style="color: #339933;">/</span>Browser<span style="color: #339933;">/</span><span style="color: #009900;">&#91;</span>profile<span style="color: #009900;">&#93;</span><span style="color: #339933;">.</span><span style="color: #b1b100;">default</span><span style="color: #339933;">/</span></code></span><br />
worked, but now <span class="geshifilter"><code class="php geshifilter-php">Tor Messenger<span style="color: #339933;">.</span>app<span style="color: #339933;">/</span>Contents</code></span> and <span class="geshifilter"><code class="php geshifilter-php">Tor Messenger<span style="color: #339933;">.</span>app<span style="color: #339933;">/</span>TorMessenger<span style="color: #339933;">/</span></code></span> are both folders within the original directory, so I can't follow the original filepath—it doesn't exist any longer, apparently.</p>
<p>Please advise.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-172806"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-172806" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 23, 2016</p>
    </div>
    <a href="#comment-172806">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-172806" class="permalink" rel="bookmark">I posted an earlier comment</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I posted an earlier comment asking for help &amp; filepaths, but I figured it out. You have to open Tor Messenger first, and connect with your acct (I only tried it with Jabber/XMPP), then close it and you can move your otr.fingerprints and otr.private_key files back in.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-174105"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-174105" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 27, 2016</p>
    </div>
    <a href="#comment-174105">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-174105" class="permalink" rel="bookmark">Thank you very much for TOR</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you very much for TOR !!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-177826"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-177826" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 05, 2016</p>
    </div>
    <a href="#comment-177826">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-177826" class="permalink" rel="bookmark">Just a quick question (and I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just a quick question (and I think it would be worth to include that info with such release announcements, at least during this alpha/beta phase): can it be used along Tor Browser without conflicts?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-179998"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-179998" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 17, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-177826" class="permalink" rel="bookmark">Just a quick question (and I</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-179998">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-179998" class="permalink" rel="bookmark">Yes.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-178379"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-178379" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 07, 2016</p>
    </div>
    <a href="#comment-178379">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-178379" class="permalink" rel="bookmark">So you release a software to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So you release a software to the public and say you can use instant messengers  one of them being Facebook, as you guys said in the description for this software and now, its NOT SHOWING the Facebook protocol, it shows everything else but not the Facebook protocol like you said!  Nothing like getting my hopes up to talk to all of my FB  contacts more securely, Not.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-179536"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-179536" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 15, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-178379" class="permalink" rel="bookmark">So you release a software to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-179536">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-179536" class="permalink" rel="bookmark">From the blog post on 9</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>From the blog post on 9 March 2016 announcing the last release:<br />
" Facebook support dropped</p>
<p>"Facebook has long officially deprecated their XMPP gateway, and it doesn't appear to work anymore. We had multiple reports from users about this issue and decided that it was best to remove support for Facebook from Tor Messenger.</p>
<p>"We hear that an implementation of the new mqtt based protocol is in the works, so we hope to restore this functionality in the future."<br />
<a href="https://blog.torproject.org/blog/tor-messenger-010b5-released" rel="nofollow">https://blog.torproject.org/blog/tor-messenger-010b5-released</a><br />
"</p>
<p>This sounds like it's more facebook's fault than the Tor Messenger devs.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-178931"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-178931" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 10, 2016</p>
    </div>
    <a href="#comment-178931">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-178931" class="permalink" rel="bookmark">ok i need help i am using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ok i need help i am using android and cant figure out how to get to the wiki</p>
</div>
  </div>
</article>
<!-- Comment END -->
