title: Some statistics about onions
---
pub_date: 2015-02-26
---
author: asn
---
tags:

hidden services
statistics
onions
---
categories: onion services
---
_html_body:

<p><b>Non-technical abstract:</b></p>

<p><i><br />
We are starting a project to study and quantify hidden services traffic. As part of this project, we are collecting data from just a few volunteer relays which only allow us to see a small portion of hidden service activity (between 2% and 5%). Extrapolating from such a small sample is difficult, and our data are preliminary. </i></p>

<p>We've been working on methods to improve our calculations, but with our current methodology, we estimate that about 30,000 hidden services announce themselves to the Tor network every day, using about 5 terabytes of data daily. We also found that hidden service traffic is about 3.4% of total Tor traffic, which means that, at least according to our early calculations, 96.6% of Tor traffic is *not* hidden services. We invite people to join us in working to research methodologies and develop systems for better understanding Tor hidden services.<br />
</p>

<p>Hello,</p>

<p>Over the past months we've been working on hidden service statistics. Our goal has been to answer the following questions:</p>

<ul>
<li><b>"Approximately how many hidden services are there?"</b></li>
<li><b>"Approximately how much traffic of the Tor network is going to hidden services?"</b></li>
</ul>

<p>We chose the above two questions because even though we want to understand hidden services, we really don't want to harm the privacy of Tor users. From a privacy perspective, the above two questions are relatively easy questions to answer because we don't need data from clients or the hidden services themselves; we just need data from hidden service directories and rendezvous points. Furthermore, the measurements reported by each relay cannot be linked back to specific hidden services or their clients.</p>

<p>Our first move was to <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007816.html" rel="nofollow">research various ways we could collect these statistics in a privacy-preserving manner</a>. After <a href="https://lists.torproject.org/pipermail/tor-dev/2014-December/007911.html" rel="nofollow">days of discussions on obfuscating statistics</a>, we began writing <a href="https://gitweb.torproject.org//torspec.git/tree/proposals/238-hs-relay-stats.txt" rel="nofollow">a Tor proposal</a> with our design, as well as <a href="https://bugs.torproject.org/13192" rel="nofollow">code that implements the proposal</a>. The code has since been reviewed and <a href="https://blog.torproject.org/blog/tor-0262-alpha-released" rel="nofollow">merged to Tor</a>! The statistics are currently disabled by default so <a href="https://lists.torproject.org/pipermail/tor-relays/2014-December/005953.html" rel="nofollow">we asked volunteer relay operators to explicitly turn them on</a>. Currently there are about 70 relays publishing measurements to us every 24 hours:</p>

<p><img alt="Number of relays reporting stats" src="https://extra.torproject.org/blog/2015-02-26-some-statistics-about-onions/num-reported-stats.png" width="100%" /></p>

<p>So as of now we've been receiving these measurements for over a month, and we have thought a lot about how to best use the reported measurements to derive interesting results. We finally have some <i>preliminary results</i> we would like to share with you:</p>

<h2>How many hidden services are there?</h2>

<p>All in all, it seems that every day about 30000 hidden services announce themselves to the hidden service directories. Graphically:</p>

<p><img alt="Number of hidden services" src="https://extra.torproject.org/blog/2015-02-26-some-statistics-about-onions/extrapolated-onions.png" width="100%" /></p>

<p>By <i>counting the number of unique hidden service addresses seen by HSDirs</i>, we can get the approximate number of hidden services. Keep in mind that we can only see between 2% and 5% of the total HSDir space, so the extrapolation is, naturally, messy.</p>

<h2>How much traffic do hidden services cause?</h2>

<p>Our preliminary results show that hidden services cause somewhere <i>between 400 to 600 Mbit of traffic per second</i>, or equivalently about <i>4.9 terabytes a day</i>. Here is a graph:</p>

<p><img alt="Hidden services traffic volume" src="https://extra.torproject.org/blog/2015-02-26-some-statistics-about-onions/extrapolated-cells.png" width="100%" /></p>

<p>We learned this by getting rendezvous points to publish the total number of cells transferred over rendezvous circuits, which allows us to learn the approximate volume of hidden service traffic. Notice that our coverage here is not very good either, with a probability of about 5% that a hidden service circuit will use a relay that reports these statistics as a rendezvous point.</p>

<p>A related statistic here is <i>"How much of the Tor network is actually hidden service usage?"</i>. There are <a href="https://lists.torproject.org/pipermail/tor-dev/2015-February/008249.html" rel="nofollow">two different ways</a> to answer this question, depending on whether we want to understand what clients are doing or what the network is doing. The fraction of hidden-service traffic at Tor clients differs from the fraction at Tor relays because connections to hidden services use 6-hop circuits while connections to the regular Internet use 3-hop circuits. As a result, the fraction of hidden-service traffic entering or leaving Tor is about half of the fraction of hidden-service traffic inside of Tor. Our conclusion is that about <i>3.4% of client traffic is hidden-service traffic, and 6.1% of traffic seen at a relay is hidden-service traffic</i>.</p>

<h1>Conclusion and future work</h1>

<p>In this blog post we presented some preliminary results that could be extracted from these new hidden service statistics. We hope that this data can help us better gauge the future development and maturity of the <i>onion space</i> as well as detect potential incidents and bugs on the network. To better present our results and methods, <a href="https://research.torproject.org/techreports/extrapolating-hidserv-stats-2015-01-31.pdf" rel="nofollow">we wrote a short technical report</a> that outlines the exact process we followed. We invite you to read it if you are curious about the methodology or the results.</p>

<p>Finally, this project is only a few months old, and there are various plans for the future. For example:</p>

<ul>
<li>
There are more interesting questions that we could examine in this area. For example: "How many people are using hidden services every day?" and "How many times does someone try to visit a hidden service that does not exist anymore?."
<p>Unfortunately, some of these questions are not easy to answer with the current statistics reporting infrastructure, mainly because collecting them in this way could reveal information about specific hidden services but also because the results of the current system contain too much obfuscating data (each reporting relay randomizes its numbers a little bit before publishing them, so we can learn about totals but not about specific events).</p>
<p>For this reason, <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008086.html" rel="nofollow">we've been analyzing various statistics aggregation protocols</a> that could be used in place of the current system, allowing us to safely collect other kinds of statistics.</p></li>
<li>
We need to incorporate these statistics in <a href="https://metrics.torproject.org/" rel="nofollow">our Metrics portal</a> so that they are updated regularly and so that everyone can follow them.</li>
<li>
Currently, these hidden service statistics are not collected in relays by default. Unfortunately, that gives us very small coverage of the network, which in turn makes our extrapolations very <a href="https://en.wikipedia.org/wiki/Noise_%28signal_processing%29" rel="nofollow">noisy</a>. The main reason that these statistics are disabled by default is that similar statistics are also disabled (e.g. CellStatistics). Also, this allows us more time to consider privacy consequences. As we analyze more of these statistics and think more about statistics privacy, we should decide whether to turn these statistics on by default.
<p>It's worth repeating that the current results are preliminary and should be digested with a grain of salt. We invite statistically-inclined people to review our code, methods, and results. If you are a researcher interested in digging into the measurements themselves, you can find them in the <a href="https://collector.torproject.org/archive/relay-descriptors/extra-infos/" rel="nofollow">extra-info descriptors of Tor relays</a>.</p>
<p>Over the next months, we will also be thinking more about these problems to figure out proper ways to analyze and safely measure private ecosystems like the onion space.</p></li>
</ul>

<p>Till then, take care, and enjoy Tor!</p>

---
_comments:

<a id="comment-88870"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-88870" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 26, 2015</p>
    </div>
    <a href="#comment-88870">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-88870" class="permalink" rel="bookmark">Unfortunately is the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Unfortunately is the procedure at <a href="https://www.torproject.org/docs/debian" rel="nofollow">https://www.torproject.org/docs/debian</a> in order to help you as a volunteer is not working:</p>
<p>After the command "gpg --keyserver keys.gnupg.net --recv 886DDD89", here is what i have:<br />
<div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">gpg <span style="color: #339933;">--</span>keyserver keys<span style="color: #339933;">.</span>gnupg<span style="color: #339933;">.</span>net <span style="color: #339933;">--</span>recv 886DDD89<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">gpg<span style="color: #339933;">:</span> requesting <a href="http://www.php.net/key"><span style="color: #990000;">key</span></a> 886DDD89 from hkp server keys<span style="color: #339933;">.</span>gnupg<span style="color: #339933;">.</span>net<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">?<span style="color: #339933;">:</span> keys<span style="color: #339933;">.</span>gnupg<span style="color: #339933;">.</span>net<span style="color: #339933;">:</span> Host not found<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">gpgkeys<span style="color: #339933;">:</span> HTTP fetch error <span style="color: #cc66cc;">7</span><span style="color: #339933;">:</span> couldn<span style="color: #0000ff;">'t connect: No such file or directory&lt;br /&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #0000ff;">gpg: no valid OpenPGP data found.&lt;br /&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #0000ff;">gpg: Total number processed: 0&lt;br /&gt;</span></div></li></ol></pre></div></p>
<p><strong>fix it please</strong></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-88913"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-88913" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 27, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-88870" class="permalink" rel="bookmark">Unfortunately is the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-88913">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-88913" class="permalink" rel="bookmark">Look at the error messages</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Look at the error messages carefully, "keys.gnupg.net: Host not found" sounds like a network problem on your end.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-88876"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-88876" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 26, 2015</p>
    </div>
    <a href="#comment-88876">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-88876" class="permalink" rel="bookmark">I&#039;ve a better idea for you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've a better idea for you to work on.</p>
<p>In addition to just collecting statistics, could you invite some black-hat hackers to hack the Tor network to see how resilient it is against attacks?</p>
<p>Or, you could employ a few experts to conduct a security audit of the products developed by Tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-88952"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-88952" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 27, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-88876" class="permalink" rel="bookmark">I&#039;ve a better idea for you</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-88952">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-88952" class="permalink" rel="bookmark">Wow, they probably never</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wow, they probably never thought about that. /s</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89147"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89147" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 01, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-88952" class="permalink" rel="bookmark">Wow, they probably never</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89147">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89147" class="permalink" rel="bookmark">LOL! Well, someone call the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>LOL! Well, someone call the hackers asap!!!</p>
<p>Of course, I'm using a Mac so it's not like I have anything to worry about. Macs are invincible!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-88915"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-88915" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 27, 2015</p>
    </div>
    <a href="#comment-88915">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-88915" class="permalink" rel="bookmark">TORPROJECT won the reddit</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TORPROJECT won the reddit donation of nearly $83K. Make sure the founders/developers work out the details with reddit team via <a href="mailto:redditdonate@reddit.com" rel="nofollow">redditdonate@reddit.com</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-88944"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-88944" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 27, 2015</p>
    </div>
    <a href="#comment-88944">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-88944" class="permalink" rel="bookmark">My Friends in iran says :all</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My Friends in iran says :all Anti-Filter softwares , VPN's (Open vpn ,PPTP,L2TP) are  down .and also Tor does Not work in some area . The Government has been blocked some Bridges . i saw the Tor Metrics .it seems Tor users From Iran have been Increased...<br />
Please Put New bridges !</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89030"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89030" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 28, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-88944" class="permalink" rel="bookmark">My Friends in iran says :all</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89030">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89030" class="permalink" rel="bookmark">i&#039;m from iran with Tails and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i'm from iran with Tails and obfs3 bridge :)  . and i don't have any issue . use obfs3 bridge <a href="https://bridges.torproject.org/" rel="nofollow">https://bridges.torproject.org/</a> . some bridges are very slow and you should get more than 3 bridge from above links . good luck</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89434"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89434" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 07, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89030" class="permalink" rel="bookmark">i&#039;m from iran with Tails and</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89434">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89434" class="permalink" rel="bookmark">Tails 1.3 includes the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tails 1.3 includes the pluggable transport obfs4. You might give obfs4 a try if nothing else works.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-89080"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89080" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 28, 2015</p>
    </div>
    <a href="#comment-89080">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89080" class="permalink" rel="bookmark">I am an iranian atheist.
I&#039;m</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am an iranian atheist.<br />
I'm a blogger.<br />
the tor is helpful for me.<br />
your  efforts  for freedom is  admirable.<br />
thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-89085"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89085" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 28, 2015</p>
    </div>
    <a href="#comment-89085">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89085" class="permalink" rel="bookmark">there is no way we can</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>there is no way we can connect to tor in Iran anymore its more than 10 days that even bridges don't do there job here, by the way tor bundle surprisingly still works.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-89162"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89162" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 02, 2015</p>
    </div>
    <a href="#comment-89162">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89162" class="permalink" rel="bookmark">how would you set up your</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>how would you set up your own hidden service?  using TBB is easy, but what webservers or http libraries can be relied on not to disclose your identity?  its a curious gap in the ease of use of tor that you have to figure that out for yourself and might make a mistake</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89217"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89217" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 03, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89162" class="permalink" rel="bookmark">how would you set up your</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89217">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89217" class="permalink" rel="bookmark">I&#039;ve used Boa, OpenBSD&#039;s</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've used Boa, OpenBSD's httpd, Nginx, and Apache.</p>
<p>Boa<br />
Boa is a good webserver for super simple entirely static web pages (it only supports serving static files, no CGI etc). It is /extremely/ small and very fast. The small code size also means it has very few vulnerabilities, especially very few information leaks. If you have absolutely 0 experience with web servers, Boa will still be very easy to use. I highly recommend it if you only want to serve static content (static content isn't just HTML, you can also serve HTML5, CSS, JavaScript, etc. Just no server-side languages like PHP).</p>
<p>lighthttpd<br />
I have heard good things about lighthttpd (it should be able to do most of what you would want to put on a website), because it's small, secure, and fast. That's only an anecdotal recommendation though, I haven't played with lighthttpd myself.</p>
<p>OpenBSD's httpd<br />
If you are using OpenBSD, their httpd is a step up from Boa in features It's fairly immature, with only basic CGI and FastCGI support, primitive TLS support, and a very simple config file, but it should be more complete in the future. The OpenBSD team has a reputation for very thorough code auditing, so their code is exceptionally high quality. If the famous OpenSSH is anything to go by, their httpd should be quite solid as well.</p>
<p>Nginx<br />
And of course the overall best if you need advanced features or high quality documentation is Nginx (the main competitor to the terrible Apache webserver). If you want to minimize information leakage (e.g. the daemon being tricked into giving up info like your hostname or more), then go with something ultra simple like Boa or OpenBSD's httpd. If you need more advanced features, stick with Nginx but read up on how to make it secure (specifically secure against information leaks).</p>
<p>Apache<br />
Do not use Apache. It is more vulnerable to denial of service attacks (especially because it spawns an entire new worker process for /every/ new TCP connection), as well as information disclosure and remote code execution exploits.</p>
<p>All of those web servers can be easily pointed at the localhost address your hidden service forwards to. I suggest you have the HS port be port 80, both externally and on localhost, to avoid some annoying bugs that many webservers can cause when the external port != the port the daemon listens to.</p>
<p>tl;dr use Boa if you are only serving static content, use Nginx otherwise. If you are using OpenBSD or want to try it, use their custom httpd.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89318"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89318" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 05, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89217" class="permalink" rel="bookmark">I&#039;ve used Boa, OpenBSD&#039;s</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89318">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89318" class="permalink" rel="bookmark">Anyway you must communicate</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Anyway you must communicate onion address of your site so you can use any port not only 80. "well known port" != "port must be used".<br />
If you are going to have public web server try to restrict information about used software and os. Though it is true that tor prevents ip/tcp level fingerprinting, your web server can happily announce that it is windows 98 it is running on.<br />
If it is a private web you can use an old microsoft trick - ask users to set useragent like "It's me JackTheRipper" and filter requests accordingly. I believe palemoon browser has an option to set customized ua strings for distinct sites.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-89442"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89442" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 07, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89162" class="permalink" rel="bookmark">how would you set up your</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89442">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89442" class="permalink" rel="bookmark">I believe I heard Roger</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I believe I heard Roger Dingledine say something about spinning up instances in Twisted becoming the norm for onion web services. I would like to learn more.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-89315"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89315" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 05, 2015</p>
    </div>
    <a href="#comment-89315">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89315" class="permalink" rel="bookmark">I&#039;m a bit surprised that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm a bit surprised that nobody has made a comment about the actual point of the article. Hey, guys, aren't you at all interested in knowing what happens in the tor space ? I mean, I find this study, despite the small means they had, and the statistical irregularities that appear (due to the small amount of data, not to the work of the researchers), quite interesting. I used Tor during a long time, some years ago, and when I came back, my first question was : is there really something left in the hidden part, or is it now just a playground for teenagers looking for some adrenaline and scams of drugs-selling or weapons-selling websites ?<br />
And this is a good way to answer this question, without, so far, attacking anyone's privacy.<br />
I may have some skills about statistical studies, and I'd be happy to put them in good use. If someone is ready to discuss, on a technical and theoretical point of view, the pertinency of this study, and interprate, as much as it is possible, its results, I'd be glad to have a talk about it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-89446"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-89446" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 07, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-89315" class="permalink" rel="bookmark">I&#039;m a bit surprised that</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-89446">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-89446" class="permalink" rel="bookmark">Well it&#039;s not just &quot;scams of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well it's not just "scams of drugs-selling or weapons-selling websites" but serious agencies collecting reports from agents by the way...<br />
As for statistics are you really sure it should be interesting for tor users? How can this research be used for increasing security of tor network? And as you can see the researchers themselves are afraid of using tor HS for presenting their results! btw there can be possible outcome of such statistics as for example a signal for a new raid on hs operators! Researchers are mad on their researches and usually absolutely don't care and can't predict outcome of their researches.<br />
And of course some relay operators always use patched tor code for different purposes. I'm sure in the researchers' case the operators review such patches.<br />
As i see it is more interesting how the network can prevent attempts to analyze its behavior, detect modified nodes and mark them as potentially unsafe (say - experimental) in consensus for allowing users to avoid such relays.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-92241"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-92241" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 13, 2015</p>
    </div>
    <a href="#comment-92241">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-92241" class="permalink" rel="bookmark">I would like to see the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I would like to see the numbers going to known CP sites and maybe some way to try block them or even just let their info leak so they get arrested and have their ass well pounded in prison.<br />
Another option is to let me know any pedo's and I'll gladly end those kids suffering by seeing what these guys do when confronted by a man who is most likely bigger and stronger than them(6'4" active Muay Thai boxer), its usually guys but I'd have a different approach to women, I'd make them see how the kids feel, you know what? Just for the kids they abused I'd put myself threw raping a guy, sure I'd not like it in the slightest but this isn't about me, its these kids that have their innocence taken at very young age, I can tell you from experience it is a life altering experience for most and some are as young as 3, 4, 5 years of age.</p>
</div>
  </div>
</article>
<!-- Comment END -->
