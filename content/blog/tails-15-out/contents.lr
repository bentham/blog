title: Tails 1.5 is out
---
pub_date: 2015-08-11
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 1.5, is out.</p>

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_1.4.1/" rel="nofollow">numerous security issues</a> and all users must <a href="https://tails.boum.org/doc/first_steps/upgrade/" rel="nofollow">upgrade</a> as soon as possible.</p>

<h2>New features</h2>

<ul>
<li>Disable access to the local network in the <em>Tor Browser</em>. You should now use the <em>Unsafe Browser</em> to access the local network.</li>
</ul>

<h2>Upgrades and changes</h2>

<ul>
<li>Install <em>Tor Browser</em> 5.0 (based on Firefox 38esr).</li>
<li>Install a <em>32-bit GRUB EFI boot loader</em>. Tails should now start on some tablets with Intel Bay Trail processors among others.</li>
<li>Let the user know when <em>Tails Installer</em> has rejected a device because it is too small.</li>
</ul>

<p>There are numerous other changes that might not be apparent in the daily operation of a typical user. Technical details of all the changes are listed in the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">Changelog</a>.</p>

<h2>Fixed problems</h2>

<ul>
<li>Our <em>AppArmor</em> setup has been audited and improved in various ways which should harden the system.</li>
<li>The network should now be properly disabled when <em>MAC address spoofing</em> fails.</li>
</ul>

<h2>Known issues</h2>

<p>See the current list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">known issues</a>.</p>

<h2>Download or upgrade</h2>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> page.</p>

<h2>What's coming up?</h2>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for September 22.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Do you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>, for example by <a href="https://tails.boum.org/contribute/how/donate/" rel="nofollow">donating</a>. If you want to help, come talk to us!</p>

<h2>Support and feedback</h2>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

