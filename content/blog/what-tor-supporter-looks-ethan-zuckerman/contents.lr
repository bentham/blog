title: This is What a Tor Supporter Looks Like: Ethan Zuckerman
---
pub_date: 2016-01-06
---
author: katina
---
tags:

funding
crowdfunding
---
categories: fundraising
---
_html_body:

<p><a href="https://www.torproject.org/donate/donate" rel="nofollow"><img src="https://extra.torproject.org/blog/2016-01-06-what-tor-supporter-looks-ethan-zuckerman/ethan.jpg" alt="Ethan Zuckerman" /></a></p>

<p>“I'm a Tor supporter for a very simple reason: my colleague's lives depend on it.</p>

<p>I'm the co-founder of <a href="https://globalvoices.org" rel="nofollow">Global Voices</a>, an international community of bloggers, reporters, translators and activists dedicated to building a more inclusive picture of the world. Members of our community are located in more than 100 nations, and many members of our team live in countries where freedom of expression is under threat.</p>

<p>Some Global Voices members need to maintain anonymity so they can report from countries where their words could lead to imprisonment or worse. These threats are not imaginary - four members of our team were <a href="https://advox.globalvoices.org/2015/10/16/global-voices-welcomes-the-release-of-zone9-bloggers-pledges-to-keep-fighting-for-free-expression-in-ethiopia/" rel="nofollow">incarcerated for more than a year in Ethiopia</a>, and our members in Bangladesh fear for their safety when they write about <a href="https://globalvoices.org/2015/10/31/bangladesh-book-publishers-suffer-fatal-attacks-in-wake-of-blogger-killings/" rel="nofollow">killings of secular bloggers, presumably by government-backed extremists.</a></p>

<p>We teach everyone who works with Global Voices about digital security, and Tor is a cornerstone of the toolkit we teach our authors and translators to use. In countries that monitor web traffic closely, frequently connecting to the Global Voices website could signal that a user is a Global Voices contributor. Correctly using Tor allows our authors in repressive nations to contribute to our site without revealing to local authorities that they are writing for Global Voices. We quite literally could not do our job in many nations we work in without Tor.</p>

<p>While financial support is critical for Tor to continue developing its tools, and while the real heroes of the Tor project are the thousands of volunteers who donate bandwidth and time to maintain Tor exit nodes, there's another way to help Tor: use Tor. Here's what I mean: in the most repressive countries we work in, we cannot use Tor because simply using strong encryption is so uncommon that governments seek out users of these tools. For Tor to be useful in an extremely repressive country like Ethiopia, it needs to be normal, as normal as HTTPS has become in the past few years. When you use Tor to do something quotidian, you're providing cover traffic for people who need to use Tor to do something extraordinary. So give, run an exit node, but at the very least, help make Tor utterly ordinary by using it as often as you can.”</p>

<p>-Ethan Zuckerman</p>

---
_comments:

<a id="comment-148568"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-148568" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 06, 2016</p>
    </div>
    <a href="#comment-148568">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-148568" class="permalink" rel="bookmark">Tor Browser Bundle 5.0.7 is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Browser Bundle 5.0.7 is out?<br />
<a href="http://www.majorgeeks.com/files/details/tor_browser_bundle.html" rel="nofollow">http://www.majorgeeks.com/files/details/tor_browser_bundle.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-148661"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-148661" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">January 07, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-148568" class="permalink" rel="bookmark">Tor Browser Bundle 5.0.7 is</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-148661">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-148661" class="permalink" rel="bookmark">Now, yes.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Now, yes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-148847"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-148847" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 07, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-148568" class="permalink" rel="bookmark">Tor Browser Bundle 5.0.7 is</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-148847">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-148847" class="permalink" rel="bookmark">If you enable JavaScript,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you enable JavaScript, you need to download the new version because this is an emergency patch of a serious flaw.</p>
<p>@ Shari:</p>
<p>I think potential users are unable to find the link to the download page from the <a href="http://www.torproject.org" rel="nofollow">www.torproject.org</a> homepage.   Maybe increase the font to make the link easier for overwhelmed newbies to spot?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-148863"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-148863" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 07, 2016</p>
    </div>
    <a href="#comment-148863">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-148863" class="permalink" rel="bookmark">@ Ethan:
Many thanks for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ Ethan:</p>
<p>Many thanks for providing such important initiatives as RuNet Echo at globalvoices.org!</p>
<p><a href="https://globalvoices.org/-/special/runet-echo/" rel="nofollow">https://globalvoices.org/-/special/runet-echo/</a></p>
<p>When US activists try to raise their voices in protest to bills such as this...</p>
<p><a href="http://thehill.com/policy/cybersecurity/262339-dem-to-revive-terrorist-reporting-mandate-for-web-firms" rel="nofollow">http://thehill.com/policy/cybersecurity/262339-dem-to-revive-terrorist-…</a><br />
Feinstein to revive social media bill after terror attacks<br />
Cory Bennett<br />
7 Dec 2015</p>
<p>&gt; The Senate Intelligence Committee's top Democrat could soon introduce legislation that would require social media platforms, including Facebook, Twitter and YouTube, to alert federal officials about online terrorist activity, according to a spokesman on Monday.</p>
<p>... it is important that they are able to point to Russian analogs such as this program:</p>
<p><a href="https://globalvoices.org/2015/12/25/russian-censors-launch-automatic-online-media-monitoring-system-to-spot-extremist-content/" rel="nofollow">https://globalvoices.org/2015/12/25/russian-censors-launch-automatic-on…</a><br />
Russian Censors Launch Automatic Online Media Monitoring System to Spot ‘Extremist’ Content<br />
25 Dec 2015</p>
<p>&gt; A system that automatically scans the content of online media outlets for forbidden and illegal material is now operating in 19 regions across Russia.<br />
&gt; ...<br />
&gt; The new system will be primarily used to analyze Russian media websites to spot content that is considered illegal under Article 4 of the Russian mass media law, which covers pornography, calls to violence or terrorism, extremist materials, and profanity. Zharov told Izvestia that even in testing mode, the algorithm has enabled the censors to find “twice as many” violations as before.</p>
<p>Another recurring topic in this blog is the issue of the professional comment trolls who work at 55 Savushkina:</p>
<p><a href="https://globalvoices.org/2014/11/19/fake-ukrainian-news-websites-run-by-russian-troll-army-offshoots/" rel="nofollow">https://globalvoices.org/2014/11/19/fake-ukrainian-news-websites-run-by…</a><br />
Fake ‘Ukrainian’ News Websites Run by Russian ‘Troll Army’ Offshoots<br />
19 Nov 2014</p>
<p>&gt; Russia has engaged in a blitzkrieg of information warfare since the start of the Euromaidan protests, increasing the budget of RT (formerly Russia Today) by 41%, and creating a new international news operation called Sputnik to “provide an alternative viewpoint on world events.” More and more, though, the Kremlin is manipulating the information sphere in more insidious ways.<br />
&gt; ...<br />
&gt; After some impressive detective work, the sleuths at DP found that an outlet called Neva News, run from the 55 Savushkina office, bought the Kharkov News Agency’s domain name on August 25, 2013. The CEO and co-owner of Neva News, Evgenii Zubarev, confirmed DP’s findings, but insisted that the location of the outlet’s hosting or newsroom does not affect its editorial policy.<br />
&gt;<br />
&gt; Zubarev is far from an isolated figure, as he heads up the Federal Agency News (FAN) project, which is also based out of—tell us if you’ve noticed a pattern yet—55 Savushkina.<br />
&gt; ...<br />
&gt; As it turns out, these websites are supported and run by Moscow businessman and hotelier Andrei Surkov, who says he only funds about 70% of the sites’ work, and the rest comes from public donations. Surkov himself is publishing a book entitled “Guard of the Steel Emperor: The Secret Origin of the Russian People,” detailing how the Russian people are actually descendants of the Aryan race and the guards of Genghis Khan.</p>
<p>Andrei Surkov is the Russian version of Donald Trump!</p>
<p>It's all of them against all of us.</p>
</div>
  </div>
</article>
<!-- Comment END -->
