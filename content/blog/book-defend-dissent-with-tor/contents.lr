title: Defend Dissent with Tor
---
pub_date: 2021-04-27
---
author: ggus
---
tags:

education
internet surveillance
censorship circumvention
---
categories:

circumvention
community
---
_html_body:

<p><em>Guest post by Glencora Borradaile</em></p>
<p>After 4 years of giving digital security trainings to activists and teaching a course called "Communications Security and Social Movements", I've compiled all my materials into an open, digital book - <a href="https://open.oregonstate.education/defenddissent/">Defend Dissent: Digital Suppression and Cryptographic Defense of Social Movements</a> hosted by Oregon State University where I am an Associate Professor. The book is intended for an introductory, non-major college audience, and I hope it will find use outside the university setting.</p>
<p>Defend Dissent has three parts:</p>
<ul>
<li>Part 1 covers the basics of cryptography: basic encryption, how keys are exchanged, how passwords protect accounts and how encryption can help provide anonymity.  When I give digital security trainings, I don't spend a lot of time here, but I still want people to know (for example) what end-to-end encryption is and why we want it.</li>
<li>Part 2 gives brief context for how surveillance is used to suppress social movements, with a US focus.</li>
<li>Part 3 contains what you might consider more classic material for digital security training, focusing on the different places your data might be vulnerable and the tactics you can use to defend your data.</li>
</ul>
<p>Each chapter ends with a story that brings social context to the material in that chapter (even in Part 1!) - from surveillance used against contemporary US protests to the African National Congress' use of partially manual encryption in fighting apartheid in South Africa in the 80s.</p>
<p>It should be no surprise that Tor is a star of Defend Dissent, ending out Parts 1 and 3. The anonymity that the Tor technology enables turns the internet into what it should be: a place to communicate without everyone knowing your business. As a professor, I love teaching Tor. It is a delightful combination of encryption, key exchange, probability and threat modeling.</p>
<p>In Defend Dissent, I aim to make Tor easy to understand, and use a three-step example to explain Tor to audiences who may have never used it before: There are just three steps to understanding how Tor works:</p>
<p>1. <strong>Encryption</strong> allows you to keep the content of your communications private from anyone who doesn't have the key. But it doesn't protect your identity or an eavesdropper from knowing who you are communicating with and when.</p>
<p><img alt="Encryption keeps the content of your communications private" height="103" src="/static/images/blog/inline-images/E2EE.png" width="543" /></p>
<p>2. Assata can send Bobby an encrypted message even if they haven't met ahead of time to agree on a key for encryption. This concept can be used to allow Assata and Bobby to agree on a single <strong>encryption key</strong>. (Put an encryption key in the box.)</p>
<p><img alt="Exchanging a secure message without sharing a key" height="305" src="/static/images/blog/inline-images/lockbox-a.gif" width="543" /></p>
<p>3. When Assata accesses Tor, the Tor Browser picks three <strong>randomly chosen nodes</strong> (her entry, relay and exit nodes) from amongst thousands in the Tor network. Assata's Tor Browser agrees on a key with the entry node, then agrees on a key with the relay node by communicating with the relay node <strong>through</strong> the entry node, and so on. Assata's Tor Browser encrypts the message with the exit key, then with the relay key and then with the entry key and sends the message along. The entry node removes one layer of encryption and so on. (Like removing the layers of an onion ...) This way, the relay doesn't know who Assata is - just that it is relaying a message through the Tor network.</p>
<p><img src="https://media.torproject.org/video/anonymous-browsing-tor-a.gif" /></p>
<p>I'm excited to share this accessible resource and to teach the world more about Tor, encryption, and secure communication. Even if you're a technical expert, Defend Dissent may help you talk to others in your life about how to use Tor and why these kinds of tools are so vital to social movements, change, and dissent.</p>
<p>For more details on how Tor works you can read the four chapters of Defend Dissent that lead to <a href="https://open.oregonstate.education/defenddissent/chapter/anonymous-routing/">Anonymous Routing</a>: <a href="https://open.oregonstate.education/defenddissent/chapter/what-is-encryption/">What is Encryption?</a>, <a href="https://open.oregonstate.education/defenddissent/chapter/modern-cryptography/">Modern Cryptography</a>, <a href="https://open.oregonstate.education/defenddissent/chapter/exchanging-keys-for-encryption/">Exchanging Keys for Encryption</a>, and <a href="https://open.oregonstate.education/defenddissent/chapter/metadata/">Metadata</a>.<br />
Or discover other topics in <a href="https://open.oregonstate.education/defenddissent/wp-admin/admin-ajax.php?action=h5p_embed&amp;id=2">defending social movements with cryptography</a>.</p>

