title: Tor 0.3.2.7-rc is released!
---
pub_date: 2017-12-14
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>Tor 0.3.2.7-rc fixes various bugs in earlier versions of Tor, including some that could affect reliability or correctness.</p>
<p>This is the first release candidate in the 0.3.2 series. If we find no new bugs or regression here, then the first stable 0.3.2. release will be nearly identical to this.</p>
<p>You can download the source from the usual place on the website. Binary packages should be available soon, with a Tor Browser alpha release likely some time next week.</p>
<h2>Changes in version 0.3.2.7-rc - 2017-12-14</h2>
<ul>
<li>Major bugfixes (circuit prediction):
<ul>
<li>Fix circuit prediction logic so that a client doesn't treat a port as being "handled" by a circuit if that circuit already has isolation settings on it. This change should make Tor clients more responsive by improving their chances of having a pre-created circuit ready for use when a request arrives. Fixes bug <a href="https://bugs.torproject.org/18859">18859</a>; bugfix on 0.2.3.3-alpha.</li>
</ul>
</li>
<li>Minor features (logging):
<ul>
<li>Provide better warnings when the getrandom() syscall fails. Closes ticket <a href="https://bugs.torproject.org/24500">24500</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (portability):
<ul>
<li>Tor now compiles correctly on arm64 with libseccomp-dev installed. (It doesn't yet work with the sandbox enabled.) Closes ticket <a href="https://bugs.torproject.org/24424">24424</a>.</li>
</ul>
</li>
<li>Minor bugfixes (bridge clients, bootstrap):
<ul>
<li>Retry directory downloads when we get our first bridge descriptor during bootstrap or while reconnecting to the network. Keep retrying every time we get a bridge descriptor, until we have a reachable bridge. Fixes part of bug <a href="https://bugs.torproject.org/24367">24367</a>; bugfix on 0.2.0.3-alpha.</li>
<li>Stop delaying bridge descriptor fetches when we have cached bridge descriptors. Instead, only delay bridge descriptor fetches when we have at least one reachable bridge. Fixes part of bug <a href="https://bugs.torproject.org/24367">24367</a>; bugfix on 0.2.0.3-alpha.</li>
<li>Stop delaying directory fetches when we have cached bridge descriptors. Instead, only delay bridge descriptor fetches when all our bridges are definitely unreachable. Fixes part of bug <a href="https://bugs.torproject.org/24367">24367</a>; bugfix on 0.2.0.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix a signed/unsigned comparison warning introduced by our fix to TROVE-2017-009. Fixes bug <a href="https://bugs.torproject.org/24480">24480</a>; bugfix on 0.2.5.16.</li>
</ul>
</li>
<li>Minor bugfixes (correctness):
<ul>
<li>Fix several places in our codebase where a C compiler would be likely to eliminate a check, based on assuming that undefined behavior had not happened elsewhere in the code. These cases are usually a sign of redundant checking or dubious arithmetic. Found by Georg Koppen using the "STACK" tool from Wang, Zeldovich, Kaashoek, and Solar-Lezama. Fixes bug <a href="https://bugs.torproject.org/24423">24423</a>; bugfix on various Tor versions.</li>
</ul>
</li>
<li>Minor bugfixes (onion service v3):
<ul>
<li>Fix a race where an onion service would launch a new intro circuit after closing an old one, but fail to register it before freeing the previously closed circuit. This bug was making the service unable to find the established intro circuit and thus not upload its descriptor, thus making a service unavailable for up to 24 hours. Fixes bug <a href="https://bugs.torproject.org/23603">23603</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (scheduler, KIST):
<ul>
<li>Properly set the scheduler state of an unopened channel in the KIST scheduler main loop. This prevents a harmless but annoying log warning. Fixes bug <a href="https://bugs.torproject.org/24502">24502</a>; bugfix on 0.3.2.4-alpha.</li>
<li>Avoid a possible integer overflow when computing the available space on the TCP buffer of a channel. This had no security implications; but could make KIST allow too many cells on a saturated connection. Fixes bug <a href="https://bugs.torproject.org/24590">24590</a>; bugfix on 0.3.2.1-alpha.</li>
<li>Downgrade to "info" a harmless warning about the monotonic time moving backwards: This can happen on platform not supporting monotonic time. Fixes bug <a href="https://bugs.torproject.org/23696">23696</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-273067"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273067" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>uldmin (not verified)</span> said:</p>
      <p class="date-time">December 14, 2017</p>
    </div>
    <a href="#comment-273067">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273067" class="permalink" rel="bookmark">got tor from bitcoin doc,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>got tor from bitcoin doc, very cool uldmin</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273095"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273095" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon (not verified)</span> said:</p>
      <p class="date-time">December 15, 2017</p>
    </div>
    <a href="#comment-273095">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273095" class="permalink" rel="bookmark">Is wordpress blocking Tor?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is wordpress blocking Tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273128"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273128" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 18, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273095" class="permalink" rel="bookmark">Is wordpress blocking Tor?</a> by <span>Anon (not verified)</span></p>
    <a href="#comment-273128">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273128" class="permalink" rel="bookmark">Not that we know of.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not that we know of.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273107"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273107" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Trump tower (not verified)</span> said:</p>
      <p class="date-time">December 16, 2017</p>
    </div>
    <a href="#comment-273107">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273107" class="permalink" rel="bookmark">I do wish there was a page…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I do wish there was a page telling us what all these different versions of TOR do?<br />
Tor Browser 7.0.11<br />
Tor Browser 7.5a9<br />
Tor 0.3.2.7-rc</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273129"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273129" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 18, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273107" class="permalink" rel="bookmark">I do wish there was a page…</a> by <span>Trump tower (not verified)</span></p>
    <a href="#comment-273129">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273129" class="permalink" rel="bookmark">The first two are the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The first two are the current Tor Browser stable version and the current Tor Browser alpha version respectively (note the "a" in the version string of the alpha one). 0.3.2.7-rc is the release candidate for the Tor software which is responsible for sending your data to and receiving it from relays around the world. Tor is shipped with Tor Browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273228"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273228" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>me (not verified)</span> said:</p>
      <p class="date-time">December 27, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-273228">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273228" class="permalink" rel="bookmark">That&#039;s useful than you, but…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's useful thank you, but don't you think it would be better to have a page dedicated to these explanations.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-273143"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273143" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>me (not verified)</span> said:</p>
      <p class="date-time">December 18, 2017</p>
    </div>
    <a href="#comment-273143">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273143" class="permalink" rel="bookmark">If net neutrality ends in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If net neutrality ends in the United States, how can we access Tor if it is blocked? Would they not block bridges also? Can someone please recommend a method to address this? Thank you. I think this issue is very serious and could impact elections and much more.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273154"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273154" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2017</p>
    </div>
    <a href="#comment-273154">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273154" class="permalink" rel="bookmark">Scalable and Provably Secure…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Scalable and Provably Secure P2P Communication Protocols<br />
<a href="https://arxiv.org/pdf/1706.05367.pdf" rel="nofollow">https://arxiv.org/pdf/1706.05367.pdf</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273161"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273161" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>berserk (not verified)</span> said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
    <a href="#comment-273161">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273161" class="permalink" rel="bookmark">i love tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i love tor</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273296"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273296" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>sat toruser (not verified)</span> said:</p>
      <p class="date-time">January 03, 2018</p>
    </div>
    <a href="#comment-273296">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273296" class="permalink" rel="bookmark">Tor is realy good thankyou…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is realy good thankyou developers !</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273435"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273435" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Freespirit (not verified)</span> said:</p>
      <p class="date-time">January 17, 2018</p>
    </div>
    <a href="#comment-273435">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273435" class="permalink" rel="bookmark">Am I becoming Paranoid? The…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Am I becoming Paranoid? The date of this comment is January 17 2018</p>
<p>I have noticed in the last few months, especially weeks that there are more, in fact MOST  IP addresses for TOR Browser from Canada, Germany, Switzerland, U.S.  and Sweden, ALL Western countries with heavy but subtle CENSORSHIP and "coincidentally" my Browser becomes SLOW to not working at all, on my visited  political sites ( I am very political).</p>
<p>When I try to change my IP. I simply get another from one of those IP address countries I cite. It requires my changing address IPS at least 5 times to get one from France, Netherlands or a small country, which, I might add, generally work much faster</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273493"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273493" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273435" class="permalink" rel="bookmark">Am I becoming Paranoid? The…</a> by <span>Freespirit (not verified)</span></p>
    <a href="#comment-273493">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273493" class="permalink" rel="bookmark">See tor-relays mailing list…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>See tor-relays mailing list. It is discussed issue related to DDoS attacks and increase of tor users from some Western countries. Now the time required to construct new circuit is higher than before. In worst case try to pick new set of guard nodes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273716"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273716" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Freespirit (not verified)</span> said:</p>
      <p class="date-time">January 27, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273493" class="permalink" rel="bookmark">See tor-relays mailing list…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273716">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273716" class="permalink" rel="bookmark">Thanks for you reply and I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for you reply and I failed to mention the worst IP's come from Germany and when I try to change, I get another form Germany and then another etc, up to at least 5 IP's from Germany, ALL terribly slow to not working at all, until finally I can get a reasonable well-working working IP, usually in France, Netherlands or a small nondescript country</p>
<p>If you are familiar with Zionist-infiltration politics in Germany you will understand</p>
<p>I did suspect DDOS but was reluctant to say so and it is going to be a difficult tactic to prevent</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273824"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273824" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 01, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273716" class="permalink" rel="bookmark">Thanks for you reply and I…</a> by <span>Freespirit (not verified)</span></p>
    <a href="#comment-273824">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273824" class="permalink" rel="bookmark">If you add to torrc option:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you add to torrc option:<br />
<span class="geshifilter"><code class="php geshifilter-php">EntryNodes <span style="color: #009900;">&#123;</span>ca<span style="color: #009900;">&#125;</span><span style="color: #339933;">,</span> <span style="color: #009900;">&#123;</span>cz<span style="color: #009900;">&#125;</span></code></span><br />
only guard nodes from Canada and Czech Republic will be used. Adjust the list of countries to your case, that's all.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273726"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273726" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Freespirit (not verified)</span> said:</p>
      <p class="date-time">January 27, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273493" class="permalink" rel="bookmark">See tor-relays mailing list…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273726">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273726" class="permalink" rel="bookmark">After watching numerous…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>After watching numerous times, now, the way IP addresses are being cycled and preventing me from viewing certain POLITICAL sites, that NOT only are DDOS attacks being used, but more importantly I believe that TOR is being flooded with CONTROLLED IP addresses and THAT is the main tactic.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273825"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273825" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 01, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273726" class="permalink" rel="bookmark">After watching numerous…</a> by <span>Freespirit (not verified)</span></p>
    <a href="#comment-273825">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273825" class="permalink" rel="bookmark">Any web site can censor tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any web site can censor tor exit nodes. Most of CDNs do it (cloudflare, google, recaptcha, etc.). You need extra proxy after tor (e.g. web proxy) to bypass this censorship.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div>
