title: Tor 0.2.2.7-alpha released
---
pub_date: 2010-01-23
---
author: phobos
---
tags:

security critical
bug fixes
security fixes
deprecated features
feature enhancements
performance enhancements
---
categories: releases
---
_html_body:

<p>alpha fixes a huge client-side performance bug, as well<br />
as laying the groundwork for further relay-side performance fixes. It<br />
also starts cleaning up client behavior with respect to the EntryNodes,<br />
ExitNodes, and StrictNodes config options.</p>

<p>This release also rotates two directory authority keys, due to a security<br />
breach of some of the Torproject servers:<br />
<a href="http://archives.seul.org/or/talk/Jan-2010/msg00161.html" rel="nofollow">http://archives.seul.org/or/talk/Jan-2010/msg00161.html</a></p>

<p>Everybody should upgrade:<br />
<a href="https://www.torproject.org/download.html.en" rel="nofollow">https://www.torproject.org/download.html.en</a></p>

<p>Changes in version 0.2.2.7-alpha - 2010-01-19<br />
  o Directory authority changes:<br />
    - Rotate keys (both v3 identity and relay identity) for moria1<br />
      and gabelmoo.</p>

<p>  o Major features (performance):<br />
    - We were selecting our guards uniformly at random, and then weighting<br />
      which of our guards we'd use uniformly at random. This imbalance<br />
      meant that Tor clients were severely limited on throughput (and<br />
      probably latency too) by the first hop in their circuit. Now we<br />
      select guards weighted by currently advertised bandwidth. We also<br />
      automatically discard guards picked using the old algorithm. Fixes<br />
      bug 1217; bugfix on 0.2.1.3-alpha. Found by Mike Perry.<br />
    - When choosing which cells to relay first, relays can now favor<br />
      circuits that have been quiet recently, to provide lower latency<br />
      for low-volume circuits. By default, relays enable or disable this<br />
      feature based on a setting in the consensus. You can override<br />
      this default by using the new "CircuitPriorityHalflife" config<br />
      option. Design and code by Ian Goldberg, Can Tang, and Chris<br />
      Alexander.<br />
    - Add separate per-conn write limiting to go with the per-conn read<br />
      limiting. We added a global write limit in Tor 0.1.2.5-alpha,<br />
      but never per-conn write limits.<br />
    - New consensus params "bwconnrate" and "bwconnburst" to let us<br />
      rate-limit client connections as they enter the network. It's<br />
      controlled in the consensus so we can turn it on and off for<br />
      experiments. It's starting out off. Based on proposal 163.</p>

<p>  o Major features (relay selection options):<br />
    - Switch to a StrictNodes config option, rather than the previous<br />
      "StrictEntryNodes" / "StrictExitNodes" separation that was missing a<br />
      "StrictExcludeNodes" option.<br />
    - If EntryNodes, ExitNodes, ExcludeNodes, or ExcludeExitNodes<br />
      change during a config reload, mark and discard all our origin<br />
      circuits. This fix should address edge cases where we change the<br />
      config options and but then choose a circuit that we created before<br />
      the change.<br />
    - If EntryNodes or ExitNodes are set, be more willing to use an<br />
      unsuitable (e.g. slow or unstable) circuit. The user asked for it,<br />
      they get it.<br />
    - Make EntryNodes config option much more aggressive even when<br />
      StrictNodes is not set. Before it would prepend your requested<br />
      entrynodes to your list of guard nodes, but feel free to use others<br />
      after that. Now it chooses only from your EntryNodes if any of<br />
      those are available, and only falls back to others if a) they're<br />
      all down and b) StrictNodes is not set.<br />
    - Now we refresh your entry guards from EntryNodes at each consensus<br />
      fetch -- rather than just at startup and then they slowly rot as<br />
      the network changes.</p>

<p>  o Major bugfixes:<br />
    - Stop bridge directory authorities from answering dbg-stability.txt<br />
      directory queries, which would let people fetch a list of all<br />
      bridge identities they track. Bugfix on 0.2.1.6-alpha.</p>

<p>  o Minor features:<br />
    - Log a notice when we get a new control connection. Now it's easier<br />
      for security-conscious users to recognize when a local application<br />
      is knocking on their controller door. Suggested by bug 1196.<br />
    - New config option "CircuitStreamTimeout" to override our internal<br />
      timeout schedule for how many seconds until we detach a stream from<br />
      a circuit and try a new circuit. If your network is particularly<br />
      slow, you might want to set this to a number like 60.<br />
    - New controller command "getinfo config-text". It returns the<br />
      contents that Tor would write if you send it a SAVECONF command,<br />
      so the controller can write the file to disk itself.<br />
    - New options for SafeLogging to allow scrubbing only log messages<br />
      generated while acting as a relay.<br />
    - Ship the bridges spec file in the tarball too.<br />
    - Avoid a mad rush at the beginning of each month when each client<br />
      rotates half of its guards. Instead we spread the rotation out<br />
      throughout the month, but we still avoid leaving a precise timestamp<br />
      in the state file about when we first picked the guard. Improves<br />
      over the behavior introduced in 0.1.2.17.</p>

<p>  o Minor bugfixes (compiling):<br />
    - Fix compilation on OS X 10.3, which has a stub mlockall() but<br />
      hides it. Bugfix on 0.2.2.6-alpha.<br />
    - Fix compilation on Solaris by removing support for the<br />
      DisableAllSwap config option. Solaris doesn't have an rlimit for<br />
      mlockall, so we cannot use it safely. Fixes bug 1198; bugfix on<br />
      0.2.2.6-alpha.</p>

<p>  o Minor bugfixes (crashes):<br />
    - Do not segfault when writing buffer stats when we haven't observed<br />
      a single circuit to report about. Found by Fabian Lanze. Bugfix on<br />
      0.2.2.1-alpha.<br />
    - If we're in the pathological case where there's no exit bandwidth<br />
      but there is non-exit bandwidth, or no guard bandwidth but there<br />
      is non-guard bandwidth, don't crash during path selection. Bugfix<br />
      on 0.2.0.3-alpha.<br />
    - Fix an impossible-to-actually-trigger buffer overflow in relay<br />
      descriptor generation. Bugfix on 0.1.0.15.</p>

<p>  o Minor bugfixes (privacy):<br />
    - Fix an instance where a Tor directory mirror might accidentally<br />
      log the IP address of a misbehaving Tor client. Bugfix on<br />
      0.1.0.1-rc.<br />
    - Don't list Windows capabilities in relay descriptors. We never made<br />
      use of them, and maybe it's a bad idea to publish them. Bugfix<br />
      on 0.1.1.8-alpha.</p>

<p>  o Minor bugfixes (other):<br />
    - Resolve an edge case in path weighting that could make us misweight<br />
      our relay selection. Fixes bug 1203; bugfix on 0.0.8rc1.<br />
    - Fix statistics on client numbers by country as seen by bridges that<br />
      were broken in 0.2.2.1-alpha. Also switch to reporting full 24-hour<br />
      intervals instead of variable 12-to-48-hour intervals.<br />
    - After we free an internal connection structure, overwrite it<br />
      with a different memory value than we use for overwriting a freed<br />
      internal circuit structure. Should help with debugging. Suggested<br />
      by bug 1055.<br />
    - Update our OpenSSL 0.9.8l fix so that it works with OpenSSL 0.9.8m<br />
      too.</p>

<p>  o Removed features:<br />
    - Remove the HSAuthorityRecordStats option that version 0 hidden<br />
      service authorities could have used to track statistics of overall<br />
      hidden service usage.</p>

---
_comments:

<a id="comment-4076"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4076" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 23, 2010</p>
    </div>
    <a href="#comment-4076">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4076" class="permalink" rel="bookmark">On my OS X Machine (Intel,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>On my OS X Machine (Intel, 10.5.8), the latest alpha (vidalia-bundle-0.2.2.7) fails to start Tor. As far as I can tell, the problem seems very similar to the SSL-related stuff that was supposed to be worked around in this release. The "advanced" log has a bunch of lines saying "[Warning] TLS error: unexpected close while renegotiating (SSL_ST_OK)" and the status bar in the GUI gets stuck about a third of the way across. I left it for an hour and it never got further than this.</p>
<p>I thought maybe this had to do with Apple's default OpenSSL, which is very outdated (0.9.71) even on Leopard, so I build 0.9.8l from source and installed that -- no improvement. However, looking at the comments above, it looks like the Vidalia bundle includes OpenSSL 0.9.8l as it is, so perhaps this problem has some other root cause.</p>
<p>Cheers,</p>
<p>-CF</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4080"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4080" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 24, 2010</p>
    </div>
    <a href="#comment-4080">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4080" class="permalink" rel="bookmark">I&#039;m getting this problem too</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm getting this problem too (also on 10.5.8).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4083"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4083" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 24, 2010</p>
    </div>
    <a href="#comment-4083">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4083" class="permalink" rel="bookmark">Same here with my Intel Mac</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Same here with my Intel Mac running 10.6.2. I also get this behavior with my PowerPC Mac running 10.5.8 after applying the latest security update 2010-001.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4092"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4092" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 24, 2010</p>
    </div>
    <a href="#comment-4092">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4092" class="permalink" rel="bookmark">FWIW, I&#039;m having the same</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>FWIW, I'm having the same experience with OSX 10.6.2, also on Intel.<br />
-BD</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4094"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4094" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 24, 2010</p>
    </div>
    <a href="#comment-4094">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4094" class="permalink" rel="bookmark">Latest Apple security update</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Latest Apple security update contained OpenSSL 0.9.8l with renegotiation disabled, and no way for apps to enable it, tor will remain broken until major changes are made either at apple or in the tor code.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4127"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4127" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 25, 2010</p>
    </div>
    <a href="#comment-4127">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4127" class="permalink" rel="bookmark">I have updated the document</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have updated the document detailing an Eclipse based IDE for developing Tor on Windows to reflect building Tor 0.2.2.7 under automake v2.6.x. </p>
<p>There is a specific section on how to update automake from the vanilla MinGWv5.16 install with MSYSv1.0.0.11 and MSysDTKv1.0.1. I'll upload the document if its likely to be of any use... ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4144"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4144" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 26, 2010</p>
    </div>
    <a href="#comment-4144">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4144" class="permalink" rel="bookmark">Jan 25 16:48:05.155 [notice]</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Jan 25 16:48:05.155 [notice] Tor v0.2.2.7-alpha (git-ad274609b7fec437). This is experimental software. Do not rely on it for strong anonymity. (Running on Linux i686)<br />
Jan 25 16:51:39.179 [notice] Bootstrapped 5%: Connecting to directory server.<br />
Jan 27 02:51:23.735 [notice] Bootstrapped 100%: Done.</p>
<p>Well .... just upgraded .... actually two days ago from version 0.2.0.3 if i recall.<br />
Apparently this version doesnt really sit well with my connection.</p>
<p>It takes for ever to get anything done.<br />
I guess i should never turn my pc off ... cause it will take for ever to reconnect ! </p>
<p>Any akes on that ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4155"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4155" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 28, 2010</p>
    </div>
    <a href="#comment-4155">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4155" class="permalink" rel="bookmark">For MAC 10.6</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For MAC 10.6 users:</p>
<p><a href="https://www.torproject.org/dist/testing/" rel="nofollow">https://www.torproject.org/dist/testing/</a><br />
vidalia-bundle-0.2.2.8-alpha-0.2.7-i386.dmg</p>
<p>Many thanks to developers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4166"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4166" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 28, 2010</p>
    </div>
    <a href="#comment-4166">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4166" class="permalink" rel="bookmark">Thank you so much!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you so much!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4237"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4237" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 02, 2010</p>
    </div>
    <a href="#comment-4237">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4237" class="permalink" rel="bookmark">Dear Tor developers, I can</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Dear Tor developers, I can play youtube video (flash?) on Google Chrome 4.0.249.78 (36714) with Tor 0.2.2.6-alpha (git-1ee580407ccb9130). I couldn't play youtube video on firefox with Tor , and I got the answer why it can't here - because of some security limitation. Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4335"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4335" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 12, 2010</p>
    </div>
    <a href="#comment-4335">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4335" class="permalink" rel="bookmark">If you are using the Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you are using the Tor button for Firefox - you need to relax the security settings in the plugin</p>
<p>Go to Tor Button.... Preferences | Security Settings.<br />
Deselect the setting 'Disable plugins during Tor usage (crucial)'</p>
<p>BE WARNED: If you do this you will reduce your anonymity online. The setting is marked crucial for good reason !</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4374"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4374" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 19, 2010</p>
    </div>
    <a href="#comment-4374">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4374" class="permalink" rel="bookmark">My hidden service is not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My hidden service is not reachable anymore after update to 0.2.2.7-alpha. What's up with that?</p>
</div>
  </div>
</article>
<!-- Comment END -->
