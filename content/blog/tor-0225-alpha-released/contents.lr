title: Tor 0.2.2.5-alpha released
---
pub_date: 2009-10-12
---
author: phobos
---
tags:

bug fixes
alpha release
code refactoring
directory authority
---
categories:

network
releases
---
_html_body:

<p>On October 11, we released Tor 0.2.2.5-alpha.  </p>

<p>It can be downloaded from <a href="https://www.torproject.org/download/" rel="nofollow">https://www.torproject.org/download/</a>.</p>

<p>It contains:  </p>

<p><strong>Major bugfixes:</strong></p>

<ul>
<li>Make the tarball compile again. Oops. Bugfix on 0.2.2.4-alpha.</li>
</ul>

<p><strong>New directory authorities:</strong></p>

<ul>
<li>Move dizum to an alternate IP address.</li>
</ul>

<p><strong>Code simplifications and refactorings</strong></p>

<ul>
<li>Numerous changes, bugfixes, and workarounds from Nathan Freitas<br />
      to help Tor build correctly for Android phones.</li>
</ul>

---
_comments:

<a id="comment-2815"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2815" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 12, 2009</p>
    </div>
    <a href="#comment-2815">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2815" class="permalink" rel="bookmark">help</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In polish version LOG are in english<br />
no help!<br />
And in WWW no image translated</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2819"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2819" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">October 13, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2815" class="permalink" rel="bookmark">help</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2819">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2819" class="permalink" rel="bookmark">re: polish</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>perhaps no one has translated the log entries to polish, we have a fine translation portal to do just that.</p>
<p>no one ever translates the images because our volunteer translators aren't graphic artists.</p>
<p>Will you help us translate these things?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2857"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2857" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Cav Edwards (not verified)</span> said:</p>
      <p class="date-time">October 17, 2009</p>
    </div>
    <a href="#comment-2857">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2857" class="permalink" rel="bookmark">Performance for differing ConstrainedSocksSize</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have been doing some testing of Tor.<br />
We are using this bandwidth test tool:<br />
<a href="http://i.dslr.net/iphone_speedtest.html" rel="nofollow">http://i.dslr.net/iphone_speedtest.html</a> (clicking the GPS test)</p>
<p>We have found the following results:<br />
<a href="http://docs.google.com/fileview?id=0B6YAzYus3pVYZWEwZmUxNTgtODlhZi00ZjE1LTk2Y2UtMDE0NDhiMTZiOTNi&amp;hl=en" rel="nofollow">http://docs.google.com/fileview?id=0B6YAzYus3pVYZWEwZmUxNTgtODlhZi00ZjE…</a></p>
<p>What we cannot understand is why there is a difference when constrained socks size is set to the default of 8KB in the torrc file as opposed to leaving it unset and defaulting. We thought the default was 8KB. This means the results for 8KB set explicitly and 8KB set by default (no option set for ConstrainedSocksSize in torrc) should deliver similar performance.</p>
<p>In our graph, the 'not set' option and the 8KB option are wildly different. The explicitly set 8KB option seems to consistently perform the best.</p>
<p>Can someone explain our results. We had thought that circuit quality could change dramatically over the lifetime that Tor is running, which means our results could be random. We did a crude 'burn in' before taking the results and ran them multiple times though.</p>
<p>Maybe others could corroborate our results.</p>
<p>Many thanks for your time...</p>
<p>Cav Edwards</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2911"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2911" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Cav Edwards (not verified)</span> said:</p>
      <p class="date-time">October 22, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2857" class="permalink" rel="bookmark">Performance for differing ConstrainedSocksSize</a> by <span>Cav Edwards (not verified)</span></p>
    <a href="#comment-2911">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2911" class="permalink" rel="bookmark">The kernel</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I guess, reviewing that last post, the question boils down to 'what is the default socket size' for Tor ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2963"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2963" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 28, 2009</p>
    </div>
    <a href="#comment-2963">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2963" class="permalink" rel="bookmark">……</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>skype（proxy）+TOR------&gt;<a href="http://clip2net.com/clip/m13179/thumb640/1256720327-clip-30kb.png" rel="nofollow">http://clip2net.com/clip/m13179/thumb640/1256720327-clip-30kb.png</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
