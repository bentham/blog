title: Tor Weekly News — August 27th, 2014
---
pub_date: 2014-08-27
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the thirty-fourth issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Orfox: a new Firefox-based secure browser for Android</h1>

<p>With the growing popularity of pocket computers (also known as “phones”), users need to have access to censorship-circumvention and anonymity systems on these devices as well as on their desktop or laptop machines. While there is currently no supported implementation of Tor for Apple’s iOS, the <a href="https://guardianproject.info" rel="nofollow">Guardian Project</a> works closely with the Tor Project to produce (amongst other software) a Tor client for Android named <a href="https://guardianproject.info/apps/orbot/" rel="nofollow">Orbot</a>. Mobile applications can be proxied through Orbot just as they can through the Tor client on other operating systems, but mobile web browsing potentially suffers from the same issues that the Tor Browser was designed to protect against, such as disk leaks and a large attack surface. The Guardian Project has therefore also been maintaining a dedicated mobile browser for use with Orbot under the name <a href="https://guardianproject.info/apps/orweb/" rel="nofollow">Orweb</a>.</p>

<p>Orweb is based on WebView, and is limited by that browser’s features; flaws such as the <a href="https://guardianproject.info/2014/06/30/recent-news-on-orweb-flaws/" rel="nofollow">potential HTML5 IP leak</a>, while possible to work around in the short term, have made it clear that the best future for secure mobile browsing lies in a switch to an application based on Firefox/Fennec/GeckoView.</p>

<p>Following a successful Google Summer of Code project by <a href="https://lists.torproject.org/pipermail/tor-dev/2014-August/007379.html" rel="nofollow">Amogh Pradeep</a> and work by other Guardian Project members, Nathan Freitas <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2014-August/003717.html" rel="nofollow">announced</a> that “a real working version” of Orfox, the new Orbot-compatible mobile browser, is now available. “All the necessary defaults [have been] changed to match Tor Browser’s defaults as closely as possible”; the developers also “remove the Android permissions for things like camera, mic, GPS” and “turn off webrtc.”</p>

<p>“We still need to figure out which preferences and features map between the desktop mobile browser and the Android version, so there is quite a bit of work to do”, but you can download and test this initial version by following the links in Nathan’s email. “Over the next few months we hope to launch this as our new official browser for Orbot, and deprecate Orweb as quickly as possible”, he concluded.</p>

<h1>Miscellaneous news</h1>

<p>A new release of ooniprobe, the network interference data collector for <a href="https://ooni.torproject.org/" rel="nofollow">OONI</a>, was <a href="https://lists.torproject.org/pipermail/ooni-dev/2014-August/000147.html" rel="nofollow">announced</a> by Arturo Filastò. Version 1.1.0 introduces a new command line tool “for listing the reports that have not been published to a collector and that allows the probe operator to choose which ones they would like to upload”. The new version also improves the privacy of the reports by sanitizing file paths.</p>

<p>Developers of applications using <a href="https://onionoo.torproject.org/" rel="nofollow">Onionoo</a> — the web service to learn about currently running Tor relays and bridges — are invited to join the new <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/onionoo-announce" rel="nofollow">onionoo-announce mailing list</a>. Keeping the list low volume, Karsten Loesing plans on using it to announce major protocol changes, scheduled maintenance, major bug fixes, and other important news.</p>

<p>Yawning Angel has <a href="https://lists.torproject.org/pipermail/tor-dev/2014-August/007404.html" rel="nofollow">made available</a> an experimental version of the Tor Browser that includes the latest version of the <a href="https://github.com/Yawning/obfs4" rel="nofollow">obfs4</a> pluggable transport. Testing on Windows and OS X would be particularly welcome. </p>

<p>Fabian Keil <a href="https://lists.torproject.org/pipermail/tor-dev/2014-August/007412.html" rel="nofollow">reported</a> that FreeBSD now includes ports of liballium and obfsclient.</p>

<p>JusticeRage <a href="https://lists.torproject.org/pipermail/tor-relays/2014-August/005168.html" rel="nofollow">explained</a> how relay operators who offer exiting on port 25 can protect the reputation of their domain name by using the Sender Policy Framework.</p>

<p>Sreenatha Bhatlapenumarthi sent the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-August/007399.html" rel="nofollow">final GSoC report</a> for the Tor Weather rewrite project. Juha Nurmi sent <a href="https://lists.torproject.org/pipermail/tor-reports/2014-August/000625.html" rel="nofollow">another report</a> on the development of ahmia.fi.</p>

<p>Thanks to s7r for hosting a new <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-August/000669.html" rel="nofollow">mirror</a> of the Tor Project’s website and software!</p>

<h1>Tor help desk roundup</h1>

<p>Users of different VPN (Virtual Private Network) services have told the help desk that Tor Browser has difficulty connecting to Tor when a VPN is in use. Using Tor with a VPN is not supported. For a trusted entry into the Tor network, bridges and pluggable transports are recommended, while for anonymizing all network traffic coming from a computer, <a href="https://tails.boum.org/" rel="nofollow">Tails</a> is recommended.</p>

<h1>Easy development tasks to get involved with</h1>

<p>The bandwidth authority scanners measure the actual bandwidth offered by Tor relays in order to get accurate information into the Tor consensus. The measurement process currently splits up the set of relays that are to be measured into 4 subsets, with the goal that measuring each of these subsets should take <a href="https://bugs.torproject.org/3440" rel="nofollow">about the same time</a>. However, this is not the case.  Measuring subsets 2 and 3 is about twice as fast as measuring subset 1, and subset 4 is twice as fast as subset 2 and 3. If you're up for doing some experiments to split up the set into more equal subsets, please let us know about your findings on the ticket.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, harmony, Matt Pagan, Karsten Loesing, and dope457.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

