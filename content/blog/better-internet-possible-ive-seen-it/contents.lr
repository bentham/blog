title: A better internet is possible. I’ve seen it.
---
pub_date: 2019-11-04
---
author: isabela
---
tags:

take back the internet
fundraising
decentralization
privacy
---
categories: fundraising
---
summary: I know that a better internet is possible. In fact, it once existed. I started using the internet in the mid-90s, with a dial up connection in my family’s house in Brazil. I loved getting online because I could go down an infinite rabbit hole of hyperlinks, learning and discovering new things, and I could talk to all kinds of people without having to reveal my real name or my real identity. Who I was in the physical world didn’t matter when I got online.
---
_html_body:

<p><em>Surveillance, censorship, and tracking run rampant online. By <a href="https://torproject.org/donate/donate-tbi-bp2">supporting</a> and using Tor, you help to <a href="https://torproject.org/donate/donate-tbi-bp2">take back the internet</a>.</em><br />
 <br />
I know that a better internet is possible. In fact, it once existed. I started using the internet in the mid-90s, with a dial up connection in my family’s house in Brazil. Even though my parents didn’t have a lot of extra money, they wanted to make sure that my siblings and I had a chance to explore the internet. I loved getting online because I could go down an infinite rabbit hole of hyperlinks, learning and discovering new things, and I could talk to all kinds of people without having to reveal my real name or my real identity. Who I was in the physical world didn’t matter when I got online.<br />
 <br />
My interest in the internet transitioned easily from user to service provider. My first big project was running free software that allowed anyone to publish news (articles with full media support, video, audio, and photos) without having to create an account. You could use any name you wanted—and whoever hosted these sites wouldn’t log the IP addresses of the people publishing content.<br />
 <br />
These sites were used by millions of people, and they existed pre-social media. This particular form of the internet—with its decentralized, untraceable flow of information and connections—allowed for an incredible exchange of ideas, conversation, and news. Its openness is part of the reason so many of us fell in love with computers in the first place. But this model didn’t remain the dominant system.<br />
 <br />
When powerful entities recognized that huge profits could be made online, the technology that made up the internet was co-opted to form a centralized system, comprised of traceable, ‘self-centered’ interactions. The decentralized vision of the internet faded from the consciousness of the general public.<br />
 <br />
As such, less than a decade ago, conversations about preserving privacy online often led to the rebuttal, “I have nothing to hide, why do I need privacy?” But that’s changing. On a societal level, we’re starting to feel the chilling effects of massive, coordinated data collection and its abuse. How it can be used to manipulate, misinform, and isolate us.<br />
 <br />
<strong>We’re beginning to understand that privacy isn't about hiding bad things, but that it’s about protecting what defines us as human beings, who we are. Our day-to-day behavior, our personality, our fears, our relationships, and our vulnerabilities.</strong><br />
 <br />
The concept at the heart of the Tor Project—Onion Routing—<a href="https://www.torproject.org/about/history/">was first conceived</a> around when I started using the internet. Today, the Tor Project is a global nonprofit organization, and the Tor network brings the private internet to an average of 2.5 to 8 million people every single day. While the centralized internet has advanced, so has the decentralized alternative. And now, we’re seeing more and more people turn to Tor, knowing that we can offer them the privacy they deserve.<br />
 <br />
<strong>As we watch the tide of public awareness turn, it’s clear there is a unique opportunity ahead of Tor. Now is the moment to <a href="https://torproject.org/donate/donate-tbi-bp2">take back the internet</a>. </strong><br />
 <br />
How do we actualize a more private, decentralized, equitable internet? It will take a lot of work and a unified vision. It takes preparing the Tor network to handle more users by scaling the network and improving its performance metrics. It takes the Anti-Censorship team working on circumvention solutions that are difficult and expensive for censors to block, but easy for us to deploy and scale. It takes the Tor Browser, Community, and UX teams working together to make Tor more accessible to the people who need it the most.<br />
 <br />
None of this is easy. We have a lot of challenges ahead of us. But it will be worth it. Not for us— but for the internet we believe is possible. And we can’t do this alone. Your support is critical.<br />
 <br />
<strong>If you <a href="https://torproject.org/donate/donate-tbi-bp2">make a contribution</a> before December 31, 2019, Mozilla will match your donation, dollar-for-dollar.</strong></p>
<p><a href="https://torproject.org/donate/donate-tbi-bp2"><img alt="tor donate button " src="/static/images/blog/inline-images/tor-donate-button-purple.png" width="300" class="align-center" /></a></p>
<p>
Now is the time to take back the internet and <a href="https://torproject.org/donate/donate-tbi-bp2">support the Tor Project</a>.</p>

---
_comments:

<a id="comment-285256"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285256" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2019</p>
    </div>
    <a href="#comment-285256">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285256" class="permalink" rel="bookmark">&quot;The Tor network brings the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"The Tor network brings the private internet to an average of 2.5 to 8 million people every single day"</p>
<p>Where did you get this data? According to metrics these numbers have never been reached during the previous 5 years (<a href="http://rougmnvswfsmd4dq.onion/userstats-relay-country.html?start=2014-08-07&amp;end=2019-11-05&amp;country=all&amp;events=off" rel="nofollow">http://rougmnvswfsmd4dq.onion/userstats-relay-country.html?start=2014-0…</a>). Actually, the average number of users seems to be around 2 mln. Even considering bridge users we do not get even close to the sum reported in the post (<a href="http://rougmnvswfsmd4dq.onion/userstats-bridge-country.html?start=2014-08-07&amp;end=2019-11-05&amp;country=all" rel="nofollow">http://rougmnvswfsmd4dq.onion/userstats-bridge-country.html?start=2014-…</a>). </p>
<p>Isn't metrics reliable? Or what else?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285258"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285258" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  steph
  </article>
    <div class="comment-header">
      <p class="comment__submitted">steph said:</p>
      <p class="date-time">November 05, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285256" class="permalink" rel="bookmark">&quot;The Tor network brings the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-285258">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285258" class="permalink" rel="bookmark">Our Metrics on users are…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Our Metrics on users are privacy-protecting, so they've always been an estimation. Now there is new research from a tool called Privcount which shows the higher estimation. We're looking into seeing if we can better pinpoint the numbers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285264"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285264" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2019</p>
    </div>
    <a href="#comment-285264">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285264" class="permalink" rel="bookmark">&quot;a/s/l irl?&quot; People had to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"a/s/l irl?" People had to ask, exposing their request. The person being asked had to consider how to respond. Asking was considered an imposition, cliche, and bad etiquette except as a proposal for cybersex.  And that's why Facebook was born.  Now, the person who used to be asked has been taught and pressured to bare themselves on full display from their moment of birth.  Are you not entertained?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285353"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285353" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anon (not verified)</span> said:</p>
      <p class="date-time">November 07, 2019</p>
    </div>
    <a href="#comment-285353">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285353" class="permalink" rel="bookmark">There&#039;s some kind of rumors…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There's some kind of rumors that Italian government has the intention of targeting Tor users to fight against anonymous hate speech on social networks. </p>
<p>Is it plausible or is it a fake news ?</p>
<p>Does anyone has any concern about that ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285442"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285442" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 12, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285353" class="permalink" rel="bookmark">There&#039;s some kind of rumors…</a> by <span>anon (not verified)</span></p>
    <a href="#comment-285442">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285442" class="permalink" rel="bookmark">I do!
Do you have a link?  …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I do!</p>
<p>Do you have any links to the rumors?  (Italian language OK but English language links would be better.)</p>
<p>If you have any information, please consider contacting TheGuardian at their secure drop (see the link in the previous thread in this blog).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285470"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285470" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>KT (not verified)</span> said:</p>
      <p class="date-time">November 13, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285353" class="permalink" rel="bookmark">There&#039;s some kind of rumors…</a> by <span>anon (not verified)</span></p>
    <a href="#comment-285470">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285470" class="permalink" rel="bookmark">I doubt it will happen, so I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I doubt it will happen, so I wouldn't be concerned.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285357"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285357" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>DerGegner (not verified)</span> said:</p>
      <p class="date-time">November 07, 2019</p>
    </div>
    <a href="#comment-285357">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285357" class="permalink" rel="bookmark">Good news for everyone who…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good news for everyone who has been fighting against all the self-defeating "Going Dark" FUD coming from Comey/Wray and Holder/Barr: in an essay published in Lawfare, former FBI General Counsel Jim Baker--- who led the USG campaign to force Apple to develop special software to break the encryption on a single smart phone, which was dropped after determined opposition from civil liberties groups and after Cellebrite broke into the phone without legally mandated help from Apple--- has adopted our most important bullet points</p>
<p>o we all need more and better encryption for everything,</p>
<p>o "key escrow" and "client side scanning" schemes would break security,</p>
<p>o a backdoor for USG is a backdoor for RU and CN too,</p>
<p>o encryption is not the source of all of societies problems but rather an essential part of the solution to many of them,</p>
<p>o poor cybersecurity protections of "smart grids" and "smart city" dragnets pose a genuine national security threat to the USA (and other nations); not so the issues cited by FBI in its "Going Dark" FUD,</p>
<p>o FBI needs to stop spreading silly FUD, to stop demanding the impossible, and to start living in the real world,</p>
<p>o far from trying to break strong encryption, LEAs should embrace it,</p>
<p>o the world won't end for law enforcement when they do.</p>
<p>@ AG Barr: read it and weep:</p>
<p><a href="https://www.lawfareblog.com/rethinking-encryption" rel="nofollow">https://www.lawfareblog.com/rethinking-encryption</a><br />
Rethinking Encryption<br />
Jim Baker<br />
22 Oct 2019</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285372"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285372" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>camosoul (not verified)</span> said:</p>
      <p class="date-time">November 08, 2019</p>
    </div>
    <a href="#comment-285372">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285372" class="permalink" rel="bookmark">Finally, the left-wingers…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Finally, the left-wingers who destroyed privacy, recognize the crimes they've committed... Of course, not one word about the fact that they are the ones who cause this crisis to begin with. No contrition. Not even so much as an "Oops."</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285381"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285381" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 09, 2019</p>
    </div>
    <a href="#comment-285381">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285381" class="permalink" rel="bookmark">&quot;[...] and tracking run…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"[...] and tracking run rampant online."</p>
<p>That is true. But it will be more and more ...unbelievable when Torproject<br />
is going the  "telemetrifying you-resistance is futile" way of software.</p>
<p>Mozilla/Torproject removes the no update feature, means the browser is<br />
constantly Phoning home. Your and mozillas, you call it, solution is Windows-policies<br />
<a href="https://blog.torproject.org/comment/285329#comment-285329" rel="nofollow">https://blog.torproject.org/comment/285329#comment-285329</a><br />
<a href="https://blog.torproject.org/comment/285330#comment-285330" rel="nofollow">https://blog.torproject.org/comment/285330#comment-285330</a><br />
<a href="https://blog.torproject.org/comment/285366#comment-285366" rel="nofollow">https://blog.torproject.org/comment/285366#comment-285366</a><br />
Like this:<br />
<a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1420514" rel="nofollow">https://bugzilla.mozilla.org/show_bug.cgi?id=1420514</a><br />
"Just a reminder: Bugzilla is not the correct place to advocate for or against features."<br />
LOL, really? You -the user- is to stupid using my software,NUDGING for telemetrie/automaticUpdates is good for you? Logic objections are futile.<br />
Torproject shouldn't go the this way further like e.g. Microsoft, Mozilla or Cisco(hardcoded passwords are good for you and NOBUS).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285402"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285402" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>neil (not verified)</span> said:</p>
      <p class="date-time">November 10, 2019</p>
    </div>
    <a href="#comment-285402">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285402" class="permalink" rel="bookmark">I downloaded this thinking…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I downloaded this thinking it might work i was wrong i typed in what is my ip address and it comes right up even tells me my location looks like tor browser isnt that good after all i'm really disappointed only one out of so many was able to hide my ip the opera browser but they i know when i download it's limited to a few GB so i was looking for an unlimited vpn, ok? I just wanted you to know your program dont work! I downloaded it and started it then a small black box came up in the upper left side of my screen it ran through and on the bottom it says done so after i see that i go into my chrome browser and test to see if they can find my ip and they can! Maybe you can fix tor like after i install it on my computer and start it a "tor browser" window would open and anything that is done in the browser is hidden maybe if you guys can work on making that happen i think more people would start to use tor. Ok i hope this helps! i'm a bit jaded with all the fails today anyway good luck!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285405"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285405" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>DigtheDog (not verified)</span> said:</p>
      <p class="date-time">November 10, 2019</p>
    </div>
    <a href="#comment-285405">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285405" class="permalink" rel="bookmark">Not sure what you all did,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not sure what you all did, but TOR will not run on Win 7 in VM's anymore (v9.0 9.1)</p>
<p>Will not run under vmware nor virt box.</p>
<p>That's pretty jacked up.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285423"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285423" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 11, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285405" class="permalink" rel="bookmark">Not sure what you all did,…</a> by <span>DigtheDog (not verified)</span></p>
    <a href="#comment-285423">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285423" class="permalink" rel="bookmark">you need to install updates…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>you need to install updates on your Win 7.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285443"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285443" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 12, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285405" class="permalink" rel="bookmark">Not sure what you all did,…</a> by <span>DigtheDog (not verified)</span></p>
    <a href="#comment-285443">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285443" class="permalink" rel="bookmark">Have you tried Tails?  …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Have you tried Tails?   tails.boum.org   Should work out of the box until bug is sorted.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
