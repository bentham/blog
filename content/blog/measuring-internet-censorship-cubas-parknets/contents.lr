title: Measuring Internet Censorship in Cuba's ParkNets
---
pub_date: 2017-08-28
---
author: agrabeli
---
tags: ooniprobe
---
categories: circumvention
---
summary: This post is part of a series that highlights OONI reports which examine internet censorship in various countries around the world. Last May, the Open Observatory of Network Interference (OONI) team visited Cuba. We ran a variety of network measurement tests in Havana, Santa Clara, and Santiago de Cuba with the aim of measuring internet censorship.
---
_html_body:

<p dir="ltr">Image by Arturo Filastò (CC-BY-SA-3.0)</p>
<p dir="ltr"><em>This post is part of a series that highlights <a href="https://ooni.torproject.org/post/">OONI reports</a> which examine internet censorship in various countries around the world.</em></p>
<p>Last May, the <a href="https://ooni.torproject.org/">Open Observatory of Network Interference (OONI)</a> team visited Cuba. We ran a variety of network measurement tests in Havana, Santa Clara, and Santiago de Cuba with the aim of measuring internet censorship.</p>
<p>Today we published our findings through a new research report. This post provides an overview of our study. Read the full report <a href="https://ooni.torproject.org/post/cuba-internet-censorship-2017/"><strong>here</strong></a>.</p>
<h2>Cuba’s ParkNets, StreetNets, and Offline Internet</h2>
<p>Cuba’s internet landscape is quite unique. It only has one telecom company (<a href="http://www.etecsa.cu/">ETECSA</a>), which is state-owned and which was only introduced to the public a few years ago. </p>
<p>But Cubans cannot access the internet from the comfort of their homes. Rather, they must visit <a href="http://www.etecsa.cu/internet_conectividad/areas_wifi/">public wifi hotspots</a>. Most hotspots are located in parks, which is why we dubbed them “<em>ParkNets</em>”. Cubans therefore have a uniquely different relationship with the internet, in comparison to other countries. They don’t access the internet, they <em>visit</em> it.</p>
<p>Visiting the internet is expensive. Merely <a href="http://www.etecsa.cu/internet_conectividad/internet/">1 hour of internet access costs 1.5 CUC</a> (1.5 USD), while on average, Cubans <a href="https://www.forbes.com/forbes/welcome/?toURL=https://www.forbes.com/sites/kenrapoza/2016/04/26/guess-how-much-cubans-earn-per-month/">earn around 25 CUC per month</a>. Access to Cuba’s <em>intranet</em>, on the other hand, is more than ten times cheaper (at <a href="http://www.etecsa.cu/internet_conectividad/internet/">0.10 CUC per hour</a>), indicating that many Cubans possibly limit their browsing experience to government-approved sites and services. </p>
<p>The high cost of the internet, the <a href="http://www.etecsa.cu/internet_conectividad/areas_wifi/">limited availability of public wifi hotspots</a> across the country (and, arguably, the inconvenience of visiting them) remain the main barriers to accessing the internet in Cuba. As such, it’s probably no surprise that only <a href="http://www.internetworldstats.com/carib.htm#cu">32.5%</a> of Cuba’s population has access to the internet. </p>
<p>However, Cuba’s environment has fostered <em>alternative</em> means of accessing the internet. Over the last decade, multiple <a href="https://www.polygon.com/features/2017/5/15/15625950/cuba-secret-gaming-network">private mesh networks</a> - called “StreetNets”, or “SNets” for short - have sprung across Havana and other Cuban cities. Ethernet cables on rooftops and wifi repeaters connect hundreds of computers to private networks. <a href="https://www.polygon.com/features/2017/5/15/15625950/cuba-secret-gaming-network">StreetNets are largely used for online gaming</a>, but they are also used for a variety of other purposes, such as watching TV shows and movies, communicating, and sharing files. </p>
<p>Even though StreetNets are technically illegal, particularly since the use of wifi equipment requires a license from the Ministry of Communications, <a href="http://www.technewsworld.com/story/81646.html">authorities appear to be turning a blind eye</a>. This is likely because users <a href="https://www.polygon.com/features/2017/5/15/15625950/cuba-secret-gaming-network">refrain from discussing politics and/or sharing prohibited materials</a>. They even enforce this rule themselves. Self-censorship might be the most effective form of censorship in Cuba, even within StreetNets. </p>
<p>But Cuba’s underground internet is not restricted to StreetNets. As of 2008, digital materials - known as “El Paquete Semanal” (“The Weekly Package”), or “El Paquete” for short - are being sold in Cuba’s underground market. El Paquete consists of a <a href="https://laredcubana.blogspot.it/2016/05/inside-edition-of-el-paquete.html">variety of digital materials</a>, ranging from music, TV series, movies, and video clips, to news websites, software manuals, and classifieds. These materials <a href="http://www.cbc.ca/news/world/cuba-el-paquete-internet-wifi-havana-1.3527274">do not include pornography or other prohibited materials</a>, nor do they include content expressing criticism towards the Cuban government. We were able to confirm this when we purchased El Paquete in Cuba. </p>
<p>In a way, El Paquete emerged as a sort of substitute for broadband internet, serving as Cuba’s “<a href="https://www.theguardian.com/world/2014/dec/23/cuba-offline-internet-weekly-packet-external-hard-drives">offline internet</a>”. It enables Cubans to gain access to online content, without being online.</p>
<h2>What We Measured</h2>
<p>When we visited ParkNets across Cuba, we ran a variety of network measurement tests. First, we ran our own software, <a href="https://github.com/TheTorProject/ooni-probe">ooniprobe</a>, which is designed to measure the following:</p>
<ul>
<li><a href="https://ooni.torproject.org/nettest/web-connectivity/">Blocking of websites</a>;</li>
<li>Blocking of instant messaging apps (<a href="https://ooni.torproject.org/nettest/whatsapp/">WhatsApp</a>, <a href="https://ooni.torproject.org/nettest/facebook-messenger/">Facebook Messenger</a>, <a href="https://ooni.torproject.org/nettest/telegram/">Telegram</a>);</li>
<li>Blocking of the <a href="https://ooni.torproject.org/nettest/vanilla-tor/">Tor network</a> and <a href="https://ooni.torproject.org/nettest/tor-bridge-reachability/">Tor bridges</a>;</li>
<li>Presence of <a href="https://ooni.torproject.org/nettest/http-invalid-request-line/">middleboxes</a>;</li>
<li><a href="https://ooni.torproject.org/nettest/ndt/">Speed and performance</a> of networks.</li>
</ul>
<p>We also performed a variety of other network tests. These included traceroutes, DNS queries, and network scans, as well as custom tests measuring the latency to blocked sites and other follow-up tests in response to <a href="https://explorer.ooni.torproject.org/country/CU">ooniprobe findings</a>.</p>
<p>The testing period started on 29th May 2017 and concluded twelve days later, on 10th June 2017. Most tests were performed in ParkNets across eight vantage points in Havana, Santa Clara, and Santiago de Cuba.</p>
<p>All network measurement data collected from ooniprobe tests was automatically published on <a href="https://explorer.ooni.torproject.org/country/CU">OONI Explorer</a> and on our <a href="https://measurements.ooni.torproject.org/files/by_country/CU">measurement API</a>. </p>
<h2>What We Found</h2>
<p><a href="https://measurements.ooni.torproject.org/files/by_country/CU">OONI data</a> collected from Cuba confirms the <strong>blocking of 41 websites</strong>. Most of these sites include news outlets and blogs, as well as pro-democracy and human rights sites. Many of the blocked sites, directly or indirectly, express criticism towards Castro’s regime. </p>
<p>The graph below illustrates the types of sites that we found to be blocked.</p>
<p><img alt="Types of sites blocked in Cuba" src="/static/images/blog/inline-images/cu-13_0.png" /></p>
<p> </p>
<p>Freedom House’s <a href="https://freedomhouse.org/country/cuba">site</a> -  which publishes annual reports on civil liberties, press freedom and net freedom for most countries around the world, including Cuba - was amongst those <a href="https://explorer.ooni.torproject.org/measurement/20170602T204322Z_AS27725_77Dv0jxB6oySsvhkyiBCOwFhH2BV6BD3IhgrP0bPHiLLUPXMXT?input=http:%2F%2Ffreedomhouse.org%2Fcountry%2Fcuba">found to be blocked</a>. While it’s likely that this site was censored for being viewed as overly critical towards Cuba’s government, it’s worth noting that other international sites, which arguably express more criticism, were found to be accessible. Reporters Without Borders, for example, published a <a href="https://rsf.org/en/predator/raul-castro-0">portrait of Cuba’s President</a>, presenting him as a “Predator of Press Freedom”. Yet, we found this site to be <a href="https://explorer.ooni.torproject.org/measurement/20170602T204322Z_AS27725_77Dv0jxB6oySsvhkyiBCOwFhH2BV6BD3IhgrP0bPHiLLUPXMXT?input=https:%2F%2Frsf.org%2Fen%2Fpredator%2Fraul-castro-0">accessible</a> across Cuba. </p>
<p>Web proxies, like <a href="http://anonymouse.org/">Anonymouse</a>, were amongst those found to be blocked, limiting Cubans’ ability to circumvent censorship. It’s worth noting though that the <a href="https://www.torproject.org/">Tor anonymity network</a> was found to be <a href="https://explorer.ooni.torproject.org/measurement/20170607T175300Z_AS27725_XXfB14TYx5szrWoFiokwjWdQZIJQIuEtGWpkfaojBEPdiNsc3u">accessible</a> across the country. This is likely due to the fact that <a href="https://metrics.torproject.org/userstats-relay-country.html?start=2017-05-30&amp;end=2017-08-28&amp;country=cu">Cuba has relatively few Tor users</a>.</p>
<p>Deep Packet Inspection (DPI) technology was found to be blocking sites by resetting connections and serving (blank) block pages. It’s worth emphasizing that we only found the HTTP version of sites to be blocked, potentially enabling users to circumvent the censorship by merely accessing them over HTTPS. Many of the blocked sites, however, do not support HTTPS.</p>
<p>Skype was the only popular communications tool that we found to be censored. By examining packet traces, we noticed that the DPI middlebox was injecting RST packets because the timing was much less than that of the SYN-ACK round-trip time and had an inconsistent Time To Live (TTL). We found this interesting because, at first, it was not clear that Skype was intentionally being blocked. Other popular communications tools, such as <a href="https://explorer.ooni.torproject.org/measurement/20170607T175309Z_AS27725_a4wPIMQ7NwY7hXozjWQ9f9gSNDRzpN9kb3XLsjzW1GFbsfcMGz">Facebook Messenger</a> and <a href="https://explorer.ooni.torproject.org/measurement/20170607T175300Z_AS27725_sscXF9fCtjN7lUqAy9WewtB7HxS3dBWVJcjmuHYvHhreJosqap">WhatsApp</a>, were accessible.</p>
<p>We wrote a new test (called “latency-to-blocked”) designed to measure the latency to the blocking infrastructure by performing test connections to a control vantage point. This test showed low latency when connecting to blocked sites from Havana. The same test, however, presented higher latency from Santa Clara, and even higher latency from Santiago de Cuba. This allowed us to infer that the censorship equipment used in Cuba is most likely located in Havana (and in any case, for sure in Cuba).</p>
<p>Huawei, a Chinese multinational networking and telecommunications equipment and services company, was found to be supporting Cuba’s internet infrastructure. The server header of blocked sites, for example, pointed to Huawei equipment. ETECSA’s captive portal - through which Cubans access the internet - appears to have been created by Chinese developers, since Chinese comments are included in the source code of the portal.</p>
<p><img alt="ETECSA login page containing Chinese comments in source code" src="/static/images/blog/inline-images/cu-19_0.png" /></p>
<p>While it is clear that Cuba is using Huawei’s access points, it remains unclear whether and to what extent Huawei equipment is actually being used to implement censorship in the country.</p>
<p>Cuba’s ISP isn’t the only one blocking access to services. <a href="https://ooni.torproject.org/nettest/ndt/">OONI’s Network Diagnostic Test (NDT)</a> relies on <a href="https://www.measurementlab.net/tools/ndt/">M-Lab servers</a>, which in turn rely on Google App Engine. Initially, we weren’t able to run NDT tests in Cuba. Once we manually specified the test servers, not only were we able to run NDT, but it also became evident that Google is blocking access to Google App Engine from Cuba.</p>
<h2>Conclusion</h2>
<p>Internet censorship in Cuba does not appear to be particularly sophisticated. Cuba’s ISP only appears to be blocking the HTTP version of sites, potentially enabling users to circumvent censorship by accessing such sites over HTTPS. While Cuba’s ISP targets <em>some</em> sites that are viewed as overly critical of its government, many other international websites which also express criticism are not censored. Given the <a href="http://www.etecsa.cu/internet_conectividad/internet/">high cost of the internet</a>, rendering it inaccessible to most Cubans, perhaps the government doesn’t even <em>need</em> to invest in sophisticated internet censorship.</p>
<p>But Cuba’s internet landscape is changing. Over the last 6 years, <a href="http://www.internetlivestats.com/internet-users/cuba/">internet penetration levels have doubled</a>. Over the last 2 years, <a href="http://www.etecsa.cu/internet_conectividad/areas_wifi/">more than 400 public wifi hotspots</a> have been set up. And multiple <a href="https://www.polygon.com/features/2017/5/15/15625950/cuba-secret-gaming-network">private mesh networks</a> have emerged across the country. Cuba’s internet landscape will likely continue to evolve, but so might internet censorship. That’s why we think it’s important to continue to measure networks. Users in Cuba and around the world can <a href="https://ooni.torproject.org/install/">run ooniprobe</a> to collect data that sheds light on information controls.</p>
<p>
<em>To learn more about this study, read the full report <a href="https://ooni.torproject.org/post/cuba-internet-censorship-2017/"><strong>here</strong></a>.</em></p>

---
_comments:

<a id="comment-271089"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271089" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 28, 2017</p>
    </div>
    <a href="#comment-271089">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271089" class="permalink" rel="bookmark">Mirror :) https://ooni…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Mirror :) <a href="https://ooni.github.io/post/cuba-internet-censorship-2017/" rel="nofollow">https://ooni.github.io/post/cuba-internet-censorship-2017/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271107"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271107" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 29, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271089" class="permalink" rel="bookmark">Mirror :) https://ooni…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-271107">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271107" class="permalink" rel="bookmark">Doesn&#039;t seem to work :/</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Doesn't seem to work :/</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-271090"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271090" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 28, 2017</p>
    </div>
    <a href="#comment-271090">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271090" class="permalink" rel="bookmark">Did you test Signal&#039;s…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Did you test Signal's censorship? What about Signal's domain fronting option, did it help circumvent the block (if any)?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271150"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271150" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  agrabeli
  </article>
    <div class="comment-header">
      <p class="comment__submitted">agrabeli said:</p>
      <p class="date-time">August 31, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271090" class="permalink" rel="bookmark">Did you test Signal&#039;s…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-271150">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271150" class="permalink" rel="bookmark">Signal worked for us in Cuba…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Signal worked for us in Cuba.</p>
<p>We don't (currently) have a test that measures the reachability of Signal. But we have tests for WhatsApp, Facebook Messenger, and Telegram. </p>
<p>You can find a list of ooniprobe tests here: <a href="https://ooni.torproject.org/nettest/" rel="nofollow">https://ooni.torproject.org/nettest/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-271095"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271095" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Aprizm (not verified)</span> said:</p>
      <p class="date-time">August 28, 2017</p>
    </div>
    <a href="#comment-271095">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271095" class="permalink" rel="bookmark">and I bet you hackers in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>and I bet you hackers in cuba already know how to bypass the captive portal :D if not, hmu on twitter and ill show you how ;)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271100"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271100" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>cubalibre (not verified)</span> said:</p>
      <p class="date-time">August 28, 2017</p>
    </div>
    <a href="#comment-271100">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271100" class="permalink" rel="bookmark">you forgot 2 or 3 little…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>you forgot 2 or 3 little things that i wish share if you allow me :<br />
- you do not speak about jails and mental hospital , police station, tourism office and rescue team, airport &amp; boat com.<br />
(has the staff a free access , have the client one ?)<br />
- guantanamo is a cuban territory occupied by u.s force so maybe the censorship is coming a bit from this military camp and from their boats/submarines no ?.<br />
(ooni rapport , satellites ?)<br />
- the politic/policy &amp; modus operandi in cuba (like in a lot of others countries) follow the kiss principle : built it yourself with your money &amp; your efforts &amp; your free time.<br />
In short , if you want a large, sophisticated, cheap internet , do it !<br />
the same apply for electricity, service, food, water, good etc. (like in a lot of others countries that you do think know very well).</p>
<p># OONI data collected from Cuba confirms the blocking of 41 websites. Most of these sites include news outlets and blogs, as well as pro-democracy and human rights sites. Many of the blocked sites, directly or indirectly, express criticism towards Castro’s regime.<br />
- many sites are fomenting murders, selling drugs, hiding u.s agent &amp; e.u criminals  - they are dressed with the virgin souls they have taken _ but who pay these impostors, charlatans &amp; terrorists ?<br />
- i did not know that pornography could bring a piece of democracy in cuba , in others countries this thing is the beginning of the end for the human rights.<br />
- i do not understand what you mean by "illegal materials".<br />
- El paquete is a gift given to the underground (perestroika).<br />
- are mesh net controlled/set by usa army ? (you do not know but you found a chinese firm strange no ?)<br />
cuba is now opened to the tourism &amp; trade and that for an international challenge : it implies a better infrastructure which an easy/cheap access internet in a near future maybe.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271152"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271152" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  agrabeli
  </article>
    <div class="comment-header">
      <p class="comment__submitted">agrabeli said:</p>
      <p class="date-time">August 31, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271100" class="permalink" rel="bookmark">you forgot 2 or 3 little…</a> by <span>cubalibre (not verified)</span></p>
    <a href="#comment-271152">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271152" class="permalink" rel="bookmark">Our study presents some…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Our study presents some limitations: <a href="https://ooni.torproject.org/post/cuba-internet-censorship-2017/#acknowledgement-of-limitations" rel="nofollow">https://ooni.torproject.org/post/cuba-internet-censorship-2017/#acknowl…</a> </p>
<p>You can learn more about our methodology here: <a href="https://ooni.torproject.org/post/cuba-internet-censorship-2017/#methodology" rel="nofollow">https://ooni.torproject.org/post/cuba-internet-censorship-2017/#methodo…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-271102"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271102" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>El Pidio Valdez (not verified)</span> said:</p>
      <p class="date-time">August 28, 2017</p>
    </div>
    <a href="#comment-271102">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271102" class="permalink" rel="bookmark">Awesome, I loved this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Awesome, I loved this article, you have done an excellent work, thank you for sharing this with the rest of the world, and thank you for providing us a network like TOR to remove all those censors and protect our privacy! <strong><em>Viva Cuba libre!</em></strong></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271110"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271110" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Citizen (not verified)</span> said:</p>
      <p class="date-time">August 29, 2017</p>
    </div>
    <a href="#comment-271110">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271110" class="permalink" rel="bookmark">What about censoship in U.S…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about censoship in U.S.A.?<br />
 It's a really FREE as in fredom country?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271151"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271151" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  agrabeli
  </article>
    <div class="comment-header">
      <p class="comment__submitted">agrabeli said:</p>
      <p class="date-time">August 31, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271110" class="permalink" rel="bookmark">What about censoship in U.S…</a> by <span>Citizen (not verified)</span></p>
    <a href="#comment-271151">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271151" class="permalink" rel="bookmark">You can find millions of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can find millions of network measurements from the U.S. here: <a href="https://explorer.ooni.torproject.org/country/US" rel="nofollow">https://explorer.ooni.torproject.org/country/US</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271178"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271178" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 04, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to agrabeli</p>
    <a href="#comment-271178">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271178" class="permalink" rel="bookmark">I believe the US at least…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I believe the US at least block some overseas medicine sites? Are they not included in the measurement?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-271117"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271117" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>NoZombie (not verified)</span> said:</p>
      <p class="date-time">August 29, 2017</p>
    </div>
    <a href="#comment-271117">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271117" class="permalink" rel="bookmark">Freedom do not exist…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Freedom do not exist<br />
Financial and corporation war, exist.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271128"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271128" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>jaap van till (not verified)</span> said:</p>
      <p class="date-time">August 30, 2017</p>
    </div>
    <a href="#comment-271128">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271128" class="permalink" rel="bookmark">Do they already use the …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do they already use the @Firechatapp automatic mesh network ? You can exchange chat messages in discussion groups and photo's but it does not give Internet site access.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271131"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271131" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 30, 2017</p>
    </div>
    <a href="#comment-271131">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271131" class="permalink" rel="bookmark">Have you been questioned by…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Have you been questioned by police or other surveillance agency?  I guess you look like an anomaly, and taking in account that you are foreign tourists it can be suspicious enough to put you in jail.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271140"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271140" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>joke (not verified)</span> said:</p>
      <p class="date-time">August 31, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271131" class="permalink" rel="bookmark">Have you been questioned by…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-271140">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271140" class="permalink" rel="bookmark">they are working with an…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>they are working with an authorization &amp; for join-venture contact : they cannot be arrested , they are guest &amp; welcome. cuba is far more democratic &amp; far less corrupted than u.s.a &amp; e.u but cuba is at the beginning of its development.<br />
for corruption &amp; jail see spain Ukraine_porenchenko france (the more corrupted) etc.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271177"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271177" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 04, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271140" class="permalink" rel="bookmark">they are working with an…</a> by <span>joke (not verified)</span></p>
    <a href="#comment-271177">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271177" class="permalink" rel="bookmark">#AccurateUserName</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>#AccurateUserName</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
