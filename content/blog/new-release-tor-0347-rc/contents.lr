title: New Release: Tor 0.3.4.7-rc
---
pub_date: 2018-08-24
---
author: nickm
---
summary: Tor 0.3.4.7-rc fixes several small compilation, portability, and correctness issues in previous versions of Tor. This version is a release candidate: if no serious bugs are found, we expect that the stable 0.3.4 release will be (almost) the same as this release.
---
_html_body:

<p>There's a new release candidate available for download. If you build Tor from source, you can download the source code for Tor 0.3.4.6-rc from the <a href="https://www.torproject.org/download/download.html">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release in the coming weeks.</p>
<p>Remember, this is an not yet a stable release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.3.4.7-rc fixes several small compilation, portability, and correctness issues in previous versions of Tor. This version is a release candidate: if no serious bugs are found, we expect that the stable 0.3.4 release will be (almost) the same as this release.</p>
<h2>Changes in version 0.3.4.7-rc - 2018-08-24</h2>
<ul>
<li>Minor features (bug workaround):
<ul>
<li>Compile correctly on systems that provide the C11 stdatomic.h header, but where C11 atomic functions don't actually compile. Closes ticket <a href="https://bugs.torproject.org/26779">26779</a>; workaround for Debian issue 903709.</li>
</ul>
</li>
<li>Minor features (continuous integration):
<ul>
<li>Backport Travis rust distcheck to 0.3.3. Closes ticket <a href="https://bugs.torproject.org/24629">24629</a>.</li>
<li>Enable macOS builds in our Travis CI configuration. Closes ticket <a href="https://bugs.torproject.org/24629">24629</a>.</li>
<li>Install libcap-dev and libseccomp2-dev so these optional dependencies get tested on Travis CI. Closes ticket <a href="https://bugs.torproject.org/26560">26560</a>.</li>
<li>Only post Appveyor IRC notifications when the build fails. Implements ticket <a href="https://bugs.torproject.org/27275">27275</a>.</li>
<li>Run asciidoc during Travis CI. Implements ticket <a href="https://bugs.torproject.org/27087">27087</a>.</li>
<li>Use ccache in our Travis CI configuration. Closes ticket <a href="https://bugs.torproject.org/26952">26952</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (continuous integration, rust):
<ul>
<li>Use cargo cache in our Travis CI configuration. Closes ticket <a href="https://bugs.torproject.org/26952">26952</a>.</li>
</ul>
</li>
<li>Minor features (directory authorities):
<ul>
<li>Authorities no longer vote to make the subprotocol version "LinkAuth=1" a requirement: it is unsupportable with NSS, and hasn't been needed since Tor 0.3.0.1-alpha. Closes ticket <a href="https://bugs.torproject.org/27286">27286</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the August 7 2018 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/27089">27089</a>.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, windows):
<ul>
<li>Don't link or search for pthreads when building for Windows, even if we are using build environment (like mingw) that provides a pthreads library. Fixes bug <a href="https://bugs.torproject.org/27081">27081</a>; bugfix on 0.1.0.1-rc.</li>
</ul>
</li>
<li>Minor bugfixes (continuous integration):
<ul>
<li>Improve Appveyor CI IRC logging. Generate correct branches and URLs for pull requests and tags. Use unambiguous short commits. Fixes bug <a href="https://bugs.torproject.org/26979">26979</a>; bugfix on master.</li>
<li>Build with zstd on macOS. Fixes bug <a href="https://bugs.torproject.org/27090">27090</a>; bugfix on 0.3.1.5-alpha.</li>
<li>Pass the module flags to distcheck configure, and log the flags before running configure. (Backported to 0.2.9 and later as a precaution.) Fixes bug <a href="https://bugs.torproject.org/27088">27088</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (in-process restart):
<ul>
<li>Always call tor_free_all() when leaving tor_run_main(). When we did not, restarting tor in-process would cause an assertion failure. Fixes bug <a href="https://bugs.torproject.org/26948">26948</a>; bugfix on 0.3.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (linux seccomp2 sandbox):
<ul>
<li>Fix a bug in out sandboxing rules for the openat() syscall. Previously, no openat() call would be permitted, which would break filesystem operations on recent glibc versions. Fixes bug <a href="https://bugs.torproject.org/25440">25440</a>; bugfix on 0.2.9.15. Diagnosis and patch from Daniel Pinto.</li>
</ul>
</li>
<li>Minor bugfixes (onion services):
<ul>
<li>Fix bug that causes services to not ever rotate their descriptors if they were getting SIGHUPed often. Fixes bug <a href="https://bugs.torproject.org/26932">26932</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (portability):
<ul>
<li>Fix compilation of the unit tests on GNU/Hurd, which does not define PATH_MAX. Fixes bug <a href="https://bugs.torproject.org/26873">26873</a>; bugfix on 0.3.3.1-alpha. Patch from "paulusASol".</li>
</ul>
</li>
<li>Minor bugfixes (rust):
<ul>
<li>Backport test_rust.sh from master. Fixes bug <a href="https://bugs.torproject.org/26497">26497</a>; bugfix on 0.3.1.5-alpha.</li>
<li>Consistently use ../../.. as a fallback for $abs_top_srcdir in test_rust.sh. Fixes bug <a href="https://bugs.torproject.org/27093">27093</a>; bugfix on 0.3.4.3-alpha.</li>
<li>Protover parsing was accepting the presence of whitespace in version strings, which the C implementation would choke on, e.g. "Desc=1\t,2". Fixes bug <a href="https://bugs.torproject.org/27177">27177</a>; bugfix on 0.3.3.5-rc.</li>
<li>Protover parsing was ignoring a 2nd hyphen and everything after it, accepting entries like "Link=1-5-foo". Fixes bug <a href="https://bugs.torproject.org/27164">27164</a>; bugfix on 0.3.3.1-alpha.</li>
<li>Stop setting $CARGO_HOME. cargo will use the user's $CARGO_HOME, or $HOME/.cargo by default. Fixes bug <a href="https://bugs.torproject.org/26497">26497</a>; bugfix on 0.3.1.5-alpha.</li>
<li>cd to ${abs_top_builddir}/src/rust before running cargo in src/test/test_rust.sh. This makes the working directory consistent between builds and tests. Fixes bug <a href="https://bugs.torproject.org/26497">26497</a>; bugfix on 0.3.3.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing, bootstrap):
<ul>
<li>When calculating bootstrap progress, check exit policies and the exit flag. Previously, Tor would only check the exit flag, which caused race conditions in small and fast networks like chutney. Fixes bug <a href="https://bugs.torproject.org/27236">27236</a>; bugfix on 0.2.6.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing, openssl compatibility):
<ul>
<li>Our "tortls/cert_matches_key" unit test no longer relies on OpenSSL internals. Previously, it relied on unsupported OpenSSL behavior in a way that caused it to crash with OpenSSL 1.0.2p. Fixes bug <a href="https://bugs.torproject.org/27226">27226</a>; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (Windows, compilation):
<ul>
<li>Silence a compilation warning on MSVC 2017 and clang-cl. Fixes bug <a href="https://bugs.torproject.org/27185">27185</a>; bugfix on 0.2.2.2-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-276569"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276569" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 25, 2018</p>
    </div>
    <a href="#comment-276569">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276569" class="permalink" rel="bookmark">Thanks for another timely…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for another timely release!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276571"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276571" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Wulfric (not verified)</span> said:</p>
      <p class="date-time">August 25, 2018</p>
    </div>
    <a href="#comment-276571">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276571" class="permalink" rel="bookmark">how to use tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><strong>how to use tor</strong></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276575"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276575" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>larry  (not verified)</span> said:</p>
      <p class="date-time">August 25, 2018</p>
    </div>
    <a href="#comment-276575">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276575" class="permalink" rel="bookmark">how abour make Tor OS 
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>how abour make Tor OS </p>
<p>thanks for tor team.</p>
<p>a lot of new ideas but tor is not safe connection </p>
<p>we need new protocol or new project.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276576"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276576" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>hello (not verified)</span> said:</p>
      <p class="date-time">August 26, 2018</p>
    </div>
    <a href="#comment-276576">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276576" class="permalink" rel="bookmark">i would like know if you…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i would like know if you plan open a free vpn service with should allow : vpn + tor or tor + vpn<br />
 unfortunatelly cryptostorm is working with the five eyes/corrupted (February 9, 2018)<br />
<a href="https://www.bestvpn.com/review/cryptostorm-beta/" rel="nofollow">https://www.bestvpn.com/review/cryptostorm-beta/</a><br />
(August 8, 2018)<br />
<a href="https://www.bestvpn.com/free-vpn-services/" rel="nofollow">https://www.bestvpn.com/free-vpn-services/</a><br />
<a href="https://windscribe.com/guides/linux" rel="nofollow">https://windscribe.com/guides/linux</a><br />
mac/windows available<br />
<a href="https://addons.mozilla.org/en-US/firefox/addon/windscribe/" rel="nofollow">https://addons.mozilla.org/en-US/firefox/addon/windscribe/</a><br />
the addon is not recommended with tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276613"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276613" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>YESSSS!!! (not verified)</span> said:</p>
      <p class="date-time">September 01, 2018</p>
    </div>
    <a href="#comment-276613">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276613" class="permalink" rel="bookmark">thnak you so much !! Iam…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thank you so much !! Iam dying for <strong>quantum</strong></p>
</div>
  </div>
</article>
<!-- Comment END -->
