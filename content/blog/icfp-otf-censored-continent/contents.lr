title: Censored continent: understanding the use of tools during information controls in Africa: Nigeria, Cameroon, Uganda, and Zimbabwe as case studies.
---
pub_date: 2020-09-18
---
author: antonela
---
tags:

censorship
censorship circumvention
---
categories: circumvention
---
summary: Today, we are sharing here the publication of his research report, titled "Censored continent: understanding the use of tools during Information controls in Africa: Nigeria, Cameroon, Uganda, and Zimbabwe as case studies."
---
_html_body:

<p>Between 2019 and 2020, The Tor Project has had the opportunity to serve as the host organization of OTF Information Controls Fellow, <a href="https://www.opentech.fund/about/people/babtunde-okunoye/">Babatunde Okunoye</a>. Today, we are sharing here the publication of his research report, titled "Censored continent: understanding the use of tools during Information controls in Africa: Nigeria, Cameroon, Uganda, and Zimbabwe as case studies."</p>
<p>As part of his fellowship, Babatunde examined the use of Internet censorship circumvention tools in Cameroon, Nigeria, Uganda, and Zimbabwe, four countries in Africa with varying degrees of Internet censorship, including Internet bandwidth throttling, social media app restrictions, and website blocks. Interviews were done with 33 people, including students, civil society members, people in business, and teachers, revealing how communities mobilized to defeat censorship.</p>
<p>Important findings include: </p>
<ul>
<li>Civil society played an important role in mobilizing people to use circumvention tools.</li>
<li>Some of the most important VPN adoption reasons were community recommendations, cost of use, and ease of use.</li>
<li>Messaging apps like Signal and Telegram, which were unknown to government censors and were not blocked, served as alternative messaging channels when more popular apps like Facebook and WhatsApp were blocked.</li>
<li>People used innovative means of sharing VPNs, such as through USBs and Bluetooth, when downloads were no longer possible from official sources such as websites of VPN makers and smartphone App stores.</li>
</ul>
<p>Full-length report:<br />
<a href="https://research.torproject.org/techreports/icfp-censored-continent-2020-07-31.pdf">https://research.torproject.org/techreports/icfp-censored-continent-202…</a></p>
<p>Media impact:<br />
<a href="https://www.cfr.org/blog/technologies-freedom-enabling-democracy-africa" target="_blank">https://www.cfr.org/blog/technologies-freedom-enabling-democracy-africa</a></p>
<p>His research in the field helped us to inform the user research work on improving Tor Browser as an essential circumvention tool.</p>
<p>If you have questions or comments regarding this study, you can reach out to Babatunde directly: to.csgroups[at]gmail.com.</p>

---
_comments:

<a id="comment-289511"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289511" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 21, 2020</p>
    </div>
    <a href="#comment-289511">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289511" class="permalink" rel="bookmark">Fascinating.  What about…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fascinating.  What about Kenya, CAR, DRC, etc?</p>
<p><a href="https://en.wikipedia.org/wiki/Kenya" rel="nofollow">https://en.wikipedia.org/wiki/Kenya</a><br />
<a href="https://www.hrw.org/world-report/2020/country-chapters/kenya" rel="nofollow">https://www.hrw.org/world-report/2020/country-chapters/kenya</a></p>
<p>(Many recent incidents of harrasment of dissidents, bloggers, and human rights groups by government officials).</p>
<p><a href="https://en.wikipedia.org/wiki/Central_African_Republic" rel="nofollow">https://en.wikipedia.org/wiki/Central_African_Republic</a><br />
<a href="https://www.hrw.org/world-report/2020/country-chapters/central-african-republic" rel="nofollow">https://www.hrw.org/world-report/2020/country-chapters/central-african-…</a></p>
<p>(Political violence directed at disparaged subpopulations, human rights workers, etc.)</p>
<p><a href="https://en.wikipedia.org/wiki/Democratic_Republic_of_the_Congo" rel="nofollow">https://en.wikipedia.org/wiki/Democratic_Republic_of_the_Congo</a><br />
<a href="https://www.hrw.org/news/2020/07/08/justice-risk-democratic-republic-congo" rel="nofollow">https://www.hrw.org/news/2020/07/08/justice-risk-democratic-republic-co…</a></p>
<p>(Courts in danger of being "captured" by special interests, a familiar story, etc.)</p>
<p>Enabling women to access good information and protecting children from surveillance capitalism are two closely related issues of importance all over the world including Africa.</p>
<p>Also, it would be interesting to study how Tor promotes/retards the spread of QAnon and David Icke paranoid conspiracy theories, which are rapidly spreading all over the world--- particularly, it seems, among women (see previous sentence).</p>
<p>If corporate sponsors are so fab, why doesn't Google or a satellite communications startup help TP bring uncensorable internet to refugee camps?   (Whatever happened to Google's widely publicized balloon project?  It was supposed to help dissidents but seems to have morphed into yet another technical surveillance program in what the US DOJ calls "anarchist jurisdictions" such as Seattle.)</p>
<p>Harambee!</p>
<p>&gt; People used innovative means of sharing VPNs, such as through USBs and Bluetooth, when downloads were no longer possible from official sources such as websites of VPN makers and smartphone App stores.</p>
<p>Perhaps there is a lesson there for TP itself: offer to communicate via OnionShare, not just (inherently insecure and non-anonymous) email?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289926"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289926" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Tunde (not verified)</span> said:</p>
      <p class="date-time">October 16, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289511" class="permalink" rel="bookmark">Fascinating.  What about…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289926">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289926" class="permalink" rel="bookmark">Thank you, those are…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you, those are important research suggestions to explore!<br />
Also I know Google and Facebook have some public Internet options in Africa. But of course more is needed!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289517"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289517" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>tortoo (not verified)</span> said:</p>
      <p class="date-time">September 22, 2020</p>
    </div>
    <a href="#comment-289517">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289517" class="permalink" rel="bookmark">How can censorship now be…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How can censorship now be avoided given the recent global drug busts and assertions by lea that the dark-web is now transparent?  </p>
<p>I'm thinking that many tor users will now be seeking reassurance from y'all. Go for it!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289568"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289568" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Obunga-Bonds (not verified)</span> said:</p>
      <p class="date-time">September 23, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289517" class="permalink" rel="bookmark">How can censorship now be…</a> by <span>tortoo (not verified)</span></p>
    <a href="#comment-289568">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289568" class="permalink" rel="bookmark">Point-to-Point Physical &quot;Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Point-to-Point Physical "Tor Dark Web" is indeed transparent!  </p>
<p>It should have been a warning with all the other busts.  But with all the physical traffic low, it makes targeting drug mules a lot easier ect.....also not wanting to turn the whole population into drug addicts or drug dealers.</p>
<p>Tor is more or less about Free Speech and Privacy.  Not to protect a Physical Drug cartels operations.  It is good for LEO, due to the nature of them not having to worry about being targeted by NSA or FBI tools in their investigations.  Unless of course the LEO is investigating Hillary Clinton or government abuse.  They will get targeted.</p>
<p>Any other security level pretty much does not entail Tor.  Although Edward Snowden promoted it with a Tor sticker on a laptop.  I am pretty sure, he would be considered the .1% club.  Entry at the door says "No one need apply" at the top of the club door.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289688"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289688" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 25, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289568" class="permalink" rel="bookmark">Point-to-Point Physical &quot;Tor…</a> by <span>Obunga-Bonds (not verified)</span></p>
    <a href="#comment-289688">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289688" class="permalink" rel="bookmark">&gt; assertions by lea that the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; assertions by lea that the dark-web is now transparent? </p>
<p>&gt; Point-to-Point Physical "Tor Dark Web" is indeed transparent! </p>
<p>Not aware of that, or even what it might mean.  I have never heard the term "point-to-point" or "physical Tor dark web" before.  I have heard the term "transparent" used to mean in the cybersecurity context to mean two completely opposite things ("transparent" in the sense of "invisible to the hapless user"--- e.g. Stingrays are "transparent" to phone users--- vs. "transparent" in the sense of "easy to understand/monitor"--- e.g. excessive "DHS secrecy is a good example of why US voters need much improved government transparency").</p>
<p>Can you give a link to an official statement by an LEA (e.g. FBI.gov) or a story at a reputable news site (e.g. TheIntercept.com?)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289691"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289691" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 25, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289568" class="permalink" rel="bookmark">Point-to-Point Physical &quot;Tor…</a> by <span>Obunga-Bonds (not verified)</span></p>
    <a href="#comment-289691">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289691" class="permalink" rel="bookmark">A certain lack of cohesive…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A certain lack of cohesive semantic content suggests a certain line of thought concerning how the comment to which I am replying came into existence.  (Wired.com recently covered an example in which such suspicions expressed by wary readers turned out to be justified.)</p>
<p>Regardless, just in case anyone (perhaps someone in Africa) who is not yet a Tor user but who is considering using Tor is reading this, an important correction to some fake memes often encountered in various places on the Internet:</p>
<p>&gt; Although Edward Snowden promoted it with a Tor sticker on a laptop. I am pretty sure, he would be considered the .1% club. Entry at the door says "No one need apply" at the top of the club door.</p>
<p>Edward Snowden was indeed an early adopter of Tails, the "amnesiac" out-of-the-box Linux system (see tails.boum.org), and he did indeed place a Tor sticker on his personal laptop, and in exile he has continued to recommend that political dissidents and potential leakers use Tails.  Just before leaving the US he also helped organize a cryptoparty where experts taught ordinary citizens how to use GPG and such.</p>
<p>Tails depends upon Tor and Tor Browser, but incorporates many other privacy and cybersecurity protections.  Tails is provided as an ISO image which anyone can download and burn on a "live" DVD--- and optionally can then make a live USB, which is more convenient to use on a daily basis.  It is essential to use GPG to verify the detached signature of the ISO image before burning this to the live DVD (see tails.boum.org for instructions).</p>
<p>"Live" means that when you reboot your computer with the DVD or USB inserted, your computer will be running the Tails system, not the one installed on your hard drive.  "Amnesiac" means that Tails will not leave any traces of your activities on your hard drive.</p>
<p>The Snowden leaks included briefings on several secret programs intended to break Tails; the NSA people concluded (in about 2010-2012) that Tails would be almost impossible to compromise if used according to the (excellent) documentation at tails.boum.org.  That might have changed in the years since, but despite the FUD above, the most knowledgeable commentators seem to agree that NSA/FBI/etc still have great trouble breaking Tails itself.  </p>
<p>The busts alluded to above appear to involve people doing dangerous things which did not involve Tails.  It is true that FBI has been able to compromise certain onion sites (which use Tor in a crucial way), but these compromises appear to have happened by means not involving a direct attack on Tor software, but rather on something dangerous the administrators of the onions were doing in order to log into their own onion site.  The onion sites featured in those widely publicized breaches were, as suggested above, alleged illegal drug sale sites.  Most onion sites, however, are operated  by reputable and badly needed news organizations such as The Intercept, the paper cofounded by Glenn Greenwald, who broke the Snowden leaks with Laura Poitras, Barton Gellman, and some colleagues from The Guardian, the paper he was then writing for.  The Guardian also has an onion site.</p>
<p>Leak on, people! 8&gt; 8&gt; 8&gt;</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-289639"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289639" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 23, 2020</p>
    </div>
    <a href="#comment-289639">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289639" class="permalink" rel="bookmark">Seems you guys forgot to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Seems you guys forgot to include the Tor Blog itself in the case study.</p>
<p>Still waiting on that open forum.. how many months has it been?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289927"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289927" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Tunde (not verified)</span> said:</p>
      <p class="date-time">October 16, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289639" class="permalink" rel="bookmark">Seems you guys forgot to…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289927">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289927" class="permalink" rel="bookmark">Thank you. I am not sure…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you. I am not sure what you meant, but if you meant that we did not include the Tor browser in our study, we certainly did. The study covered all tools people used to circumvent Internet censorship in the four African countries, so the Tor browser was included. There were a number of interviewees who reported using Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
