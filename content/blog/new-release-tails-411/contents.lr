title: New Release: Tails 4.11
---
pub_date: 2020-09-22
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_4.10/" rel="nofollow">many security vulnerabilities</a>. You should upgrade as soon as possible.</p>

<h1>New features</h1>

<ul>
<li>
We added a new feature of the Persistent Storage to save the settings from the Welcome Screen: language, keyboard, and additional settings.<br />
To restore your settings when starting Tails, unlock your Persistent Storage in the Welcome Screen.
</li>
</ul>

<h1>Changes and updates</h1>

<ul>
<li>
Update <em>Tor Browser</em> to <a href="https://blog.torproject.org/new-release-tor-browser-100" rel="nofollow">10.0</a>.
</li>
<li>
Update <em>Thunderbird</em> to <a href="https://www.thunderbird.net/en-US/thunderbird/68.12.0/releasenotes/" rel="nofollow">68.12</a>.
</li>
<li>
Update <em>Linux</em> to 5.7.17. This should improve the support for newer hardware (graphics, Wi-Fi, etc.).
</li>
<li>
Configure <em>KeePassXC</em> to use the new default location <em>Passwords.kdbx</em>. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/17286" rel="nofollow">#17286</a>)
</li>
<li>
Update python3-trezor to 0.11.6 to add compatibility with the new Trezor Model T.
</li>
</ul>

<h1>Fixed problems</h1>

<ul>
<li>Disable the feature to <strong>Turn on Wi-Fi Hotspot</strong> in the Wi-Fi settings because it doesn't work in Tails. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/17887" rel="nofollow">#17887</a>)</li>
</ul>

<p>For more details, read our <a href="https://gitlab.tails.boum.org/tails/tails/-/blob/master/debian/changelog" rel="nofollow">changelog</a>.<br />
<a rel="nofollow"></a></p>

<h1>Known issues</h1>

<p>None specific to this release.<br />
See the list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">long-standing issues</a>.</p>

<h1>Get Tails 4.11</h1>

<h2>To upgrade your Tails USB stick and keep your persistent storage</h2>

<ul>
<li>
Automatic upgrades are available from Tails 4.2 or later to 4.11.
</li>
<li>
If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a <a href="https://tails.boum.org/doc/upgrade/#manual" rel="nofollow">manual upgrade</a>.
</li>
</ul>

<h2>To install Tails on a new USB stick</h2>

<p>Follow our installation instructions:</p>

<ul>
<li><a href="https://tails.boum.org/install/win/" rel="nofollow">Install from Windows</a></li>
<li><a href="https://tails.boum.org/install/mac/" rel="nofollow">Install from macOS</a></li>
<li><a href="https://tails.boum.org/install/linux/" rel="nofollow">Install from Linux</a></li>
</ul>

<p>The Persistent Storage on the USB stick will be lost if you install instead of upgrading.</p>

<h2>To download only</h2>

<p>If you don't need installation or upgrade instructions, you can download Tails 4.11 directly:</p>

<ul>
<li><a href="https://tails.boum.org/install/download/" rel="nofollow">For USB sticks (USB image)</a></li>
<li><a href="https://tails.boum.org/install/download-iso/" rel="nofollow">For DVDs and virtual machines (ISO image)</a></li>
</ul>

<h1>What's coming up?</h1>

<p>Tails 4.12 is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for October 20.<br />
Have a look at our <a href="https://tails.boum.org/contribute/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.<br />
We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/donate/?r=4.11" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev" rel="nofollow">talk to us</a>!</p>

<h1>Support and feedback</h1>

<p>      For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

