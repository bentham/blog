title: Tor 0.2.2.11-alpha and 0.2.2.12-alpha are out
---
pub_date: 2010-04-24
---
author: phobos
---
tags:

bug fixes
alpha release
performance improvements
---
categories: releases
---
_html_body:

<p>Tor 0.2.2.12-alpha fixes a critical bug in how directory authorities<br />
handle and vote on descriptors. It was causing relays to drop out of<br />
the consensus.</p>

<p>Tor 0.2.2.11-alpha fixes yet another instance of broken OpenSSL libraries<br />
that was causing some relays to drop out of the consensus.</p>

<p>(Windows bundles will be available whenever Andrew gets around to making<br />
them; we're trying to stick to a policy of announcing alphas on time<br />
rather than waiting for every package.)</p>

<p><a href="https://www.torproject.org/download.html.en" rel="nofollow">https://www.torproject.org/download.html.en</a></p>

<p>Original announcement is at <a href="http://archives.seul.org/or/talk/Apr-2010/msg00174.html" rel="nofollow">http://archives.seul.org/or/talk/Apr-2010/msg00174.html</a></p>

<p>Changes in version 0.2.2.12-alpha - 2010-04-20<br />
  o Major bugfixes:<br />
    - Many relays have been falling out of the consensus lately because<br />
      not enough authorities know about their descriptor for them to get<br />
      a majority of votes. When we deprecated the v2 directory protocol,<br />
      we got rid of the only way that v3 authorities can hear from each<br />
      other about other descriptors. Now authorities examine every v3<br />
      vote for new descriptors, and fetch them from that authority. Bugfix<br />
      on 0.2.1.23.<br />
    - Fix two typos in tor_vasprintf() that broke the compile on Windows,<br />
      and a warning in or.h related to bandwidth_weight_rule_t that<br />
      prevented clean compile on OS X. Fixes bug 1363; bugfix on<br />
      0.2.2.11-alpha.<br />
    - Fix a segfault on relays when DirReqStatistics is enabled<br />
      and 24 hours pass. Bug found by keb. Fixes bug 1365; bugfix on<br />
      0.2.2.11-alpha.</p>

<p>  o Minor bugfixes:<br />
    - Demote a confusing TLS warning that relay operators might get when<br />
      someone tries to talk to their OrPort. It is neither the operator's<br />
      fault nor can they do anything about it. Fixes bug 1364; bugfix<br />
      on 0.2.0.14-alpha.</p>

<p>Changes in version 0.2.2.11-alpha - 2010-04-15<br />
  o Major bugfixes:<br />
    - Directory mirrors were fetching relay descriptors only from v2<br />
      directory authorities, rather than v3 authorities like they should.<br />
      Only 2 v2 authorities remain (compared to 7 v3 authorities), leading<br />
      to a serious bottleneck. Bugfix on 0.2.0.9-alpha. Fixes bug 1324.<br />
    - Fix a parsing error that made every possible value of<br />
      CircPriorityHalflifeMsec get treated as "1 msec". Bugfix<br />
      on 0.2.2.7-alpha. Rename CircPriorityHalflifeMsec to<br />
      CircuitPriorityHalflifeMsec, so authorities can tell newer relays<br />
      about the option without breaking older ones.<br />
    - Fix SSL renegotiation behavior on OpenSSL versions like on Centos<br />
      that claim to be earlier than 0.9.8m, but which have in reality<br />
      backported huge swaths of 0.9.8m or 0.9.8n renegotiation<br />
      behavior. Possible fix for some cases of bug 1346.</p>

<p>  o Minor features:<br />
    - Experiment with a more aggressive approach to preventing clients<br />
      from making one-hop exit streams. Exit relays who want to try it<br />
      out can set "RefuseUnknownExits 1" in their torrc, and then look<br />
      for "Attempt by %s to open a stream" log messages. Let us know<br />
      how it goes!<br />
    - Add support for statically linking zlib by specifying<br />
      --enable-static-zlib, to go with our support for statically linking<br />
      openssl and libevent. Resolves bug 1358.</p>

<p>  o Minor bugfixes:<br />
    - Fix a segfault that happens whenever a Tor client that is using<br />
      libevent2's bufferevents gets a hup signal. Bugfix on 0.2.2.5-alpha;<br />
      fixes bug 1341.<br />
    - When we cleaned up the contrib/tor-exit-notice.html file, we left<br />
      out the first line. Fixes bug 1295.<br />
    - When building the manpage from a tarball, we required asciidoc, but<br />
      the asciidoc -&gt; roff/html conversion was already done for the<br />
      tarball. Make 'make' complain only when we need asciidoc (either<br />
      because we're compiling directly from git, or because we altered<br />
      the asciidoc manpage in the tarball). Bugfix on 0.2.2.9-alpha.<br />
    - When none of the directory authorities vote on any params, Tor<br />
      segfaulted when trying to make the consensus from the votes. We<br />
      didn't trigger the bug in practice, because authorities do include<br />
      params in their votes. Bugfix on 0.2.2.10-alpha; fixes bug 1322.</p>

<p>  o Testsuite fixes:<br />
    - In the util/threads test, no longer free the test_mutex before all<br />
      worker threads have finished. Bugfix on 0.2.1.6-alpha.<br />
    - The master thread could starve the worker threads quite badly on<br />
      certain systems, causing them to run only partially in the allowed<br />
      window. This resulted in test failures. Now the master thread sleeps<br />
      occasionally for a few microseconds while the two worker-threads<br />
      compete for the mutex. Bugfix on 0.2.0.1-alpha.</p>

---
_comments:

<a id="comment-5470"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5470" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 24, 2010</p>
    </div>
    <a href="#comment-5470">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5470" class="permalink" rel="bookmark">Hi!!!!!!!!!!!!!
What&#039;s the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi!!!!!!!!!!!!!</p>
<p>What's the purpose of alpha releases?!!!!!!! Usually, they're experimental releases, much more unstable than betas!!!! is a Tor alpha enough good to be used and included into Tor Bundles?!!!!!!! I knew that when you're dealing with privacy applications you shouldn't use "new" stuffs, because they aren't enough tested!!! It isn't a good idea to use unstable software in tor-bundles!!!!!!!!!! but if you're willing to include the Tor alphas into your TorBB, and this is what you are doing, then i've suppose them to be stable!!!! is it?!!!!!!!</p>
<p>Hah!!! Did you noticed that also VIDALIA has been updated!!!!!!!!!!? v0.2.8 is available to download here <a href="https://www.torproject.org/vidalia/" rel="nofollow">https://www.torproject.org/vidalia/</a> !!!!!!!!!!! though there isn't any super news!!!</p>
<p>bye!!!!!!!!!!!!!<br />
~bee!!!!!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5481"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5481" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">April 25, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5470" class="permalink" rel="bookmark">Hi!!!!!!!!!!!!!
What&#039;s the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5481">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5481" class="permalink" rel="bookmark">The difference between alpha</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The difference between alpha and stable is here, <a href="https://www.torproject.org/download#packagediff" rel="nofollow">https://www.torproject.org/download#packagediff</a></p>
<p>The entire linux tbb is alpha right now, so it gets the alpha version of tor.</p>
<p>Yes, we released a new version of vidalia, but since it doesn't build in osx 10.5+ nor windows, there's little point to telling the world to upgrade.  Vidalia 0.2.9 should be out soon with the things that broke in 0.2.8, namely universal plug and play.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5529"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5529" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 01, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-5529">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5529" class="permalink" rel="bookmark">Phobos,
Pretty please (with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Phobos,</p>
<p>Pretty please (with a cherry on top) _stop_ bee from posting.  Nothing it writes is useful enough to allow it's spamming and it's unfounded attacks upon Tor-BB and Tor in general.  If it has so many problems with Tor and Tor-BB then why must it post here and why must it spam it's  project here?  </p>
<p>Not only that but the thing called bee can not accept constructive criticisms, one poster wrote about bee's "project" and how it is flawed in it's downloads scripts ( see 10th post here: <a href="https://blog.torproject.org/blog/torbutton-release-125-google-captchas-and-addonsmozillaorg#comments" rel="nofollow">https://blog.torproject.org/blog/torbutton-release-125-google-captchas-…</a>  ) yet all bee did was attack the poster who was offering constructive criticism.  Bee could not accept it is wrong and needed to learn a thing or two about security.  For the sake of Tor users who may not be aware of bee's antics and insanity, please stop allowing it to post.</p>
<p>Have you not noticed that everywhere the thing called bee posts it merely attempts to cause trouble and 'dust ups'?  At the Firefox blogs it is banned, every post it has made here, save a couple in Mike's current TorButton blog entry are filled with venom and "why bee is smarter than everyone else".</p>
<p>Please for the sake of  stop the thing called bee from posting!</p>
<p>Thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5539"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5539" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 01, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5529" class="permalink" rel="bookmark">Phobos,
Pretty please (with</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5539">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5539" class="permalink" rel="bookmark">The signal to noise ratio of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The signal to noise ratio of bee's posts is very low.  However, there are some gems and flashes of insight amongst the noise.  The writing style is highly annoying, but a quick regex in mutt removes the nonsense.  </p>
<p>I would think less of phobos and tor if they censored posts on content, at least those that aren't full of spam.  It seems since the blog moved to moderated comments, the amount of spam has fallen to zero.</p>
<p>If only there was an rss feed for comments.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-5543"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5543" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 02, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5529" class="permalink" rel="bookmark">Phobos,
Pretty please (with</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5543">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5543" class="permalink" rel="bookmark">As someone else said in this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As someone else said in this thread, bee's posts have a low signal to noise ratio, but there are good parts in there.</p>
<p>We generally don't care what everyone else is doing.  We're not trying to maintain a position at the center of the herd.  We're trying to build an anonymity network, this includes allowing all to post and comment. </p>
<p>Smart users can make their own decisions as to bee's statements.</p>
<p>I only filter out obvious spam (link farms, bayesian word clusters that make no sense in any language, and product advertisements).  As soon as we start exercising editorial control we run into various legal liability and other issues we've been warned not to cross.  I'd rather spend the money we receive on making tor better, not fighting lawsuits.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-5563"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5563" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 03, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5529" class="permalink" rel="bookmark">Phobos,
Pretty please (with</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5563">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5563" class="permalink" rel="bookmark">I think that you&#039;re very</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think that you're very confused and utterly jealous!!!!!!!!!!!!! although i like your trolling!!!!!!!!! it's amusing!!!!!!!!!!!!! yeah!! it's sad, but amusing!!!!!!!! i'm unusual to reply to posts like your one, but you've to know that i ain't blocked from the <em>Firefox Addons</em> blog, there isn't any filter and i could post new messages at any time!!!!! But after getting a few of my messages deleted, i've dropped out of it!!! i've almost quit to read their blog too!!!!!!!!!! I never liked AMOs because they're sold out to Google (AMO is Googlezilla!!!!!!!!!). They want to keep it hidden even if everybody knows that Google is the source of almost all donations given to Mozilla (they're called "donations", but they're actually "bribes"!!! now, guess why Google is the default search engine of firefox!!!!!!!!), and they'll delete your posts if you dare to point out the way AMO behaves (they have centralized everything!!! they're very totalitarian and very well oriented towards abuses; as they're already doing some!!!!!!!). </p>
<p>You think that i dislike the Tor Project, yet i made a <strong><em>Tor</em> Browser Bundle for Linux</strong>!!!!!!!!!!!!!!!!! Yeah, my Tor Browser Bundle for Linux is <strong>FactorBee</strong>!!!!!!!!!! Misconceptions?!!!!!  I made it, and it's normal if i'm promoting it sometimes!!!!!!!!! Who else has to do it?!!!!!!!!!!!!!!!! and, yeah, it's indeed better than the official Tor Browser Bundle for Linux made by the TorProject!!!!!!! Did you said something about constructive criticisms?!!!!!!!! I'll be very happy to learn something from you!!! I would like to listen your wise suggestions and to copy the ideas you used to realized <em>your</em> Tor Browser Bundle, but i guess that i'll have to wait for the day you'll make one!!!!!!!!!!!!!!!!!!!!!!! You always say that my posts are useless; ok, to save your precious time, i'll avoid to talk about the null usefulness of the posts sent by you!!!!!!!!!!! lol!!!!!!!!!! ~bee!!!!!!!!!!!!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5567"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5567" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 03, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5563" class="permalink" rel="bookmark">I think that you&#039;re very</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5567">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5567" class="permalink" rel="bookmark">OK, I&#039;ll bite, but look out,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><b>OK, I'll bite, but look out, you may not like where I bite:</b></p>
<p>AMO _did_ ban you because it's obvious you can't write like a rational person, the reason you can't post there anymore is they mark ANY post with excessive exclamation points as spam, that's because of _you_ and _you_ alone!  So you just pick and choose your "truth" huh?</p>
<p>You did not make a Tor-BB for Linux, you made simply made a portable version of Firefox and Tor, etc; big whoop.  My gripe is that you to claim Tor-BB is insecure and your bundle is SOOO much better, yea right, sure.  Less informed people might actually believe you and that's dangerous.  </p>
<p><b>Let me cite you above:</b><br />
"...and, yeah, it's indeed better than the official Tor Browser Bundle for Linux made by the TorProject".  That is spamming buddy boy, either that or help Tor and stop bashing it with broad and unsubstantiated claims [rolls eyes], the only other option is to clearly state why you think Tor-BB is not as good as yours.</p>
<p><b>Here is another instance of your fear mongering about Tor-BB:</b><br />
"The official Tor Browser Bundle, is very much more UNSAFE than factorbee!!!!"</p>
<p><b>And here is yet another example of your ranting:</b><br />
"Well; i want to compare this Tor Browser Bundle for GNU/Linux (the official belonging to the TOR Project), with my one (Factorbee)!!!!!!!!!!!!<br />
This TorBB of the torproject hasn't been well-made!!!! It actually sucks!!!!! Six months for this crap lol!!!!!!!!!!"</p>
<p>While I have you here, let me ask you:  why do you feel the need to write like you do?  You make yourself look like an idiot and someone who yearns for attention; I should probably not feed the troll but I can't help it.</p>
<p>FWIW, I have made my own version of portable Firefox (rightly and legally labeled Iceweasel), Tor, Polipo and Vidalia for Linux (Ubuntu) like a year ago, I did it for myself and talked about it on IRC with Steven when he was first making TorBB for windows.  I for one did not feel like I needed to be a braggart and one who wanted to bash Tor folks; besides, I knew they would make a better package than I could for _other_ users besides myself.  </p>
<p>Also FWIW, considering you called me out, I was the one to first proposed TorButton with Mike Perry on IRC a long time ago.  I first tried to make it after talking about the idea on IRC but I did not (and still do not) have the skill; thank goodness MIke too up the effort.  </p>
<ul><i>You did nothing special, you act like you turned water into wine...jeeze.</i></ul>
<p><b>You want an example of where you can learn?  Let me quote another person who already tried to school you but you threw a tantrum like all kids do:</b><br />
-----------------------------------------<br />
"bee's posting style aside, bee doesn't understand security nor firefox. bee is simply adding in various plugins/extensions and calling it good.</p>
<p>case in point, look at download.sh from the source bundle:</p>
<p># <a href="https://www.torproject.org/vidalia/" rel="nofollow">https://www.torproject.org/vidalia/</a><br />
wget "<a href="https://www.torproject.org/vidalia/dist/vidalia-0.2.7.tar.gz" rel="nofollow">https://www.torproject.org/vidalia/dist/vidalia-0.2.7.tar.gz</a>"<br />
let counter++</p>
<p>The script doesn't download the pgp signature, nor check it. This is true for every download link in that file. And then to make the user feel better:</p>
<p>md5sum * &gt; ../sources.md5sum<br />
sha1sum * &gt; ../sources.sha1sum</p>
<p>Rather than bother checking the md5 or sha1 hashes from the source website, just create them all AFTER the download and compare against that. If you have a corrupt/malicious download, you have no way to know that using bee's method.</p>
<p>Also, "wget" is called from the environment PATH variable, not specifically hardcoded to a path. While easier to code, anything can be called wget and download.sh will run it.</p>
<p>Also, all of the scripts use "/tmp/factorbee", which as a known path, is exploitable. mkstemp would be preferable.</p>
<p>The tor config file has this rampant problem too,</p>
<p>DataDirectory /tmp/factorbee/app/tor-cache<br />
GeoIPFile /tmp/factorbee/app/tor/share/tor/geoip"<br />
----------------------------------------</p>
<p><b>You are darn lucky Phobos, et al., still allow you to post, I for one would have put an end to it a long time ago.  If you really wanted to be helpful you would do as Phobos first asked you to do:</b><br />
"Before this turns into any more of a flamewar, if bee has suggestions, we're happy to collaborate with him/her on making tbb better. This antagonistic relationship is not helping anyone. Our goal is with the browser bundle is to help non-technical users protect their privacy and anonymity online. We try to do this by making a complete bundle that just works without any configuration. This appears to be what factorbee is trying to do as well. Fighting about it is just a waste of resources."</p>
<p>OK, schools out, now go home, or stay and learn to post like the rest of us and stop bashing Tor-BB.  You can write all you want about Factorbee, but stop making unsubstantiated claims that Factorbee is much better and more secure and more 'safe' than Tor-BB.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5572"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5572" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 04, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5567" class="permalink" rel="bookmark">OK, I&#039;ll bite, but look out,</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5572">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5572" class="permalink" rel="bookmark">Hi!!!!!!!!!!!
It seems to me</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi!!!!!!!!!!!<br />
It seems to me like you're going to freak out!!!!!!!!!!!!!!!!! Btw, ok, you claim you did a Tor Browser Bundle, but you kept it for you, and now you're jealous because i made my one for everyone!!!!!!!!!!(and it has got a lot of success too!!!!!!!) I won't answer again to the whole post you pasted up here, because i already did it!!!!!!!! The suggestions and the flaws you claim are nonsense!!!!! lol!!!!!!!!!! At the AMO blog i ain't blocked, but they're of course usual to remove all the messages they dislike!!!!!!!!!!!! it may be possible that if i'll post again they'll remove my posts!!!!!! Factorbee is better than the official TorBB!!!! For example there isn't "firefox portable" but a real custom built of firefox!!!! i built it, deactivating at compiling-time a lot of things (like the whole plug-in support, just to name one!!), it works without polipo (firefox is patched with chrisd's patch to avoid SOCKETS timeouts), i even changed the Firefox (nodoka) icon to another one to recognize it better in the tasks bar!!!lol!!!!!! but also because unlike in the official torbb firefox doesn't crash when you start a download lol!!!!!!!!!!!!!!!!!!!!! and so on....!!!!!!!! I could write the list of all features related to the scripts of factorbee, very useful to improve the safeness of the whole bundle, but to keep this short, i'll only say that the official TorBB isn't yet made with the original idea to support external scripts!!!!!!!!!!! i already said this though!!!!!!!!!!! it's very too long to write all the reasons why factorbee is better!!!!!!!!!! you could understand them by reading the pages about factorbee at the HONEY BEE NET!!!!!!! and, yeah, finally, it's more secure &amp; safe &amp; stable than the official TorBB!!!!!!!!! I made it in a very accurate way, and with love!!!!!!!!!!!!!!!!! I didn't said that the official TorBB is to trash altogether, but compared to factorbee, it's far behind and indeed it sucks!!!!!!!!!! bye!!!!!!!!!! ~bee!!!!!!!!!!!!!!!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div></div><a id="comment-5472"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5472" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 24, 2010</p>
    </div>
    <a href="#comment-5472">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5472" class="permalink" rel="bookmark">How exactly does the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How exactly does the aggressive approach to prevent clients from making 1 hop streams work? Does it mean exits can verify the route a give packet has taken through the network? More information on this would be nice.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-5480"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5480" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 25, 2010</p>
    </div>
    <a href="#comment-5480">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5480" class="permalink" rel="bookmark">0.2.2.13-alpha source now</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>0.2.2.13-alpha source now available on download page</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5482"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5482" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">April 25, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5480" class="permalink" rel="bookmark">0.2.2.13-alpha source now</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5482">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5482" class="permalink" rel="bookmark">Yes.  However, until</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes.  However, until packages are available for a few operating systems, we don't announce too quickly.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-5514"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5514" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 30, 2010</p>
    </div>
    <a href="#comment-5514">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5514" class="permalink" rel="bookmark">No tor packages available</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No tor packages available for ubuntu 10.04 LTS (lucid)?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5524"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5524" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 01, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5514" class="permalink" rel="bookmark">No tor packages available</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5524">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5524" class="permalink" rel="bookmark">Not yet.  I hear the karmic</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not yet.  I hear the karmic packages work fine for now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
