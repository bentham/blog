title: New Release: Tails 3.12
---
pub_date: 2019-01-29
---
author: tails
---
tags:

anonymous operating system
tails
tails releases
---
categories:

partners
releases
---
_html_body:

<p>This release fixes many security vulnerabilities. You should upgrade as soon as possible.</p>

<h1>Changes</h1>

<h2>New features</h2>

<h3>New installation methods</h3>

<p>The biggest news for 3.12 is that we completely changed the installation methods for Tails.<br />
In short, instead of downloading an ISO image (a format originally designed for CDs), you now download Tails as a <strong>USB image</strong>: an image of the data as it needs to be written to the USB stick.</p>

<ul>
<li>
<a href="https://tails.boum.org/install/mac/usb-overview/" rel="nofollow">For macOS</a>, the new method is much simpler as it uses a graphical tool (<a href="https://www.balena.io/etcher/" rel="nofollow"><em>Etcher</em></a>) instead of the command line.
</li>
<li>
<a href="https://tails.boum.org/install/win/usb-overview/" rel="nofollow">For Windows</a>, the new method is much faster as it doesn't require 2 USB sticks and an intermediary Tails anymore. The resulting USB stick also works better on newer computers with UEFI.
</li>
<li>
<a href="https://tails.boum.org/install/linux/usb-overview/" rel="nofollow">For Debian and Ubuntu</a>, the new method uses a native application (<em>GNOME Disks</em>) and you don't have to install <em>Tails Installer</em> anymore.
</li>
<li>
<a href="https://tails.boum.org/install/linux/usb-overview/" rel="nofollow">For other Linux distributions</a>, the new method is faster as it doesn't require 2 USB sticks and an intermediary Tails anymore.
</li>
</ul>

<p>We are still providing ISO images for people using DVDs or virtual machines.<br />
The methods for upgrading Tails remain the same.</p>

<h2>Upgrades and changes</h2>

<ul>
<li>
Starting Tails should be a bit faster on most machines. (<a href="https://redmine.tails.boum.org/code/issues/15915" rel="nofollow">#15915</a>)
</li>
<li>
Tell users to use sudo when they try to use su on the command line.
</li>
</ul>

<h3>Included software</h3>

<ul>
<li>
Update Linux to 4.19. Update Intel and AMD microcodes and most firmware packages. This should improve the support for newer hardware (graphics, Wi-Fi, etc.).
</li>
<li>
Remove <em>Liferea</em>, as announced in <a href="./version_3.9/" rel="nofollow">Tails 3.9</a>.
</li>
<li>
Update <em>Tor Browser</em> to 8.0.5.
</li>
<li>
Update <em>Thunderbird</em> to <a href="https://www.thunderbird.net/en-US/thunderbird/60.4.0/releasenotes/" rel="nofollow">60.4.0</a>.
</li>
</ul>

<h2>Fixed problems</h2>

<ul>
<li>Fix the black screen when starting Tails with some Intel graphics cards. (<a href="https://redmine.tails.boum.org/code/issues/16224" rel="nofollow">#16224</a>)</li>
</ul>

<p>For more details, read our <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">changelog</a>.<br />
<a rel="nofollow"></a></p>

<h1>Known issues</h1>

<p>See also the list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">long-standing issues</a>.</p>

<h3>Tails fails to start a second time on some computers (<a href="https://redmine.tails.boum.org/code/issues/16389" rel="nofollow">#16389</a>)</h3>

<p>On some computers, after installing Tails to a USB stick, Tails starts a first time but fails to start a second time. In some cases, only BIOS (Legacy) was affected and the USB stick was not listed in the Boot Menu.<br />
We are still investigating the issue, so if it happens to you, please report your findings by email to <a href="mailto:tails-testers@boum.org" rel="nofollow">tails-testers@boum.org</a>. Mention the model of the computer and the USB stick. This mailing list is <a href="https://lists.autistici.org/list/tails-testers/" rel="nofollow">archived publicly</a>.<br />
To fix this issue:</p>

<ol>
<li>
Reinstall your USB stick using the same installation method.
</li>
<li>
Start Tails for the first time and <a href="https://tails.boum.org/doc/first_steps/startup_options/administration_password/" rel="nofollow">set up an administration password</a>.
</li>
<li>
Choose Applications ▸ System Tools ▸ Root Terminal to open a Root Terminal.
</li>
<li>
Execute the following command:<br />
sgdisk --recompute-chs /dev/bilibop
</li>
</ol>

<p>You can also test an experimental image:</p>

<ol>
<li>
<a href="https://nightly.tails.boum.org/build_Tails_ISO_bugfix-16389-recompute-chs/lastSuccessful/archive/build-artifacts/" rel="nofollow">Download the <em>.img</em> file from our development server</a>.
</li>
<li>
Install it using the same installation methods.<br />
We don't provide any OpenPGP signature or other verification technique for this test image. Please only use it for testing.
</li>
</ol>

<h1>Get Tails 3.12</h1>

<ul>
<li>
To install, follow our <a href="https://tails.boum.org/install/" rel="nofollow">installation instructions</a>.
</li>
<li>
To upgrade, automatic upgrades are available from 3.10, 3.10.1, 3.11, and 3.12~rc1 to 3.12.<br />
If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a <a href="https://tails.boum.org/upgrade/" rel="nofollow">manual upgrade</a>.
</li>
<li>
Download Tails 3.12:
<ul>
<li><a href="https://tails.boum.org/install/download/" rel="nofollow">For USB sticks (USB image)</a></li>
<li><a href="https://tails.boum.org/install/download-iso/" rel="nofollow">For DVDs and virtual machines (ISO image)</a></li>
</ul>
</li>
</ul>

<h1>What's coming up?</h1>

<p>Tails 3.13 is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for March 19.<br />
Have a look at our <a href="https://tails.boum.org/contribute/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.<br />
We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/donate?r=3.12" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev" rel="nofollow">talk to us</a>!</p>

<h1>Support and feedback</h1>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

