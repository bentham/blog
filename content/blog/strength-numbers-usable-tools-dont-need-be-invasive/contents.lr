title: Strength in Numbers: Usable Tools Don’t Need To Be Invasive
---
pub_date: 2018-12-19
---
author: antonela
---
tags:

Strength in Numbers
usability
community
UX
---
categories:

community
usability
---
summary:

To improve user experience, most of the tech industry relies on analyzing their users’ behavioral data to drive decision making. Tor does things differently. 
---
_html_body:

<p><em>This post is one in a series of blogs to complement our 2018 crowdfunding campaign, Strength in Numbers. Anonymity loves company and we are all safer and stronger when we work together. <a href="https://torproject.org/donate/donate-sin-us-bp">Please contribute today</a>, and your gift will be matched by Mozilla.</em><br />
   <br />
Usability is about making sure anyone, no matter their technical background, can use a tool. Usability and user experience (UX) work has gained a lot of importance in the last decade as the tech industry has grown. To improve user experience, most of the tech industry relies on analyzing their users’ behavioral data to drive decision making. Mechanisms for collecting this data are often invasive and performed without consent from users, who may never be told their behavior is being analyzed for this purpose. The same means used to collect behavioral data is also responsible for aiding the surveillance economy.</p>
<p>Tor does things differently. We refuse to collect this type of invasive data. Our approach to usability is built on respecting and safeguarding the privacy of our users.  We test our hypotheses and make observations in the safest way possible; in most cases, we do this work in-person with our users, not by collecting data about their behavior.</p>
<p>This year, we have focused on connecting with communities in what is sometimes referred to as the Global South. We have met with Tor users in India, Uganda, Colombia, and Kenya. This immersion has allowed us to carry out usability tests in person, so we can see first-hand if what we are building is serving people in different contexts with different levels of technical understanding. Knowing the reality of our users helps us understand their context, empathize with them, and consider solutions to meet their needs.</p>
<p>Running small-scale, short, open-ended, qualitative user tests on specific improvements we could make to Tor Browser allows us to get to know our users better and can bring their various mental models and levels of technical knowledge to light. In the evaluations we carried out, 93% of the people we met said they thought they needed some protection online, but there was a shortage of knowledge about what to do about it.</p>
<p>We met people like Jon, an environmental activist and journalist in Hoima, Uganda, who uses Tor to anonymously publish his blog.</p>
<p>Hoima is an oil city located 200 kilometers from Kampala, the capital of Uganda, where some 30,000 people live. Alison Macrina, leader of the Community team, and I, as part of the Usability team, visited Hoima in April this year to run a digital security workshop and conduct user tests with a group of environmental activists. Five minutes after the workshop started, the light was cut off. When we talk about access, we need to consider whether or not there is available technical infrastructure ready to allow users to access the open web. We found that in addition to infrastructure challenges, several threats--including the hijacking of electronic devices by local police, or the current political party in power, forcing journalists to declassify their sources--were common in most of those communities.</p>
<p>Conducting these usability tests allows us to reach people who use our software in extreme conditions, with poor infrastructure, expensive data packages, or old hardware and learn how we can better build tools for their needs. It would be selfish not to ask ourselves about these contexts and put those user stories in our software development roadmap. Creating technology that respects our users is a design decision, and one that we have always chosen.</p>
<p>We believe that if we can make our product usable for people without technical knowledge, all users will benefit, and that is what we’re striving for. <a href="https://torproject.org/donate/donate-sin-us-bp">Your donation</a> can help us reach this goal by allowing us to visit more people around the world who use Tor and collect their feedback face to face, rather than by using invasive means like the rest of the industry.</p>
<p><a href="https://torproject.org/donate/donate-sin-us-bp"><img alt="donate button" src="/static/images/blog/inline-images/tor-donate-button_10.png" class="align-center" /></a></p>
<p>In 2019, we need to reinforce our efforts to make secure and private browsing usable and to empower our community in solidarity. Our impact is not defined by numbers, but by bringing a user experience that helps real people to access the internet safely. You can help us reach this goal <a href="https://torproject.org/donate/donate-sin-us-bp">by making a donation</a>. If you give before the end of 2018, Mozilla will match your donation, and you’ll have twice the impact.</p>

---
_comments:

<a id="comment-279136"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279136" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2018</p>
    </div>
    <a href="#comment-279136">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279136" class="permalink" rel="bookmark">Thanks for the post, and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the post, and thanks to TP for doing things differently!</p>
<p>I would love to see TP reaching out more to other NGOs on this issue.  One NGO with which TP already works closely is Debian Project.  For many years some Debian users begged Debian for, essentially, onion mirrors, but until TP got involved Debian ignored us.  So thanks again to GK and others for creating and maintaining the onion mirrors.  And please try to make using the onions the default for all Debian users, as Tails already does.</p>
<p>I want to point out an opportunity for TP to repeat its success by persuading Debian to do things right in how Debian collects information about Debian user activity.  Debian has pretty much always featured a package called "popularity contest" (popcon) by which the system collects information on how often each user calls a command or utility (which is part of some Debian package) and somehow contacts debian.org to report on the user's activities.  In principle installing popcon is option, but I notice that even if you ask for it not to be installed, the installer script cheerfully announces that popcon has been installed.  (Which makes Debian sound just a bit like Facebook, ugh.)</p>
<p>The idea (when popcon was introduced 15 or 20 years ago) was that Debian can use the data to decide which packages to drop in future editions of Debian.  The problem of course is that popcon has apparently never been secure against a global adversary (the usage data has apparently never been encrypted or protected in any way).  Naturally people who worry about their privacy and about global adversaries do not knowingly choose to participate in popcon, with the result that privacy-minded packages used by privacy minded people are unfairly penalized.</p>
<p>So I hope TP will consider pressuring Debian to rewrite popcon so it uses onions.  Probably popcon should depend upon OnionShare and it should send usage data in strongly encrypted form to an onion run by Debian, possibly with some dummy traffic or other tricks to make things harder for global adversaries.  Because Debian certainly should not be trying to make things *easy* for them.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279293"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279293" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 09, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279136" class="permalink" rel="bookmark">Thanks for the post, and…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279293">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279293" class="permalink" rel="bookmark">The upload of popcon results…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The upload of popcon results is not plaintext, it is encrypted with GPG. There is also work in progress to move to using HTTPS instead of HTTP (<a href="https://bugs.debian.org/880121" rel="nofollow">https://bugs.debian.org/880121</a>) for the upload. One issue is that this creates a dependency on ca-certificates which a user may not want to have installed because they actually don't want to trust the CAs. An Onion service would solve this issue as the address is self-authenticating, but would require a dependency on tor.</p>
<p>Some things in Debian already use tor if it is available. One application is dirsrv from GnuPG which will fetch keys over Tor if a client is available. It would be cool to see popularity-contest do something similar.</p>
<p>If everyone installs tor to upload their popcon results but then doesn't use it again, this is a lot of clients that are trying to keep their consensus up-to-date but without actually using the network which would be another thing to consider.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-279137"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279137" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2018</p>
    </div>
    <a href="#comment-279137">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279137" class="permalink" rel="bookmark">our software in extreme…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>our software in extreme conditions, with poor infrastructure, expensive data packages, or old hardware</p></blockquote>
<p>Therefore, you dropped support for Windows XP and Vista.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279171"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279171" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279137" class="permalink" rel="bookmark">our software in extreme…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279171">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279171" class="permalink" rel="bookmark">Some suggestions from…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Some suggestions from another Tor user:</p>
<p>If you have an older computer which runs M$ but want to use Tor, provided that your computer uses a 64 bit CPU and will boot from a DVD or USB drive, you can download (and verify) the Tails ISO and use that to burn a live DVD (see tails.boum.org).  If your computer is a tablet which does not have a DVD R/W drive, provided that it has a USB port you can buy an external DVD R/W drive and use that to burn the DVD.</p>
<p>If you boot your computer using the DVD you can then use Tor Browser (Tails uses latest version available from <a href="http://www.torproject.org" rel="nofollow">www.torproject.org</a>).  You can store downloaded files on an encrypted data USB (you can make a LUKS encrypted volume on a USB using a simple utility which comes with Tails).</p>
<p>After booting your computer from the Tails DVD, you can create a bootable USB using a provided script you can create an encrypted "persistent volume" on the USB and store documents, and you can also install software from the Debian repositories and have it available the next time you boot from the USB.  The USB is also easier and faster to update to the next Tails edition (if the USB update script fails, you can just repeat the process with the latest Tails ISO burned to a new DVD). </p>
<p>Tails comes with many useful utilities including libreoffice and anonymization tools, so you can use it to prepare anonymized documents or videos.  It also comes with OnionShare and Pidgin with OTR, so you can share documents anonymously.</p>
<p>When you boot from the USB, you have the choice of mounting or not the encrypted volume (by providing the passphrase).  For surfing it is safest not to mount it.  Rather the idea is that you can mount the encrypted volume when you are working offline, for example preparing a document you intend to publish or share anonymously.  When you are surfing with the persistent volume not mounted, you can store downloaded files on a separate data USB if your computer has a second USB port; when offline you can boot again with the encrypted volume enabled if you expect to use the item often.</p>
<p>I find that switching between on-line and offline Tails sessions and using encrypted data USB drives as an everyday activity is much easier than it might sound.</p>
<p>Tails is working an a "Tails server" which will apparently be separate from Tails as it presently is formulated.  I believe the next edition will feature the much improved current version of OnionShare.  In future more secure onion technologies will likely be introduced (unless USG outlaws encryption and/or Tor).</p>
<p>It would be wonderful if Tor Project could find funding to resurrect Tor Messenger or a new encrypted anonymous chat engine.  The creation of Tor Browser was a major landmark in the development of the Tor community and I believe that the creation of a successful Messenger would have an even greater impact in terms of growing the Tor user community worldwide.  While Sukhbir was frank in discussing thorny technical problems which prevented TM from getting out of beta, I worry that an undisclosed issue may have been that TP received an NSL with gag order requiring TP on pain of life in prison to put in a backdoor.  That certainly could explain the very unfortunate demise of TM.  EFF is involved in a lawsuit which might enable TP to safely divulge such an NSL, if EFF wins the case.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279180"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279180" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279137" class="permalink" rel="bookmark">our software in extreme…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279180">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279180" class="permalink" rel="bookmark">Extended support for Windows…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Extended support for Windows XP ended on April 8, 2014 (<a href="https://en.wikipedia.org/wiki/Windows_XP" rel="nofollow">https://en.wikipedia.org/wiki/Windows_XP</a>) and extended support for Windows Vista ended on April 11, 2017 (<a href="https://en.wikipedia.org/wiki/Windows_Vista" rel="nofollow">https://en.wikipedia.org/wiki/Windows_Vista</a>). Therefore, both no longer receives security updates from Microsoft. Using Tor in such system is like locking your window but keeping your door wide open, aka pointless.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279181"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279181" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279137" class="permalink" rel="bookmark">our software in extreme…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279181">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279181" class="permalink" rel="bookmark">Extended support for Windows…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Extended support for Windows XP ended on April 8, 2014 (<a href="https://en.wikipedia.org/wiki/Windows_XP" rel="nofollow">https://en.wikipedia.org/wiki/Windows_XP</a>) and extended support for Windows Vista ended on April 11, 2017 (<a href="https://en.wikipedia.org/wiki/Windows_Vista" rel="nofollow">https://en.wikipedia.org/wiki/Windows_Vista</a>). Therefore, both no longer receives security updates from Microsoft. Using Tor in such system is like locking your window but keeping your door wide open, aka pointless.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279189"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279189" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>User (not verified)</span> said:</p>
      <p class="date-time">December 25, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279137" class="permalink" rel="bookmark">our software in extreme…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279189">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279189" class="permalink" rel="bookmark">Let me second that remark…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Let me second that remark.<br />
Dear TorProject, restore XP support for Tor Browser!<br />
 XP users, please support this request!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279199"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279199" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279137" class="permalink" rel="bookmark">our software in extreme…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279199">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279199" class="permalink" rel="bookmark">Tor can&#039;t protect you if you…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor can't protect you if you're using a system that reached end-of-life years ago, doesn't get security updates any more and could be easily exploited. If you're using hardware that doesn't work with newer versions of Windows, you could get some lean Linux distribution aimed at legacy hardware.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279209"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279209" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous_002 (not verified)</span> said:</p>
      <p class="date-time">December 29, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279137" class="permalink" rel="bookmark">our software in extreme…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279209">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279209" class="permalink" rel="bookmark">I&#039;d like to add that I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'd like to add that I stopped updating Tor, because of a lack of support for older AddOns. The 'vanilla' Firefox gets more and more unusable for me, the higher the version number climbs. I would be fine if the fuctionality had stayed at the Firefox 3.0 update and only security updates would have been made.<br />
(Sorry if my sentences sound wierd, English is not my first language.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279278"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279278" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 05, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279137" class="permalink" rel="bookmark">our software in extreme…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279278">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279278" class="permalink" rel="bookmark">FirefoxESR dropped support…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><em><strong>FirefoxESR</strong></em> dropped support for XP and Vista. Therefore, many migrated to liveUSB and Linux. Many government institutions and schools around the world did for other reasons. Xubuntu, Lubuntu, Linux Mint (Xfce), elementary OS, Zorin OS Core (Lite or Education), Linux Lite, Ubuntu MATE -- take your pick. These are a handful of free or pay-what-you-want distributions based on Debian (good support) but designed for beginners or old hardware and run as a bootable liveUSB or liveDVD in RAM without touching your hard drive or SSD unless you tell it to install. "Search" on Distrowatch on the top right page header or scroll down the right side to see the popularity ranking.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279305"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279305" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  antonela
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Antonela</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Antonela said:</p>
      <p class="date-time">January 09, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279137" class="permalink" rel="bookmark">our software in extreme…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279305">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279305" class="permalink" rel="bookmark">Unfortunately, ESR60 doesn&#039;t…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Unfortunately, ESR60 doesn't support Windows XP and Vista. Since Tor Browser is based on it, is hard for us to have that compatibility.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-279191"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279191" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Nadeem saifi (not verified)</span> said:</p>
      <p class="date-time">December 25, 2018</p>
    </div>
    <a href="#comment-279191">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279191" class="permalink" rel="bookmark">That is very important to me…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That is very important to me even I am searching something so help me</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279226"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279226" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Apostle (not verified)</span> said:</p>
      <p class="date-time">January 01, 2019</p>
    </div>
    <a href="#comment-279226">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279226" class="permalink" rel="bookmark">I do like the safety in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I do like the safety in numbers paradigm. Maybe the counter-approach to data analysing by Big Tech is to give them salt and pepper data which looks likes everyone else's. I wonder if that could be possible given our technology know how?</p>
<p>Rather than encrypt everything, put everything out into the open that is fake. In plain sight. Since Big Tech does 'fake', why not ordinary citizens? Two can play at that game.</p>
<p>If our data is fake, then Big Tech, those in Authority and Criminals have nothing unique to hold onto. Therefore identification is stopped in its tracks. The game ends.</p>
<p>Maybe some smart young boffin out there can construct such a system for mankind.</p>
</div>
  </div>
</article>
<!-- Comment END -->
