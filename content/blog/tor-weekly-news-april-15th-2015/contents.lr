title: Tor Weekly News — April 15th, 2015
---
pub_date: 2015-04-15
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the fifteenth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor Browser 4.0.8 is out</h1>

<p>Mike Perry <a href="https://blog.torproject.org/blog/tor-browser-408-released" rel="nofollow">announced</a> a new stable release by the Tor Browser team. This version is exactly the same as 4.0.7, which was briefly advertised to users last week but then withdrawn because a <a href="https://bugs.torproject.org/15637" rel="nofollow">bug</a> would have caused it to endlessly recommend updating.</p>

<p>This release includes Tor 0.2.5.12, which fixes the recent <a href="https://bugs.torproject.org/15600" rel="nofollow">onion service</a> and <a href="https://bugs.torproject.org/15601" rel="nofollow">client</a> crash bugs.</p>

<p>There is no corresponding Tor Browser alpha update; that series will become the new stable release in a couple of weeks.</p>

<p>Download your copy of Tor Browser 4.0.8 from the <a href="https://www.torproject.org/projects/torbrowser.html" rel="nofollow">project page</a>, or over the in-browser update system.</p>

<h1>Hidden services that aren’t hidden</h1>

<p>As the name implies, Tor hidden services (also known as “onion services”) are location-hidden and anonymous, just like regular Tor clients. There may be instances, however, in which a potential hidden service operator doesn’t much care about being hidden: they are more interested in the other unique properties of hidden services, like free end-to-end encryption and authentication, or they want to prevent their users from accidentally sending information non-anonymously. For example, even though everyone knows who Facebook are and where to find them, their users still have things to gain from <a href="https://blog.torproject.org/blog/facebook-hidden-services-and-https-certs" rel="nofollow">using their .onion address</a>.</p>

<p>At the moment, these kinds of services are still forced to use the regular hidden service protocol, meaning they connect to <a href="https://www.torproject.org/docs/hidden-services" rel="nofollow">rendezvous points</a> over a Tor circuit. Hiding someone who doesn’t want to be hidden is an inefficient use of network resources, and needlessly slows down connections to the service in question, so Tor developers have been discussing the possibility of enabling “direct onion services”, which sacrifice anonymity for improved performance.</p>

<p>George Kadianakis requested feedback on a <a href="https://lists.torproject.org/pipermail/tor-dev/2015-April/008625.html" rel="nofollow">draft</a> of a Tor proposal for this feature. One of the major questions still to be resolved is how to ensure that nobody enables this option by mistake, or fails to understand the implications for their service’s anonymity. Possible solutions include choosing a better name, making the configuration file option sound more ominous, or even requiring the operator to compile Tor themselves with a special flag.</p>

<p>See George’s proposal for more use-cases and full details of the concept, and feel free to comment on the tor-dev list thread.</p>

<h1>Tor Summer of Privacy — entry closing soon!</h1>

<p>If you’d like to participate in the first-ever <a href="https://trac.torproject.org/projects/tor/wiki/org/TorSoP" rel="nofollow">Tor Summer of Privacy</a>, you still have the chance — but be quick, as the application period closes on Friday.</p>

<p>Competition for places is already strong, so make it as easy as possible for your entry to be chosen: look at <a href="https://www.torproject.org/about/gsoc#Example" rel="nofollow">previous applications</a> for an idea of what Tor developers like to see, drum up interest from potential mentors on the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-dev" rel="nofollow">tor-dev mailing list</a> or <a href="https://www.torproject.org/about/contact#irc" rel="nofollow">IRC channel</a>, link to your best code samples, and show the community that you can take the initiative in moving your project forward. Good luck!</p>

<h1>More monthly status reports for March 2015</h1>

<p>A few more Tor developers submitted monthly reports for March: <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000802.html" rel="nofollow">Isis Lovecruft</a> (for work on BridgeDB and pluggable transports), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000803.html" rel="nofollow">Arlo Breault</a> (reporting on Tor Messenger and Tor Check), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000804.html" rel="nofollow">Karsten Loesing</a> (for projects including hidden service statistics, translation coordination, and Tor network tools), and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-April/000805.html" rel="nofollow">Colin C.</a> (for work on support, documentation, and localization).</p>

<p>The Tails team published its <a href="https://tails.boum.org/news/report_2015_03" rel="nofollow">March report</a>. Take a look for updates on development, funding, and outreach; summaries of ongoing discussions; Tails in the media; and much more besides.</p>

<h1>Miscellaneous news</h1>

<p>Nathan Freitas <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2015-April/004316.html" rel="nofollow">announced</a> the third release candidate for Orbot v15.  This version supports the x86 processor architecture, so devices such as the Galaxy Tab 3 and the Asus Zenphone/Padphone are now officially Tor-compatible. See Nathan’s announcement for the full changelog.</p>

<p>Giovanni Pellerano <a href="https://lists.torproject.org/pipermail/tor-talk/2015-April/037457.html" rel="nofollow">announced</a> GlobalLeaks 2.60.65, featuring lots of bugfixes, improved localization (including eight new languages), and more.</p>

<p>David Fifield <a href="https://lists.torproject.org/pipermail/tor-dev/2015-April/008637.html" rel="nofollow">located and fixed</a> a problem with meek’s Microsoft Azure backend that was causing it to run much more slowly than the other two options. “If you tried meek-azure before, but it was too slow, give it another try!”</p>

<p>Thanks to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2015-April/000870.html" rel="nofollow">John Penner</a> for running a mirror of the Tor Project’s website and software!</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, the Tails team, and other contributors.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

